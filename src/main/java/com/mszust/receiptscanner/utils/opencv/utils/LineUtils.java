package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.experimental.UtilityClass;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.stream.Collectors;

import static com.mszust.receiptscanner.utils.opencv.utils.PointUtils.getPoints;
import static com.mszust.receiptscanner.utils.opencv.utils.PointUtils.verticalDistance;

@UtilityClass
public class LineUtils {

    public enum LineType {
        LEFT, TOP, RIGHT, BOTTOM
    }

    public static List<List<Line>> partition(List<Line> lines, Mat mat) {
        final List<List<Line>> partitions = new ArrayList<>();

        final List<Line> notMatched = new ArrayList<>(lines);

        while (!notMatched.isEmpty()) {
            final List<Line> partition = new ArrayList<>();

            final Line first = notMatched.get(0);
            partition.add(first);
            notMatched.remove(first);

            for (Line line : notMatched) {
                if (first.isSimilarTo(line)) {
                    partition.add(line);
                }
            }
            notMatched.removeAll(partition);
            partitions.add(partition);
        }
        return partitions;
    }

    public static List<List<Line>> filterRectangle(List<List<Line>> partitions, Mat mat) {
        final List<Line> firstPartition = partitions.get(0);

        final Map<LineType, List<Line>> resultPartitions = new HashMap<>();
        final Line firstLine = firstPartition.get(0);
        LineType firstLineType = determineLineType(firstLine, mat);
        resultPartitions.put(firstLineType, firstPartition);

        //filter out partitions that are not parallel or perpendicular to firstLine
        final List<List<Line>> remainingPartitions = partitions.subList(1, partitions.size()).stream()
                .filter(partition -> partition.get(0).isHorizontalOrPerpendicular(firstLine))
                .collect(Collectors.toList());

//        final Mat toPrint = drawPartitions(mat, Stream.concat(remainingPartitions.stream(), List.of(firstPartition).stream()).collect(Collectors.toList()));
//        print(toPrint, "during-filtering-rect");

        final Map<LineType, List<List<Line>>> conflictingPartitions = new HashMap<>();

        for (List<Line> partition : remainingPartitions) {
            final Line line = partition.get(0);

            final double angleDiff = Math.abs(firstLine.angle() - line.angle());
            if (angleDiff < 5) {
                if (firstLine.distance(line.middle()) > 200) {
                    putIfMissingOrAddConflict(mat, resultPartitions, conflictingPartitions, partition, line);
                }
            } else if (angleDiff > 85 && angleDiff < 95) {
                putIfMissingOrAddConflict(mat, resultPartitions, conflictingPartitions, partition, line);
            }
        }

        return filterConflicts(resultPartitions, conflictingPartitions);
    }

    private static List<List<Line>> filterConflicts(Map<LineType, List<Line>> resultPartitions, Map<LineType, List<List<Line>>> conflictingPartitions) {
        final List<List<Line>> chosenPartitions = new ArrayList<>();

        final List<Line> topPartition = resultPartitions.get(LineType.TOP);
        final List<Line> botPartition = resultPartitions.get(LineType.BOTTOM);
        final List<Line> leftPartition = resultPartitions.get(LineType.LEFT);
        final List<Line> rightPartition = resultPartitions.get(LineType.RIGHT);

        //right now only calculating bot and top
        chosenPartitions.add(leftPartition);
        chosenPartitions.add(rightPartition);

        boolean topMatched = false;
        if (conflictingPartitions.get(LineType.TOP) != null) {
            for (List<Line> conflictingTop : conflictingPartitions.get(LineType.TOP)) {
                if (isCloser(LineType.TOP, topPartition, conflictingTop, leftPartition, rightPartition)) {
                    topMatched = true;
                    chosenPartitions.add(conflictingTop);
                    break;
                }
            }
        }
        if (!topMatched) {
            chosenPartitions.add(topPartition);
        }

        boolean botMatched = false;
        if (conflictingPartitions.get(LineType.BOTTOM) != null) {
            for (List<Line> conflictingBot : conflictingPartitions.get(LineType.BOTTOM)) {
                if (isCloser(LineType.BOTTOM, botPartition, conflictingBot, leftPartition, rightPartition)) {
                    botMatched = true;
                    chosenPartitions.add(conflictingBot);
                    break;
                }
            }
        }
        if (!botMatched) {
            chosenPartitions.add(botPartition);
        }

        return chosenPartitions.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static void putIfMissingOrAddConflict(Mat mat, Map<LineType, List<Line>> resultPartitions, Map<LineType, List<List<Line>>> conflictingPartitions, List<Line> partition, Line line) {
        final LineType lineType = determineLineType(line, mat);
        if (resultPartitions.get(lineType) == null) {
            resultPartitions.put(lineType, partition);
        } else {
            final List<List<Line>> conflicts = conflictingPartitions.getOrDefault(lineType, new ArrayList<>());
            conflicts.add(partition);
            conflictingPartitions.put(lineType, conflicts);
        }
    }

    /**
     * true if <param>testedPartition</param> is closer
     *
     * @param lineType
     * @param partition
     * @param testedPartition
     * @param firstNeighbour
     * @param secondNeighbour
     * @return
     */
    private static boolean isCloser(LineUtils.LineType lineType, List<Line> partition, List<Line> testedPartition, List<Line> firstNeighbour, List<Line> secondNeighbour) {
        if (lineType.equals(LineType.TOP)) {
            final Point existing = partition.get(0).middle();

            final Point conflict = testedPartition.get(0).middle();

            final Point top1 = furthestPoints(firstNeighbour, LineType.TOP);
            final Point top2 = furthestPoints(secondNeighbour, LineType.TOP);

            return Math.min(verticalDistance(existing, top1), verticalDistance(existing, top2)) >
                    Math.min(verticalDistance(conflict, top1), verticalDistance(conflict, top2));
        }
        if (lineType.equals(LineType.BOTTOM)) {
            final Point existing = partition.get(0).middle();

            final Point conflict = testedPartition.get(0).middle();

            final Point top1 = furthestPoints(firstNeighbour, LineType.BOTTOM);
            final Point top2 = furthestPoints(secondNeighbour, LineType.BOTTOM);

            return Math.min(verticalDistance(existing, top1), verticalDistance(existing, top2)) >
                    Math.min(verticalDistance(conflict, top1), verticalDistance(conflict, top2));
        }
        return false;
    }

    private static Point furthestPoints(List<Line> partition, LineUtils.LineType orientation) {
        if (partition == null) {
            return null;
        }
        final List<Point> points = getPoints(List.of(partition), null);
        if (orientation.equals(LineUtils.LineType.TOP)) {
            return points.stream().min(Comparator.comparingDouble(point -> point.y)).orElse(null);
        }
        if (orientation.equals(LineUtils.LineType.BOTTOM)) {
            return points.stream().max(Comparator.comparingDouble(point -> point.y)).orElse(null);
        }
        if (orientation.equals(LineUtils.LineType.LEFT)) {
            return points.stream().min(Comparator.comparingDouble(point -> point.x)).orElse(null);
        }
        if (orientation.equals(LineUtils.LineType.RIGHT)) {
            return points.stream().max(Comparator.comparingDouble(point -> point.x)).orElse(null);
        }
        return null;
    }

    @Nullable
    public static LineType determineLineType(Line firstLine, Mat image) {
        if (isVertical(firstLine)) {
            if (isOnTheLeftHalf(firstLine, image)) {
                return LineType.LEFT;
            }
            return LineType.RIGHT;
        }
        if (isHorizontal(firstLine)) {
            if (isOnTheTopHalf(firstLine, image)) {
                return LineType.TOP;
            }
            return LineType.BOTTOM;
        }
        return null;
    }

    private static boolean isOnTheTopHalf(Line firstLine, Mat image) {
        return firstLine.middle().y <= image.size().height / 2; //y increases towards bottom
    }

    private static boolean isOnTheLeftHalf(Line firstLine, Mat image) {
        return firstLine.middle().x <= image.size().width / 2;
    }

    private static boolean isHorizontal(Line firstLine) {
        return firstLine.angle() < 45;
    }

    private static boolean isVertical(Line firstLine) {
        return firstLine.angle() >= 45;
    }

    public static List<Line> extractLongest(List<List<Line>> partitions) {
        return partitions.stream()
                .map(LineUtils::findLongest)
                .collect(Collectors.toList());
    }

    public static Line findLongest(List<Line> lines) {
        return lines.stream()
                .max(Comparator.comparingDouble(Line::length))
                .orElse(null);
    }

    public static List<List<Line>> findMissingLine(Mat mat, List<Line> fitlines, List<List<Line>> partitions) {
        List<List<Line>> missing = new ArrayList<>();

        final List<Point> points = new ArrayList<>();
        partitions.forEach(lines ->
                lines.forEach(line ->
                        points.addAll(line.points())
                )
        );

        final Boundary boundary = new Boundary(points);

        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(boundary.top(), 10, 100))) {
            missing.add(List.of(boundary.top()));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(boundary.bot(), 10, 100))) {
            missing.add(List.of(boundary.bot()));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(boundary.left(), 10, 100))) {
            missing.add(List.of(boundary.left()));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(boundary.right(), 10, 100))) {
            missing.add(List.of(boundary.right()));
        }

        return missing;
    }

}
