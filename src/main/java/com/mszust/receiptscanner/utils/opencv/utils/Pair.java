package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Pair {
    Double x;
    Integer y;
}
