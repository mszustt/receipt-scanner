package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.springframework.lang.Nullable;

import static org.opencv.imgproc.Imgproc.INTER_AREA;
import static org.opencv.imgproc.Imgproc.INTER_CUBIC;

@Slf4j
@UtilityClass
public class Scaling {

    private final static double OPTIMAL_HEIGHT = 22.0d;

    @Nullable
    public Double findScale(@Nullable Double lineHeight) {
        if (lineHeight == null) {
            log.debug("Line height for image is null");
            return null;
        }
        double scale = OPTIMAL_HEIGHT / lineHeight;
        log.debug("Calculated scale: {}", scale);

        return scale;
    }

    public Mat rescale(Mat src, double scale) {
        Mat processedMat = new Mat();

        int interpolation;
        if (scale > 1.0d) {
            interpolation = INTER_CUBIC;
        } else {
            interpolation = INTER_AREA;
        }
        Imgproc.resize(src, processedMat, new Size(), scale, scale, interpolation);

        return processedMat;
    }
}
