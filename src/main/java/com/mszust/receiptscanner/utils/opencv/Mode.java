package com.mszust.receiptscanner.utils.opencv;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Mode {
    int position;
    int leftWidth;
    int rightWidth;
    double value;

    @Value
    @Builder
    public static class Range {
        int x1;
        int x2;

        public int size() {
            return x2 - x1 + 1;
        }
    }
}
