package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.opencv.ColorUtils.ensureGreyscale;
import static org.opencv.core.Core.NORM_MINMAX;

@UtilityClass
@Slf4j
public class HistogramUtils {

    public static final int HIST_W = 512;
    public static final int HIST_H = 400;

    public Mat histogram(Mat img) {
        log.debug("Calculating histogram for image {}, with {} channels", img, img.channels());
        if (img.channels() > 1) {
            return histogramBGR(img);
        }
        return histogramGreyscale(img);
    }

    public Mat equalize(Mat src) {
        final Mat toEqualize = ensureGreyscale(src);
        final Mat equalized = new Mat();
        Imgproc.equalizeHist(toEqualize, equalized);
        return equalized;
    }

    public Mat normalize(Mat src) {
        final Mat toNormalize = ensureGreyscale(src);
        final Mat normalized = new Mat();
        Core.normalize(toNormalize, normalized, 0, 255, NORM_MINMAX);
        return normalized;
    }

    public static Mat recalculate(Mat src, int threshold) {
        Mat recalculated = new Mat(src, Range.all());
        int rows = src.rows();
        int cols = src.cols();

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                double value = src.get(row, col)[0];
                if (value > threshold) {
                    recalculated.put(row, col, 255);
                }
            }
        }

        return recalculated;
    }

    public static int findBackgroundThreshold(Histogram histogram, int counts) {
        final double[] values = histogram.values();

        int max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > counts) {
                break;
            }
            max = i;
        }

        return max;
    }

    private static int findGlobalMax(double[] values) {
        int max = 0;
        for (int i = 0; i < values.length; i++) {
            max = values[i] > values[max]
                    ? i
                    : max;
        }

        return max;
    }

    private static Mat histogramGreyscale(Mat src) {
        int histSize = 256;

        final Mat hist = histogramArray(src).getHistogram();
        Mat histImage = new Mat(HIST_H, HIST_W, CvType.CV_8UC3, new Scalar(0, 0, 0));
        normalizeHistogram(hist, histImage);
        float[] histData = new float[(int) (hist.total() * hist.channels())];
        hist.get(0, 0, histData);
        int binW = (int) Math.round((double) HIST_W / histSize);

        for (int i = 1; i < histSize; i++) {
            Imgproc.line(histImage, new Point(binW * (i - 1), HIST_H - Math.round(histData[i - 1])),
                    new Point(binW * (i), HIST_H - Math.round(histData[i])), new Scalar(255, 255, 255), 2);
        }

        return histImage;
    }

    public static Histogram histogramArray(Mat image) {
        int histSize = 256;
        float[] range = {0, 256}; //the upper boundary is exclusive

        MatOfFloat histRange = new MatOfFloat(range);
        Mat hist = new Mat();
        Imgproc.calcHist(List.of(image), new MatOfInt(0), new Mat(), hist, new MatOfInt(histSize), histRange, false);
        return Histogram.of(hist);
    }

    private Mat histogramBGR(Mat src) {
        List<Mat> bgrPlanes = new ArrayList<>();
        Core.split(src, bgrPlanes);

        int histSize = 256;

        float[] range = {0, 256}; //the upper boundary is exclusive
        MatOfFloat histRange = new MatOfFloat(range);
        Mat bHist = new Mat(), gHist = new Mat(), rHist = new Mat();
        Imgproc.calcHist(bgrPlanes, new MatOfInt(0), new Mat(), bHist, new MatOfInt(histSize), histRange, false);
        Imgproc.calcHist(bgrPlanes, new MatOfInt(1), new Mat(), gHist, new MatOfInt(histSize), histRange, false);
        Imgproc.calcHist(bgrPlanes, new MatOfInt(2), new Mat(), rHist, new MatOfInt(histSize), histRange, false);
        int binW = (int) Math.round((double) HIST_W / histSize);
        Mat histImage = new Mat(HIST_H, HIST_W, CvType.CV_8UC3, new Scalar(0, 0, 0));
        normalizeHistogram(bHist, histImage);
        normalizeHistogram(gHist, histImage);
        normalizeHistogram(rHist, histImage);
        float[] bHistData = new float[(int) (bHist.total() * bHist.channels())];
        bHist.get(0, 0, bHistData);
        float[] gHistData = new float[(int) (gHist.total() * gHist.channels())];
        gHist.get(0, 0, gHistData);
        float[] rHistData = new float[(int) (rHist.total() * rHist.channels())];
        rHist.get(0, 0, rHistData);

        for (int i = 1; i < histSize; i++) {
            Imgproc.line(histImage, new Point(binW * (i - 1), HIST_H - Math.round(bHistData[i - 1])),
                    new Point(binW * (i), HIST_H - Math.round(bHistData[i])), new Scalar(255, 0, 0), 2);
            Imgproc.line(histImage, new Point(binW * (i - 1), HIST_H - Math.round(gHistData[i - 1])),
                    new Point(binW * (i), HIST_H - Math.round(gHistData[i])), new Scalar(0, 255, 0), 2);
            Imgproc.line(histImage, new Point(binW * (i - 1), HIST_H - Math.round(rHistData[i - 1])),
                    new Point(binW * (i), HIST_H - Math.round(rHistData[i])), new Scalar(0, 0, 255), 2);
        }

        return histImage;
    }

    private static void normalizeHistogram(Mat hist, Mat histImage) {
        Core.normalize(hist, hist, 0, histImage.rows(), NORM_MINMAX);
    }


    private static void findPeeks(Mat histogram) {
        Mat slopeMat = histogram.clone();
//        float slopeP = slopeMat.

        Mat src2 = histogram.reshape(1, 1);

        Mode.Range upHill, downHill;


    }

    // Recursive function to find a peak in the array
    public static int findPeakElement(double[] A, int left, int right) {
        // find the middle element. To avoid overflow, use `left + (right - left) / 2`
        int mid = (left + right) / 2;

        // check if the middle element is greater than its neighbors
        if ((mid == 0 || A[mid - 1] <= A[mid]) &&
                (mid == A.length - 1 || A[mid + 1] <= A[mid])) {
            return mid;
        }

        // If the left neighbor of `mid` is greater than the middle element,
        // find the peak recursively in the left subarray
        if (mid - 1 >= 0 && A[mid - 1] > A[mid]) {
            return findPeakElement(A, left, mid - 1);
        }

        // If the right neighbor of `mid` is greater than the middle element,
        // find the peak recursively in the right subarray
        return findPeakElement(A, mid + 1, right);
    }
}
