package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.Value;

import java.util.List;

@Value
public class Partition {
    List<Line> lines;
    LineUtils.LineType type;

    public Line get(int index) {
        return lines.get(index);
    }
}
