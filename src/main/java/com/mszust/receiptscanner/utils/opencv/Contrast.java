package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

@UtilityClass
public class Contrast {
    private static final CLAHE PROCESSOR = Imgproc.createCLAHE(0.5, new Size(1,1));

    public static Mat contrast(Mat src, double scale) {
        final Mat contrasted = new Mat();
        src.convertTo(contrasted, -1, scale, 0);
        return contrasted;
    }

    public static Mat clahe(Mat src) {
        final Mat greyscaled = ColorUtils.ensureGreyscale(src);
        final Mat clahe = new Mat();
        PROCESSOR.apply(greyscaled, clahe);
        return clahe;
    }
}
