package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;

@UtilityClass
public class OpenCvBinarization {

    public void adaptiveBinarization(Mat originalFile, Mat processedFile) {
        Imgproc.adaptiveThreshold(originalFile, processedFile, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 15, 40);
    }

    public void otsuBinarization(Mat originalFile, Mat processedFile) {
        final Mat rgb = new Mat();
        Imgproc.cvtColor(originalFile, rgb, COLOR_BGR2GRAY);
        Imgproc.threshold(rgb, processedFile, 0, 255, Imgproc.THRESH_OTSU);
    }

    public Mat otsuInverted(Mat originalFile) {
        final Mat rgb = new Mat();
        final Mat processedFile = new Mat();
        Imgproc.cvtColor(originalFile, rgb, COLOR_BGR2GRAY);
        Imgproc.threshold(rgb, processedFile, 0, 255, Imgproc.THRESH_OTSU + Imgproc.THRESH_BINARY_INV);
        return processedFile;
    }
}
