package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.opencv.utils.Pair;
import lombok.experimental.UtilityClass;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@UtilityClass
public class PolygonUtils {

    public static List<Point> orderPoints(List<Point> contour) {
        List<Pair> sum = new ArrayList<>(contour.size());
        List<Pair> diff = new ArrayList<>(contour.size());

        for (int i = 0; i < contour.size(); ++i) {
            sum.add(i, new Pair(contour.get(i).x + contour.get(i).y, i));
            diff.add(i, new Pair(contour.get(i).x - contour.get(i).y, i));
        }

        var minSum = sum.stream().min(Comparator.comparingDouble(Pair::getX)).get();
        var maxSum = sum.stream().max(Comparator.comparingDouble(Pair::getX)).get();
        var minDiff = diff.stream().min(Comparator.comparingDouble(Pair::getX)).get();
        var maxDiff = diff.stream().max(Comparator.comparingDouble(Pair::getX)).get();


        Point topLeft = contour.get(minSum.getY());
        Point topRight = contour.get(maxDiff.getY());
        Point bottomRight = contour.get(maxSum.getY());
        Point bottomLeft = contour.get(minDiff.getY());
        return List.of(topLeft, topRight, bottomRight, bottomLeft);
    }
}
