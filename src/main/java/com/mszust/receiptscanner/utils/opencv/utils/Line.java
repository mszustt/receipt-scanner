package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.Value;
import org.opencv.core.Point;

import java.util.List;

@Value
public class Line {
    Point start;
    Point end;

    private double A() {
        if (Double.isInfinite(slope())) {
            return 1;
        }
        return -slope();
    }

    private double B() {
        if (Double.isInfinite(slope())) {
            return 0;
        }
        return 1;
    }

    private double C() {
        return -rest();
    }

    public double angle() {
        final double angle = Math.abs(Math.toDegrees(Math.atan2(end.y - start.y, end.x - start.x)));
        return reduceAngle(angle);
    }

    private double reduceAngle(double angle) {
        if (angle > 90) {
            return reduceAngle(angle - 180);
        }
        return angle;
    }

    public double length() {
        return Math.sqrt(
                Math.pow(this.start.x - this.end.x, 2)
                        + Math.pow(this.start.y - this.end.y, 2));
    }

    public double slope() {
        return(end.y - start.y) / (double) (end.x - start.x);
    }

    public double rest() {
        if (Double.isInfinite(slope())) {
            return start.x;
        }
        return start.y - slope() * start.x;
    }

    public double verticalDistance(Line other) {
        return Math.abs(this.middle().y - other.middle().y);
    }

    public double horizontalDistance(Line other) {
        return Math.abs(this.middle().x - other.middle().x);
    }

    public Point middle() {
        return new Point(
                (start.x + end.x) / 2,
                (start.y + end.y) / 2
        );
    }

    public double minLinearDistance(Line other) {
        final double horizontal = horizontalDistance(other);
        final double vertical = verticalDistance(other);

        return Math.min(horizontal, vertical);
    }

    public double distance(Line other) {
        return Math.sqrt(
                Math.pow(this.middle().x - other.middle().x, 2)
                        + Math.pow(this.middle().y - other.middle().y, 2)
        );
    }

    public double distance(Point point) {
        return Math.abs(A() * point.x + B() * point.y + C()) / Math.sqrt(Math.pow(A(), 2) + Math.pow(B(), 2));
    }

    public Line normalTo(Point point) {
        return null;
    }

    public List<Point> points() {
        return List.of(start, end);
    }

    public boolean isSimilarTo(Line other) {
        final double angleDiff = Math.abs(this.angle() - other.angle());
        if (angleDiff < 5) {
            return this.distance(other.middle()) < 50;
        }
        return false;
    }

    public boolean isSimilarTo(Line other, double angle, double distance) {
        final double angleDiff = Math.abs(this.angle() - other.angle());
        if (angleDiff < angle) {
            return this.distance(other.middle()) < distance;
        }
        return false;
    }

    public boolean isHorizontalOrPerpendicular(Line other) {
        return hasSimilarAngle(this.angle(), other.angle(), 3) || isPerpendicular(this.angle(), other.angle(), 5);
    }

    public static boolean hasSimilarAngle(double x, double y, double epsilon) {
        return Math.abs(x - y) < epsilon;
    }

    public static boolean isPerpendicular(double x, double y, double epsilon) {
        return Math.abs(Math.abs(x - y) - 90) < epsilon;
    }
}
