package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.opencv.core.Point;

import java.util.List;

import static com.mszust.receiptscanner.utils.opencv.PolygonUtils.orderPoints;

@Value
@AllArgsConstructor
public class Boundary {
    Point topLeft;
    Point topRight;
    Point bottomRight;
    Point bottomLeft;

    public Boundary(List<Point> points) {
        final List<Point> ordered = orderPoints(points);

        this.topLeft = ordered.get(0);
        this.topRight = ordered.get(1);
        this.bottomRight = ordered.get(2);
        this.bottomLeft = ordered.get(3);
    }

    public List<Point> points() {
        return List.of(topLeft, topRight, bottomRight, bottomLeft);
    }

    public Line top() {
        return new Line(this.getTopLeft(), this.getTopRight());
    }

    public Line bot() {
        return new Line(this.getBottomLeft(), this.getBottomRight());
    }

    public Line right() {
        return new Line(this.getBottomRight(), this.getTopRight());
    }

    public Line left() {
        return new Line(this.getBottomLeft(), this.getTopLeft());
    }


}
