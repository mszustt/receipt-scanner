package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.opencv.utils.Boundary;
import com.mszust.receiptscanner.utils.opencv.utils.Line;
import com.mszust.receiptscanner.utils.opencv.utils.LineUtils;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.*;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.stream.Collectors;

import static com.mszust.receiptscanner.utils.opencv.FindingContours.approxLines;
import static com.mszust.receiptscanner.utils.opencv.FindingContours.approxWithPolylines;
import static com.mszust.receiptscanner.utils.opencv.PolygonUtils.orderPoints;
import static com.mszust.receiptscanner.utils.opencv.utils.LineUtils.determineLineType;
import static com.mszust.receiptscanner.utils.opencv.utils.LineUtils.findMissingLine;
import static com.mszust.receiptscanner.utils.opencv.utils.PointUtils.getPoints;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.*;

@Slf4j
public class Cropping {

//    @Deprecated
//    public static List<Point> getContourUsingCanny(Mat src) {
//        Mat src_gray = new Mat();
//        Imgproc.cvtColor(src, src_gray, COLOR_BGR2GRAY);
////        Imgproc.GaussianBlur(src_gray, src_gray, new Size(9, 9), 0);
//
//        Mat canny_output = new Mat();
//
//        /// Detect edges using canny
//        Imgproc.Canny(src_gray, canny_output, 200, 250);
//        Imgproc.dilate(canny_output, src_gray, Imgproc.getStructuringElement(MORPH_RECT, new Size(2, 2)), new Point(-1, -1), 2);
//        Mat toPrint = new Mat();
//        Imgproc.resize(src_gray, toPrint, new Size(), 0.25d, 0.25d, INTER_AREA);
//        HighGui.imshow("src_gray", toPrint);
//        HighGui.waitKey(0);
//        /// Find contours
//        List<MatOfPoint> contours = new ArrayList<>();
//        Mat hierarchy = new Mat();
//        Imgproc.findContours(src_gray, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, new Point(0, 0));
//
//        final List<MatOfPoint> significantContours = findSignificantContours(src, contours);
//        final MatOfPoint contour = findBoundingContour(src, contours);
//
//        toPrint = new Mat();
//        Imgproc.resize(src, toPrint, new Size(), 0.25d, 0.25d, INTER_AREA);
//        Imgproc.drawContours(toPrint, List.of(contour), -1, new Scalar(255), 4);
//        HighGui.imshow("src", toPrint);
//        HighGui.waitKey(0);
//
//        //filter rectangles
////        List<MatOfPoint> rectContours = contours.stream().filter(contour -> isRectangle(contour.toList())).collect(Collectors.toList());
//        List<MatOfPoint> contoursApproxed = new ArrayList<>(contours.size());
//        List<Point> result = new ArrayList<>();
//        double maxArea = 0.0;
//        for (int i = 0; i < contours.size(); ++i) {
//            MatOfPoint2f mMOP2f1 = new MatOfPoint2f();
//            MatOfPoint2f mMOP2f2 = new MatOfPoint2f();
//            contours.get(i).convertTo(mMOP2f1, CvType.CV_32FC2);
//            Imgproc.approxPolyDP(mMOP2f1, mMOP2f2, contours.get(i).toList().size() * 0.2, true);
//            mMOP2f2.convertTo(contours.get(i), CV_32S);
////            Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i)), contoursApproxed.get(i), contours.get(i).toList().size() * 0.2, true);
//
////            if (!isRectangle(contours.get(i).toList())) {
////                continue;
////            }
//            double area = Imgproc.contourArea(contours.get(i));
//            //find largest contour
//            if (area > maxArea) {
//                maxArea = area;
//                result = contours.get(i).toList();
//            }
//            if (area < 1000d) {
//                continue;
//            }
////            System.out.println("i = " + i + ", area = " + area);
//
////            Imgproc.polylines(src, List.of(contours.get(i)), true, new Scalar(255), 4);
////            Mat toPrint = new Mat();
////            Imgproc.resize(src, toPrint, new Size(), 0.25d, 0.25d, INTER_AREA);
////            HighGui.imshow("src", toPrint);
////            HighGui.waitKey(0);
//        }
//        result = orderPoints(result);
//        System.out.println("Max area = " + maxArea);
//        return result;
//    }

//    private static List<MatOfPoint> findSignificantContours(Mat src, List<MatOfPoint> contours) {
//        return contours.stream()
////                .filter(contour -> Double.compare(Imgproc.contourArea(contour), src.size().area() * 0.05d) > 0)
//                .filter(contour -> Double.compare(Imgproc.contourArea(contour), 300d) > 0)
//                .collect(toList());
//    }

//    private static boolean isRectangle(List<Point> contour) {
//        return contour.size() == 4;
//    }

//    @Deprecated
//    public static List<Point> getContourUsingMSER(Mat src) {
//        List<MatOfPoint> msers = new ArrayList<>();
//        MatOfRect bboxes = new MatOfRect();
//        MSER.create().detectRegions(src, msers, bboxes);
//
//        Mat toPrint = new Mat();
//        Imgproc.resize(src, toPrint, new Size(), 0.25d, 0.25d, INTER_AREA);
//        MatOfPoint biggestContour = findBoundingContour(src, msers);
//        System.out.println("Biggest contour area: " + Imgproc.contourArea(biggestContour));
//        Imgproc.polylines(toPrint, List.of(biggestContour), true, new Scalar(128), 4);
//        Imgproc.drawContours(toPrint, List.of(biggestContour), -1, new Scalar(255), 4);
//
//        HighGui.imshow("msers", toPrint);
//        HighGui.waitKey(0);
//
//        return biggestContour.toList();
//    }

    private static List<MatOfPoint> sort(List<MatOfPoint> contours) {
        final var list = contours.stream()
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    private static List<MatOfPoint> sortAndFilterContours(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        final var list = contours.stream()
                .filter(contour -> 0.2 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    //    public static MatOfPoint findBoundingContour(Mat srcImage, List<MatOfPoint> contours) {
//        final double sourceArea = srcImage.size().area();
//        return contours.stream()
//                .filter(contour -> 0.3 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
//                .max(Comparator.comparingDouble(Imgproc::contourArea))
//                .orElse(null);
//    }
//
//    @Deprecated
//    private static List<Point> findBoudingRectUsingCanny(Mat src) {
//        Mat src_gray = new Mat();
//        Imgproc.cvtColor(src, src_gray, COLOR_BGR2GRAY);
//        Imgproc.GaussianBlur(src_gray, src_gray, new Size(9, 9), 0);
//
//        Mat canny_output = new Mat();
//
//        /// Detect edges using canny
//        Imgproc.Canny(src_gray, canny_output, 250, 350);
//        Imgproc.dilate(canny_output, src_gray, Imgproc.getStructuringElement(MORPH_RECT, new Size(2, 2)), new Point(-1, -1), 2);
//        final Rect boundingRect = Imgproc.boundingRect(src_gray);
//
//        return List.of(
//                boundingRect.tl(),
//                new Point(boundingRect.tl().x + boundingRect.width, boundingRect.tl().y),
//                boundingRect.br(),
//                new Point(boundingRect.br().x - boundingRect.width, boundingRect.br().y)
//        );
//    }
//
    public static Mat cropImageApprox(Mat sourceImage) {
        log.info("Cropping image approx: " + sourceImage);
        final MatOfPoint primalContour = FindingContours.findBoundingContour(sourceImage);


        if (primalContour == null) {
            return sourceImage;
        }

        final MatOfPoint approxPolylines = approxWithPolylines(primalContour);

        final Mat withLines = approxLines(sourceImage, approxPolylines);

        List<MatOfPoint> newContours = FindingContours.findContours(withLines);
        final List<MatOfPoint> newContoursSorted = sortAndFilterContours(withLines, newContours);

        MatOfPoint contour = chooseContour(primalContour, newContoursSorted);

        if (contour != null) {
            return cropOnContour(sourceImage, contour); //best results
//            return cropRotated(sourceImage, contour);
//            return cropOnMask(sourceImage, contour);
//            return houghLinePContour(sourceImage, contour);
        }
        return sourceImage;
    }

    public static Mat cropImageExact(Mat sourceImage) throws ExactCroppingFailedException {
        log.info("Cropping image exact");
        final MatOfPoint primalContour = FindingContours.findBoundingContour(sourceImage);


        if (primalContour == null) {
            throw new ExactCroppingFailedException();
        }

        return houghLinePContour(sourceImage, primalContour);
    }

    private static Mat cropOnMask(Mat sourceImage, MatOfPoint contour) {
        Rect roi = Imgproc.boundingRect(contour);
        Mat mask = Mat.zeros(sourceImage.size(), sourceImage.type());
        Imgproc.drawContours(mask, List.of(contour), -1, Scalar.all(255), -1);
        Mat cropped = new Mat();
        sourceImage.copyTo(cropped, mask);
//        Mat backgroundMask = new Mat();
//        Core.inRange(cropped, new Scalar(0), new Scalar(1), backgroundMask);
//        cropped.setTo(new Scalar(255, 255, 255), backgroundMask);
        return new Mat(cropped, roi);
    }

    private static Mat cropOnContourBoundary(Mat sourceImage, @NonNull MatOfPoint contour) {
        return new Mat(sourceImage, Imgproc.boundingRect(contour));
    }

    public static Mat cropRotated(Mat sourceImage, @NonNull MatOfPoint contour) {
        MatOfPoint2f theNewContour2f = new MatOfPoint2f(contour.toArray());
        RotatedRect rotatedRect = Imgproc.minAreaRect(theNewContour2f);
        Mat rotatedRectPoints = new Mat();
        Imgproc.boxPoints(rotatedRect, rotatedRectPoints);
        Point[] vertices = new Point[4];
        rotatedRect.points(vertices);
        List<Point> srcOrdered = orderPoints(Arrays.asList(vertices));
        int width = (int) rotatedRect.size.width;
        int height = (int) rotatedRect.size.height;

        if (width > height) {
            width = (int) rotatedRect.size.height; //swap
            height = (int) rotatedRect.size.width; //swap
        }

        MatOfPoint2f src = new MatOfPoint2f(srcOrdered.get(0), srcOrdered.get(1), srcOrdered.get(2), srcOrdered.get(3));

        MatOfPoint2f dst = new MatOfPoint2f(
                new Point(0, 0),
                new Point(width - 1, 0),
                new Point(width - 1, height - 1),
                new Point(0, height - 1)
        );

        final Mat transformed = Imgproc.getPerspectiveTransform(src, dst);

        final Mat dstMat = new Mat(sourceImage, Range.all());
        Imgproc.warpPerspective(sourceImage, dstMat, transformed, new Size(width, height));
        return dstMat;
    }

    private static MatOfPoint chooseContour(MatOfPoint contour, List<MatOfPoint> newContoursSorted) {
        if (newContoursSorted.isEmpty()) {
            return contour;
        }
        if (newContoursSorted.size() > 1) {
            if (Imgproc.matchShapes(newContoursSorted.get(1), contour, 1, 0d) >
                    Imgproc.matchShapes(newContoursSorted.get(0), contour, 1, 0d)) {
                return newContoursSorted.get(1);
            } else {
                return newContoursSorted.get(0);
            }
        }
        return newContoursSorted.get(0);
    }

    public static Mat houghLinePContour(Mat matFile, MatOfPoint contour) throws ExactCroppingFailedException {
        //find contour
        final Mat contourMat = new Mat(matFile.size(), CV_8UC1);
        Imgproc.drawContours(contourMat, List.of(contour), -1, new Scalar(255), 4);

//        Mat toPrint = new Mat(contourMat, Range.all());
//        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//        HighGui.imshow("contour", toPrint);
//        HighGui.waitKey(0);

        //find lines with probabilistic hough
        final Mat linesP = findLinesP(preprocess(contourMat), 1);

        //convert lines to normal objects
        final List<Line> linesPList = new ArrayList<>();
        for (int i = 0; i < linesP.rows(); i++) {
            double[] data = linesP.get(i, 0);
            linesPList.add(getLineP(data));
        }

        //partition lines
        List<List<Line>> partitions = LineUtils.partition(linesPList, contourMat);
//        final Mat matPartitions = drawPartitions(contourMat, partitions);
//        print(matPartitions, "partitions-mat");


        partitions = LineUtils.filterRectangle(partitions, matFile);
        drawPartitions(contourMat, partitions);
//        final Mat matFilterRect = drawPartitions(contourMat, partitions);
//        print(matFilterRect, "filter-rect");

        if (partitions.size() < 3) {
            throw new ExactCroppingFailedException();
        }

        partitions = fixIncorrectLines(matFile, partitions);
        drawPartitions(contourMat, partitions);
//        final Mat matFixLines = drawPartitions(contourMat, partitions);
//        print(matFixLines, "fix-inc-lines");

        //fitlines - 1st round
        List<Line> fittedLines = fitLines(contourMat, partitions);

        if (partitions.size() < 4) {
            //add missing contour lines based on original contour boundaries
            partitions.addAll(findMissingLine(contourMat, fittedLines, partitions));
            //recalculate fitlines
            fittedLines = fitLines(contourMat, partitions);
        }

        //find contour of fitlines combination
        Mat singleChannelFitlines = new Mat(matFile.size(), CV_8UC1);
        drawLines(singleChannelFitlines, fittedLines);
        final List<MatOfPoint> fitlineContours = new ArrayList<>();
        Imgproc.findContours(singleChannelFitlines, fitlineContours, new Mat(), RETR_LIST, CHAIN_APPROX_SIMPLE);

        if (fitlineContours.isEmpty()) {
            return matFile;
        }
        //find the right
        final List<MatOfPoint> fitlineContoursSorted = sortAndFilterContours(matFile, fitlineContours);

        if (fitlineContoursSorted.isEmpty()) {
            return matFile;
        }

        if (fitlineContoursSorted.size() > 1) {
            return Cropping.cropOnContour(new Mat(matFile, Range.all()), fitlineContoursSorted.get(1));
        }
        return Cropping.cropOnContour(new Mat(matFile, Range.all()), fitlineContoursSorted.get(0));
    }

    private static List<List<Line>> fixIncorrectLines(Mat contourMat, List<List<Line>> partitions) {
        //todo: for IMG_0305 its not picking the right one (should be from contour)
        final List<List<Line>> result = new ArrayList<>();

        Map<LineUtils.LineType, List<Line>> lines = new HashMap<>();
        partitions.forEach(partition -> lines.put(determineLineType(partition.get(0), contourMat), partition));

        //checking boundary N line only when N line is removed from partitions
        //todo: what condition makes sense? can be checking perpendicularity to other lines
        final List<Line> topPartition = lines.get(LineUtils.LineType.TOP);
        final List<Line> bottomPartition = lines.get(LineUtils.LineType.BOTTOM);
        final List<Line> leftPartition = lines.get(LineUtils.LineType.LEFT);
        final List<Line> rightPartition = lines.get(LineUtils.LineType.RIGHT);

        if (topPartition != null) {
            final Line longestTop = topPartition.stream().max(Comparator.comparingDouble(Line::length)).orElse(null);
            final Line topBoundary = new Boundary(getPoints(partitions, topPartition)).top();

            if (isSignificantlyLonger(topBoundary, longestTop) && isPerpendicular(topBoundary, leftPartition, rightPartition)) {
                result.add(List.of(topBoundary));
            } else {
                result.add(topPartition);
            }
        }

        if (bottomPartition != null) {
            final Line longestBot = bottomPartition.stream().max(Comparator.comparingDouble(Line::length)).orElse(null);

            final Line botBoundary = new Boundary(getPoints(partitions, bottomPartition)).bot();

            if (isSignificantlyLonger(botBoundary, longestBot) && isPerpendicular(botBoundary, leftPartition, rightPartition)) {
                result.add(List.of(botBoundary));
            } else {
                result.add(bottomPartition);
            }
        }

//        Mat toPrint = new Mat(contourMat.size(), CV_8UC3);
//        toPrint = drawPartitions(toPrint, partitions);
//        for (Line line : List.of(topBoundary, botBoundary)) {
//            Imgproc.line(toPrint, line.getStart(), line.getEnd(), new Scalar(255, 0, 255), 4);
//        }
//        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//        HighGui.imshow("partition-vs-boundaries", toPrint);
//        HighGui.waitKey(0);


        if (leftPartition != null) {
            result.add(leftPartition);
        }
        if (rightPartition != null) {
            result.add(rightPartition);
        }

        return result;
    }


    private static boolean isSignificantlyLonger(Line boundaryLine, @Nullable Line longestInPartition) {
        return longestInPartition == null || boundaryLine.length() > 2 * longestInPartition.length();

    }

    private static boolean isPerpendicular(Line boundaryLine, @Nullable List<Line> firstNeighbour, @Nullable List<Line> secondNeighbour) {
        if (firstNeighbour == null || secondNeighbour == null) {
            return false;
        }
        return Line.isPerpendicular(boundaryLine.angle(), secondNeighbour.get(0).angle(), 3) && Line.isPerpendicular(boundaryLine.angle(), firstNeighbour.get(0).angle(), 3);
    }


    public static Mat drawPartitions(Mat contourMat, List<List<Line>> partitions) {
        Mat partitionsMat = new Mat(contourMat.size(), CV_8UC3);
        for (List<Line> partition : partitions) {
            for (Line line : partition) {
                Imgproc.line(partitionsMat, line.getStart(), line.getEnd(), new Scalar(255, 255, 0), 4);
            }
        }
        return partitionsMat;
    }

    public static void print(Mat mat, @Nullable String name) {
        String windowName = "mat";
        if (isNotBlank(name)) {
            windowName = name;
        }
        Mat toPrint = new Mat(mat, Range.all());
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow(windowName, toPrint);
        HighGui.waitKey(0);
    }

    private static List<Line> fitLines(Mat contourMat, List<List<Line>> partitions) {
        //for each partition draw it, and fitline to its points
        List<Line> fittedLines = new ArrayList<>();
        for (List<Line> partition : partitions) {
            Mat partitionsMat = new Mat(contourMat.size(), CV_8UC1);
            drawLines(partitionsMat, partition);
            // fitting line to contours - could be a better option! (directly)
            List<MatOfPoint> contours = new ArrayList<>();
            Imgproc.findContours(partitionsMat, contours, new Mat(), RETR_LIST, CHAIN_APPROX_SIMPLE);
            // use only biggest contour
            List<MatOfPoint> contoursSorted = sort(contours);
            final Mat fitline = new Mat();
            Imgproc.fitLine(contoursSorted.get(0), fitline, 2, 0, 0.01, 0.01);

            final Line fitlineLine = extractFitline(partitionsMat.size(), fitline);
            fittedLines.add(fitlineLine);
        }
        return fittedLines;
    }

    private static Mat cropOnContour(Mat mat, MatOfPoint contour) {
        Boundary boundary = new Boundary(contour.toList());
        RotatedRect rotatedRect = Imgproc.minAreaRect(new MatOfPoint2f(contour.toArray()));

        int width = (int) rotatedRect.size.width;
        int height = (int) rotatedRect.size.height;

        if (width > height) {
            width = (int) rotatedRect.size.height; //swap
            height = (int) rotatedRect.size.width; //swap
        }

        MatOfPoint2f src = new MatOfPoint2f(boundary.getTopLeft(), boundary.getTopRight(), boundary.getBottomRight(), boundary.getBottomLeft());

        MatOfPoint2f dst = new MatOfPoint2f(
                new Point(0, 0),
                new Point(width - 1, 0),
                new Point(width - 1, height - 1),
                new Point(0, height - 1)
        );

        final Mat transformed = Imgproc.getPerspectiveTransform(src, dst);

        final Mat dstMat = new Mat(mat, Range.all());
        Imgproc.warpPerspective(mat, dstMat, transformed, new Size(width, height));
        return dstMat;
    }

    private static Mat findLinesP(Mat src, int maxLineGap) {
        Mat linesP = new Mat();
        Imgproc.HoughLinesP(src, linesP, 1, Math.PI / 180, 100, 100, maxLineGap);
        return linesP;
    }


    private static Line extractFitline(Size srcSize, Mat fitline) {
        double lefty = ((-1) * fitline.get(2, 0)[0] * fitline.get(1, 0)[0] / fitline.get(0, 0)[0]) + fitline.get(3, 0)[0];
        double righty = ((srcSize.width - fitline.get(2, 0)[0]) * fitline.get(1, 0)[0] / fitline.get(0, 0)[0]) + fitline.get(3, 0)[0];
        return new Line(
                new Point(srcSize.width - 1, righty),
                new Point(0, lefty)
        );
    }

    private static Line getLineP(double[] line) {
        Point pt1 = new Point(line[0], line[1]);
        Point pt2 = new Point(line[2], line[3]);
        return new Line(pt1, pt2);
    }

    private static Mat preprocess(Mat contourMat) {
        final Mat preprocessed = new Mat(contourMat, Range.all());
        Imgproc.GaussianBlur(preprocessed, preprocessed, new Size(3, 3), 0);
        return preprocessed;
    }

    private static Mat drawLines(Mat src, Mat lines) {
        final Mat withLines = new Mat(src, Range.all());
        for (int y = 0; y < lines.rows(); y++) {
            double[] vec = lines.get(y, 0);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(withLines, start, end, new Scalar(255), 4);
        }
        return withLines;
    }

    private static void drawLines(Mat src, List<Line> lines) {
        for (Line line : lines) {
            Imgproc.line(src, line.getStart(), line.getEnd(), new Scalar(255), 4);
        }
    }


}

