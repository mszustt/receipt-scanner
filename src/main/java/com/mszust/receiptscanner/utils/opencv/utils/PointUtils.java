package com.mszust.receiptscanner.utils.opencv.utils;

import lombok.experimental.UtilityClass;
import org.opencv.core.Point;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@UtilityClass
public class PointUtils {

    public static double verticalDistance(@Nullable Point point, @Nullable Point other) {
        if (point == null || other == null) {
            return Double.POSITIVE_INFINITY;
        }
        return Math.abs(point.y - other.y);
    }

    public static double distance(@Nullable Point point, @Nullable Point other) {
        if (point == null || other == null) {
            return Double.POSITIVE_INFINITY;
        }
        return Math.sqrt(
                Math.pow(point.x - other.x, 2)
                        + Math.pow(point.y - other.y, 2));
    }

    public static List<Point> getPoints(List<List<Line>> partitions, @Nullable List<Line> without) {
        final List<List<Line>> toCalculate = new ArrayList<>(partitions);
        if (isNotEmpty(without)) {
            toCalculate.remove(without);
        }
        final List<Point> points = new ArrayList<>();
        toCalculate.forEach(lines ->
                lines.forEach(line ->
                        points.addAll(line.points())
                )
        );
        return points;
    }
}

