package com.mszust.receiptscanner.utils.opencv;

import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;
import static org.opencv.imgproc.Imgproc.RETR_LIST;

@Slf4j
public class FindingContours {

    public static List<MatOfPoint> findContours(Mat sourceBinaryImage) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat binaryImage = new Mat();
        if (sourceBinaryImage.channels() > 1) {
            log.warn("Warning - finding contours on non-binary image might result in no output.");
            Imgproc.cvtColor(sourceBinaryImage, binaryImage, COLOR_BGR2GRAY);
        } else {
            binaryImage = sourceBinaryImage;
        }
        Imgproc.findContours(binaryImage, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE, new Point(0, 0));
        return contours;
    }

    public static MatOfPoint findReceiptContour(Mat sourceImage) {
        final MatOfPoint primalContour = findBoundingContour(sourceImage);

        if (primalContour == null) {
            return null;
        }


        final MatOfPoint approxPolylines = approxWithPolylines(primalContour);

        final Mat withLines = approxLines(sourceImage, approxPolylines);

        List<MatOfPoint> newContours = FindingContours.findContours(withLines);
        final List<MatOfPoint> newContoursSorted = sortAndFilterContours(withLines, newContours);

        return chooseContour(primalContour, newContoursSorted);
    }

    @Nullable
    public static MatOfPoint findBoundingContour(Mat sourceImage) {
        Mat binaryMat = new Mat();
        if (sourceImage.channels() > 1) {
            OpenCvBinarization.otsuBinarization(sourceImage, binaryMat);
        } else {
            binaryMat = sourceImage;
        }

        final List<MatOfPoint> contours = findContours(binaryMat);

        return chooseBoundingContour(sourceImage, contours);
    }

    private static MatOfPoint chooseContour(MatOfPoint contour, List<MatOfPoint> newContoursSorted) {
        if (newContoursSorted.isEmpty()) {
            return contour;
        }
        if (newContoursSorted.size() > 1) {
            if (Imgproc.matchShapes(newContoursSorted.get(1), contour, 1, 0d) >
                    Imgproc.matchShapes(newContoursSorted.get(0), contour, 1, 0d)) {
                return newContoursSorted.get(1);
            } else {
                return newContoursSorted.get(0);
            }
        }
        return newContoursSorted.get(0);
    }

    public static MatOfPoint chooseBoundingContour(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        return contours.stream()
                .filter(contour -> 0.2 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .max(Comparator.comparingDouble(Imgproc::contourArea))
                .orElse(null);
    }

    private static List<MatOfPoint> sortAndFilterContours(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        final var list = contours.stream()
                .filter(contour -> 0.3 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    static MatOfPoint approxWithPolylines(MatOfPoint contour) {
        MatOfPoint2f approxPoly = new MatOfPoint2f();
        MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());
        double approxDistance = Imgproc.arcLength(contour2f, true) * 0.014;
        Imgproc.approxPolyDP(contour2f, approxPoly, approxDistance, true);
        return new MatOfPoint(approxPoly.toArray());
    }

    static Mat approxLines(Mat matFile, MatOfPoint polylines) {
        var base = new Mat(matFile.size(), CV_8UC1);
        Imgproc.polylines(base, List.of(polylines), true, new Scalar(255), 4);
        final Mat lines = new Mat();
        Imgproc.HoughLinesP(base, lines, 1, Math.PI / 180, 100, 10, 1000);
        Mat withLines = new Mat(matFile.size(), CV_8UC1);
        for (int i = 0; i < lines.rows(); i++) {
            double[] val = lines.get(i, 0);
            Imgproc.line(withLines, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(255), 4);
        }
        return new Mat(withLines, Range.all());
    }
}
