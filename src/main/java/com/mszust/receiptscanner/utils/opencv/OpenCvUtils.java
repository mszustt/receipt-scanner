package com.mszust.receiptscanner.utils.opencv;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.BORDER_DEFAULT;
import static org.opencv.imgproc.Imgproc.INTER_AREA;
import static org.opencv.imgproc.Imgproc.INTER_CUBIC;

@UtilityClass
public class OpenCvUtils {

    public static Mat loadImage(String imagePath) {
        return Imgcodecs.imread(imagePath, Imgcodecs.IMREAD_GRAYSCALE);
    }

    public static void writeImage(String path, Mat image) {
        Imgcodecs.imwrite(path, image);
    }

    @SneakyThrows
    public static void writeImage(String path, BufferedImage bufferedImage) {
        ImageIO.write(
                bufferedImage,
                "png",
                new File(path)
        );
    }

    public static byte[] toByteArray(Mat image) {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", image, matOfByte);
        return matOfByte.toArray();
    }

//    @SneakyThrows
//    public static Mat toMat(BufferedImage bufferedImage) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        ImageIO.write(bufferedImage, "bmp", byteArrayOutputStream);
//        byteArrayOutputStream.flush();
//        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
//    }

    public static Mat bufferedImageToMat(BufferedImage bufferedImage) {
        Mat mat = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
        try {
            byte[] data = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
            mat.put(0, 0, data);
            return mat;
        } catch (ClassCastException cce) {
            //do nothing
        }

        int[] data = ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }

    public static BufferedImage toBufferedImage(Mat image) throws IOException {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", image, matOfByte);
        byte[] ba = matOfByte.toArray();
        return ImageIO.read(new ByteArrayInputStream(ba));
    }

    public static void rescale(Mat source, Mat dst, double scale) {
        int interpolation;
        if (scale > 1.0d) {
            interpolation = INTER_CUBIC;
        } else {
            interpolation = INTER_AREA;
        }
        Imgproc.resize(source, dst, new Size(), scale, scale, interpolation);
    }

    public static List<Rect> detectText(Mat img) {
        List<Rect> boundRect = new ArrayList<>();

        Mat img_blur = new Mat(), img_gray = new Mat(), img_sobel_x = new Mat(), img_sobel_y = new Mat(), abs_sobel_x = new Mat(), abs_sobel_y = new Mat(), img_sobel = new Mat(), img_threshold = new Mat();

        //Gaussian blur
//        Imgproc.GaussianBlur(img, img_blur, new Size(3,3), 0, 0, BORDER_DEFAULT);

        //Greyscale
        Imgproc.cvtColor(img, img_gray, Imgproc.COLOR_BGR2GRAY);


        //Sobel
        Imgproc.Sobel(img_gray, img_sobel_x, CvType.CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
        Imgproc.Sobel(img_gray, img_sobel_y, CvType.CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT);
//        Convert back to 8U
        Core.convertScaleAbs(img_sobel_x, abs_sobel_x);
        Core.convertScaleAbs(img_sobel_y, abs_sobel_y);

        Core.addWeighted(abs_sobel_x, 0.5, abs_sobel_y, 0.5, 0, img_sobel);

        //at src, Mat dst, double thresh, double maxval, int type
        Imgproc.threshold(img_sobel, img_threshold, 0, 255, 8);

        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.morphologyEx(img_threshold, img_threshold, Imgproc.MORPH_CLOSE, element);
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(img_threshold, contours, hierarchy, 0, 1);

        List<MatOfPoint> contours_poly = new ArrayList<>(contours.size());

        for (int i = 0; i < contours.size(); i++) {

            MatOfPoint2f mMOP2f1 = new MatOfPoint2f();
            MatOfPoint2f mMOP2f2 = new MatOfPoint2f();

            contours.get(i).convertTo(mMOP2f1, CvType.CV_32FC2);
            Imgproc.approxPolyDP(mMOP2f1, mMOP2f2, 2, true);
            mMOP2f2.convertTo(contours.get(i), CvType.CV_32S);


            Rect appRect = Imgproc.boundingRect(contours.get(i));
            if (appRect.width > appRect.height) {
                boundRect.add(appRect);
            }
        }

        return boundRect;
    }

    public static void printMat(Mat src) {
        int rows = src.rows();
        int cols = src.cols();

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                double value = src.get(row, col)[0];
                if (value != 0) {
                    System.out.println(String.format("row = %d, col = %d, value = %f", row, col, value));
                }
            }
        }
    }

}
