package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.imgproc.Imgproc;

@UtilityClass
public class ColorUtils {

    public static Mat ensureGreyscale(Mat src) {
        if (src.channels() > 1) {
            Mat greyscale = new Mat();
            Imgproc.cvtColor(src, greyscale, Imgproc.COLOR_BGR2GRAY);
            return greyscale;
        }
        return new Mat(src, Range.all());
    }
}
