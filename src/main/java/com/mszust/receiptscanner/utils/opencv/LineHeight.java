package com.mszust.receiptscanner.utils.opencv;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@UtilityClass
public class LineHeight {

    /**
     * @param src - cropped and transformed image
     * @return null if height could not be calculated, probabilistic line height otherwise
     */
    @Nullable
    public Double findLineHeight(Mat src) {
        Mat binaryMat;
        if (src.channels() > 1) {
            binaryMat = OpenCvBinarization.otsuInverted(src);
        } else {
            binaryMat = src;
        }

        Mat hist = new Mat();
        Core.reduce(binaryMat, hist, 1, Core.REDUCE_AVG);
        hist.reshape(-1);

        var th = 2;
        var originalHeight = src.size().height;
        var originalWidth = src.size().width;

        byte[][] arr = new byte[hist.cols()][hist.rows()];

        byte[] tmp = new byte[1];
        for (int i = 0; i < hist.rows(); ++i) {
            for (int j = 0; j < hist.cols(); ++j) {
                hist.get(i, j, tmp);
                arr[j][i] = tmp[0];
            }
        }

        final List<Integer> uppers = new ArrayList<>();
        final List<Integer> lowers = new ArrayList<>();

        //find first 2 consecutive zeros to reduce noise, as its not what we're looking for anyway
        int j = 0;

        for (int i = 0; i < originalHeight - 1; i++) {
            if (arr[0][i] == 0 && arr[0][i+1] == 0) {
                j = i;
                break;
            }
        }

        for (int i = j; i < originalHeight - 1; i++) {
            if (arr[0][i] <= th && arr[0][i + 1] > th) {
                uppers.add(i);
            }
            if (arr[0][i] > th && arr[0][i + 1] <= th) {
                lowers.add(i);
            }
        }

        List<Double> heights = new ArrayList<>();

        for (int i = 0; i < Math.min(uppers.size(), lowers.size()); i++) {
            heights.add((double) lowers.get(i) - uppers.get(i));
        }

        for (Integer y : uppers) {
            Imgproc.line(binaryMat, new Point(0, y), new Point(originalWidth, y), new Scalar(255, 0, 0), 4);
        }

        for (Integer y : lowers) {
            Imgproc.line(binaryMat, new Point(0, y), new Point(originalWidth, y), new Scalar(0, 255, 0), 4);
        }

//        toPrint = new Mat(binaryMat, Range.all());
//        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//        HighGui.imshow("reduced", toPrint);
//        HighGui.waitKey(0);


        final Double[] heightsFiltered = heights.stream()
                .filter(height -> height >= 15 && height <= 100)
                .toArray(Double[]::new);

        final double[] modes = StatUtils.mode(ArrayUtils.toPrimitive(heightsFiltered));

        if (modes.length > 0) {
            log.debug("Mode line height: {}", modes[0]);
            return modes[0];
        }
        log.debug("Could not determine line height");
        return null;
    }
}
