package com.mszust.receiptscanner.utils.opencv;

import lombok.Value;
import org.opencv.core.Mat;

/**
 * 1D array of 256 elements representing histogram
 */
@Value(staticConstructor = "of")
public class Histogram {
    Mat histogram;

    public double[] values() {
        final double[] values = new double[256];

        for (int row = 0; row < histogram.rows(); row++) {
            double value = histogram.get(row, 0)[0];
            values[row] = value;
        }

        return values;
    }
}
