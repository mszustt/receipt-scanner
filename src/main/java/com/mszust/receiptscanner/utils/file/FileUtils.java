package com.mszust.receiptscanner.utils.file;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class FileUtils {

    public static String getFileName(File file) {
        return FilenameUtils.removeExtension(file.getName());
    }

    public static List<String> readFile(final String location) throws URISyntaxException, IOException {
        final var file = file(location);
        return readFile(file);
    }

    public static List<String> readFile(final File file) throws IOException {
        return Files.readAllLines(file.toPath());
    }

    public static File file(final String location) throws URISyntaxException {
        return new File(FileUtils.class.getResource(location).toURI());
    }

    public static BufferedImage readImage(final String location) throws URISyntaxException, IOException {
        return ImageIO.read(file(location));
    }

    public static List<String> trimAndReplaceWhitespaces(List<String> file) {
        return file.stream()
                .map(
                        line -> line.trim().replaceAll(" +", " ")
        ).collect(Collectors.toList());
    }

}
