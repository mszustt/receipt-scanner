package com.mszust.receiptscanner.utils.diff.line;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Score {
    String originalLine;
    String scannedLine;
    int scannedLineNo;
    Double score;
    Double matchPercentage;
}
