package com.mszust.receiptscanner.utils.diff;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MultipleDiffResult {
    private final List<SingleDiffResult> diffResultList = new ArrayList<>();
    private String htmlReport;

    public MultipleDiffResult add(SingleDiffResult singleDiffResult) {
        this.diffResultList.add(singleDiffResult);
        return this;
    }

    public MultipleDiffResult addAll(List<SingleDiffResult> singleDiffResults) {
        this.diffResultList.addAll(singleDiffResults);
        return this;
    }
}
