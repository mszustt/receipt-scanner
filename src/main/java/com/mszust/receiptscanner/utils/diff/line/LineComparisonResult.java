package com.mszust.receiptscanner.utils.diff.line;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalTime;
import java.util.List;

@Data
@RequiredArgsConstructor
public class LineComparisonResult {
    private final String fileName;
    private final String benchmarkFileName;
    private final List<LineComparison> lines;
    private LocalTime duration;
    private long durationMilis;

    public double getOverallMatch() {
        final double charCount = lines.stream()
                .map(LineComparison::getOriginalLine)
                .map(String::trim)
                .mapToInt(String::length)
                .sum();
        return lines.stream()
                .mapToDouble(LineComparison::getScore)
                .sum()
                / charCount;
    }

    public double getOverallPercentage() {
        return lines.stream()
                .mapToDouble(LineComparison::getMatchPercentage)
                .average()
                .orElse(0d);
    }

    public void setDuration(Long millis) {
        this.durationMilis = millis;
        final int seconds = (int) (millis / 1000);
        final int minutes = seconds / 60;
        final int hours = minutes / 60;

        this.duration = LocalTime.of(hours, minutes, seconds % 60);
//        this.ocrDuration = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalTime();
    }
}
