package com.mszust.receiptscanner.utils.diff;

import com.github.difflib.text.DiffRow;
import com.mszust.receiptscanner.utils.diff.line.LineComparison;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SingleDiffResult {
    private Long duration;
    private String fileName;
    private List<LineComparison> lineComparisons;
    private List<DiffRow> diffRows;
}
