package com.mszust.receiptscanner.utils.diff.html;

import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import j2html.TagCreator;
import j2html.attributes.Attr;
import j2html.tags.ContainerTag;
import j2html.tags.DomContent;
import j2html.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static j2html.TagCreator.body;
import static j2html.TagCreator.button;
import static j2html.TagCreator.div;
import static j2html.TagCreator.document;
import static j2html.TagCreator.each;
import static j2html.TagCreator.h2;
import static j2html.TagCreator.head;
import static j2html.TagCreator.html;
import static j2html.TagCreator.img;
import static j2html.TagCreator.join;
import static j2html.TagCreator.p;
import static j2html.TagCreator.script;
import static j2html.TagCreator.table;
import static j2html.TagCreator.tbody;
import static j2html.TagCreator.td;
import static j2html.TagCreator.th;
import static j2html.TagCreator.thead;
import static j2html.TagCreator.tr;

@Component
public class HtmlReportBuilder {


    public String generateDocument(LineComparisonResult lineComparisonResult) {
        return generateDocument(Collections.singletonList(lineComparisonResult));
    }

    public String generateDocument(List<LineComparisonResult> lineComparisonResults) {

        final var firstRow = new FirstRowIndicator();
        return document(
                html(
                        head(
                                bootstrapCss()
                        ),
                        body(
                                div(
                                        each(lineComparisonResults, lineComparisonResult -> {
                                                    firstRow.setFirstRow(true);
                                                    return div(
                                                            div(
                                                                    h2(
                                                                            button(
                                                                                    join(lineComparisonResult.getFileName(),
                                                                                            p(lineComparisonResult.getDuration().getHour() + ":"
                                                                                                    + lineComparisonResult.getDuration().getMinute() + ":"
                                                                                                    + lineComparisonResult.getDuration().getSecond()),
                                                                                            p("Overall %: " + lineComparisonResult.getOverallPercentage()))
                                                                            ).withClass("btn btn-link collapsed")
                                                                                    .withType("button")
                                                                                    .attr("data-toggle", "collapse")
                                                                                    .attr("data-target", "#collapse-" + lineComparisonResult.getFileName())
                                                                    ).withClass("mb-0")
                                                            ).withClass("card-header").withId(lineComparisonResult.getFileName()),
                                                            div(
                                                                    table(
                                                                            thead(
                                                                                    tr(
                                                                                            th("image"),
                                                                                            th(lineComparisonResult.getFileName()),
                                                                                            th("recognized"),
                                                                                            th("match %")
                                                                                    )
                                                                            ),
                                                                            tbody(
                                                                                    each(lineComparisonResult.getLines(), row -> {
                                                                                        final List<ContainerTag> tds = new ArrayList<>();
                                                                                        if (firstRow.isFirstRow()) {
                                                                                            tds.add(td(
                                                                                                    img().withSrc("../" + lineComparisonResult.getFileName() + ".jpeg")
                                                                                                            .attr(Attr.HEIGHT, "1200px")
                                                                                                            .attr(Attr.WIDTH, "600px")
                                                                                            ).attr(Attr.ROWSPAN, lineComparisonResult.getLines().size()));
                                                                                            firstRow.setFirstRow(false);
                                                                                        }
                                                                                        tds.add(td(row.getOriginalLine()));
                                                                                        tds.add(td(row.getScannedLine()));
                                                                                        tds.add(td(row.getMatchPercentage().toString()));
                                                                                        return new ContainerTag("tr").with(tds);
                                                                                    })
                                                                            )
                                                                    ).withClass("card-body")
                                                            ).withClasses("collapse", "show")
                                                                    .withId("collapse-" + lineComparisonResult.getFileName())
                                                                    .attr("data-parent", "#resultAccordion")
                                                    ).withClass("card");
                                                }
                                        )
                                ).withClass("accordion").withId("resultAccordion"),
                                bootstrapJs()
                        )
                )
        );
    }

    private Tag<?> bootstrapCss() {
        return TagCreator.link()
                .withRel("stylesheet")
                .withHref("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css")
                .attr("integrity", "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T")
                .attr("crossorigin", "anonymous");
    }

    private DomContent bootstrapJs() {
        return join(script()
                        .withSrc("https://code.jquery.com/jquery-3.3.1.slim.min.js")
                        .attr("integrity", "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo")
                        .attr("crossorigin", "anonymous"),
                script()
                        .withSrc("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js")
                        .attr("integrity", "sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1")
                        .attr("crossorigin", "anonymous"),
                script()
                        .withSrc("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js")
                        .attr("integrity", "sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM")
                        .attr("crossorigin", "anonymous")
        );
    }

    @Getter
    @Setter
    private static class FirstRowIndicator {
        private boolean isFirstRow = true;
    }
}
