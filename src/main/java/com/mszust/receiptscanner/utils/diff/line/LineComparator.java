package com.mszust.receiptscanner.utils.diff.line;

import com.mszust.receiptscanner.utils.diff.Text;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.abs;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Simple line comparator, calculating exact matches of characters in lines.
 * Performs simple line matching, as some lines might have been shifted, which does not
 * necessarily mean that they're scanned wrongly.
 */
@Component
public class LineComparator {
    /**
     * Determines many lines before and after line n should be checked for match.
     */
    private static final int NEIGHBOUR_LINE_COUNT = 15;

    public LineComparisonResult compareLines(String fileName, List<String> originalLines, List<String> scannedLines) {
        return compareLines(fileName, "", originalLines, scannedLines);
    }

    public LineComparisonResult compareLines(String fileName, String benchmarkFile, List<String> originalLines, List<String> scannedLines) {
        final Text original = new Text(originalLines);
        final Text scanned = new Text(scannedLines);

        return matchLines(fileName, benchmarkFile, original, scanned);
    }

    private LineComparisonResult matchLines(String fileName, String benchmarkFile, Text original, Text scanned) {
        final List<LineComparison> lineComparisons = new ArrayList<>();
        final int originalLineCount = original.getLines().keySet().size();
        final int scannedLineCount = scanned.getLines().keySet().size();

        for (int i = 0; i < originalLineCount; i++) {
            final List<Score> scores = matchNeighbourLines(original, scanned, scannedLineCount, i);

            final Score bestScore = getBestScore(scores);
            if (bestScore.getMatchPercentage() < 50) {
                lineComparisons.add(
                        LineComparison.builder()
                                .line(i)
                                .originalLine(original.get(i))
                                .scannedLine(scanned.get(i))
                                .score(0d)
                                .matchPercentage(0d)
                                .build()
                );
            } else {
                lineComparisons.add(
                        LineComparison.builder()
                                .line(i)
                                .originalLine(bestScore.getOriginalLine())
                                .scannedLine(bestScore.getScannedLine())
                                .score(bestScore.getScore())
                                .matchPercentage(bestScore.getMatchPercentage())
                                .directMatch(true)
                                .build()
                );
                scanned.markMatched(bestScore.getScannedLineNo());
            }
        }

        return new LineComparisonResult(fileName, benchmarkFile, lineComparisons);
    }

    private LineComparisonResult matchLinesRecursive(String fileName, String benchmarkFile, Text original, Text scanned) {
        final List<LineComparison> lineComparisons = new ArrayList<>();
        final int originalLineCount = original.getLines().keySet().size();
        final int scannedLineCount = scanned.getLines().keySet().size();
        final List<Integer> linesToRematch = new ArrayList<>();

        for (int i = 0; i < originalLineCount; i++) {
            final List<Score> scores = matchNeighbourLines(original, scanned, scannedLineCount, i);

            final Score bestScore = getBestScore(scores);
            if (bestScore.getMatchPercentage() < 70) {
                linesToRematch.add(i);
            } else {
                lineComparisons.add(
                        LineComparison.builder()
                                .line(i)
                                .originalLine(bestScore.getOriginalLine())
                                .scannedLine(bestScore.getScannedLine())
                                .score(bestScore.getScore())
                                .matchPercentage(bestScore.getMatchPercentage())
                                .directMatch(true)
                                .build()
                );
                scanned.markMatched(bestScore.getScannedLineNo());
            }
        }

        final List<Integer> linesMatched = new ArrayList<>();
        for (Integer i : linesToRematch) {
            final List<Score> scores = matchNeighbourLines(original, scanned, scannedLineCount, i);

            final Score bestScore = getBestScore(scores);
            if (bestScore.getMatchPercentage() > 50) {
                lineComparisons.add(
                        LineComparison.builder()
                                .line(i)
                                .originalLine(bestScore.getOriginalLine())
                                .scannedLine(bestScore.getScannedLine())
                                .score(bestScore.getScore())
                                .matchPercentage(bestScore.getMatchPercentage())
                                .directMatch(true)
                                .build()
                );
                linesMatched.add(i);
                scanned.markMatched(bestScore.getScannedLineNo());
            }
        }
        linesToRematch.removeAll(linesMatched);

        for (Integer i : linesToRematch) {
            final List<Score> scores = matchNeighbourLines(original, scanned, scannedLineCount, i);

            final Score bestScore = getBestScore(scores);
            if (bestScore.getMatchPercentage() > 30) {
                lineComparisons.add(
                        LineComparison.builder()
                                .line(i)
                                .originalLine(bestScore.getOriginalLine())
                                .scannedLine(bestScore.getScannedLine())
                                .score(bestScore.getScore())
                                .matchPercentage(bestScore.getMatchPercentage())
                                .directMatch(true)
                                .build()
                );
                linesMatched.add(i);
                scanned.markMatched(bestScore.getScannedLineNo());
            }
        }
        linesToRematch.removeAll(linesMatched);

        for (Integer i : linesToRematch) {
            final Score directMatch = fuzzyCompare(i, original.get(i), scanned.get(i));
            lineComparisons.add(
                    LineComparison.builder()
                            .line(i)
                            .originalLine(directMatch.getOriginalLine())
                            .scannedLine(directMatch.getScannedLine())
                            .score(directMatch.getScore())
                            .matchPercentage(directMatch.getMatchPercentage())
                            .build()
            );
            scanned.markMatched(i);
        }
        return new LineComparisonResult(fileName, benchmarkFile, lineComparisons);
    }

    private List<Score> matchNeighbourLines(Text original, Text scanned, int scannedLineCount, int currentLineNo) {
        final List<Score> scores = new ArrayList<>();
        for (int j = 1; j <= NEIGHBOUR_LINE_COUNT; j++) {
            if (currentLineNo - j > 0) {
                scores.add(fuzzyCompare(currentLineNo - j, original.get(currentLineNo), scanned.get(currentLineNo - j)));
            }
            if (currentLineNo + j < scannedLineCount) {
                scores.add(fuzzyCompare(currentLineNo + j, original.get(currentLineNo), scanned.get(currentLineNo + j)));
            } else {
                scores.add(new Score(original.get(currentLineNo), "", currentLineNo + j, 0d, 0d));
            }
        }
        scores.add(fuzzyCompare(currentLineNo, original.get(currentLineNo), scanned.get(currentLineNo)));

        return scores;
    }

    /**
     * Compares single lines. Assigns 1 point for exact match, subtracts 0.5 point for each redundant character.
     *
     * @param original
     * @param scanned
     * @return
     */
    private Score compareLine(int lineNo, String original, String scanned) {
        double score = 0;
        int matched = 0;

        if (isBlank(scanned)) {
            return Score.builder()
                    .originalLine(original)
                    .scannedLine("")
                    .scannedLineNo(lineNo)
                    .score(0d)
                    .matchPercentage(0d)
                    .build();
        }

        for (int i = 0; i < original.length(); i++) {
            try {
                if (original.charAt(i) == scanned.charAt(i)) {
                    matched++;
                }
            } catch (IndexOutOfBoundsException e) {
                continue;
            }
        }

        score = matched;

        if (scanned.length() - original.length() != 0) {
            score = score - (abs((scanned.length() - original.length())) * 0.5);
        }

        return Score.builder()
                .originalLine(original)
                .scannedLine(scanned)
                .scannedLineNo(lineNo)
                .matchPercentage(score / original.length())
                .score(score)
                .build();
    }

    private Score fuzzyCompare(int lineNo, String original, String scanned) {
        if (original == null || isBlank(scanned)) {
            return Score.builder()
                    .originalLine(original)
                    .scannedLine(scanned)
                    .scannedLineNo(lineNo)
                    .matchPercentage(0d)
                    .score(0d)
                    .build();
        }
        final int matchPercentage = FuzzySearch.ratio(original, scanned);

        return Score.builder()
                .originalLine(original)
                .scannedLine(scanned)
                .scannedLineNo(lineNo)
                .matchPercentage((double) matchPercentage)
                .score((double) matchPercentage)
                .build();
    }

    private Score getBestScore(List<Score> scores) {
        return Collections.max(scores, Comparator.comparing(Score::getScore));
    }

}
