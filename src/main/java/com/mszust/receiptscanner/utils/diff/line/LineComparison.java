package com.mszust.receiptscanner.utils.diff.line;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class LineComparison {
    int line;
    String originalLine;
    String scannedLine;
    Double score;
    Double matchPercentage;
    boolean directMatch;
}
