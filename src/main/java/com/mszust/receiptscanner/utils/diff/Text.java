package com.mszust.receiptscanner.utils.diff;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map-like data structure, where each line has its number and it is possible to insert line before or after n-th line
 * Starting with line 0
 * Same line cannot be fetched twice.
 */
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Text {
    @Getter
    private Map<Integer, String> lines = new HashMap<>();
    private List<Integer> usedLines = new ArrayList<>();

    public Text(List<String> lineList) {
        lineList.forEach(line -> lines.put(nextFreeIndex(), line));
    }

    public Text push(String line) {
        this.lines.put(nextFreeIndex(), line);
        return this;
    }

    public String get(Integer index) {
        return usedLines.contains(index) ?
                "" :
                this.lines.getOrDefault(index, "");
    }

    public Text insertAtIndex(Integer index, String line) {
        final Map<Integer, String> newLines = new HashMap<>();

        this.lines.keySet().forEach(key -> {
            if (key < index) {
                newLines.put(key, lines.get(key));
            }
            if (key >= index) {
                newLines.put(key + 1, lines.get(key));
            }
        });

        newLines.put(index, line);
        this.lines = newLines;
        return this;
    }

    public void markMatched(Integer index) {
        this.usedLines.add(index);
    }

    private Integer nextFreeIndex() {
        return this.lines.size();
    }

}
