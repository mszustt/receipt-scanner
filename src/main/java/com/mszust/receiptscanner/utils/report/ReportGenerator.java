package com.mszust.receiptscanner.utils.report;

import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.report.csv.CsvReportBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ReportGenerator {
    private final HtmlReportBuilder htmlReportBuilder;
    private final CsvReportBuilder csvReportBuilder;


    public String generateHtmlReport(List<LineComparisonResult> lineComparisonResults) {
        return htmlReportBuilder.generateDocument(lineComparisonResults);
    }

    public String generateCsvReport(List<LineComparisonResult> lineComparisonResults) {
        return csvReportBuilder.generateReport(lineComparisonResults);
    }
}
