package com.mszust.receiptscanner.utils.report.csv;

import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.List;

@Component
public class CsvReportBuilder {
    private static final String[] HEADERS = {"file", "benchmark", "precision", "time"};

    @SneakyThrows
    public String generateReport(List<LineComparisonResult> lineComparisonResults) {
        double sum = 0;
        long timeSum = 0;
        int resultCount = lineComparisonResults.size();
        final StringWriter stringWriter = new StringWriter();
        final var csvPrinter = new CSVPrinter(
                stringWriter,
                CSVFormat.DEFAULT.withHeader(HEADERS)
        );

        for (LineComparisonResult result : lineComparisonResults) {
            sum += result.getOverallPercentage();
            timeSum += result.getDurationMilis();
            csvPrinter.printRecord(
                    result.getFileName(),
                    result.getBenchmarkFileName(),
                    result.getOverallPercentage(),
                    result.getDuration()
            );
        }

        csvPrinter.printRecord(
                "total",
                String.format("count = %d", resultCount),
                String.valueOf(sum / resultCount),
                String.valueOf(timeSum / resultCount)
        );

        return stringWriter.toString();
    }
}
