package com.mszust.receiptscanner.utils.imagej;

import ij.ImagePlus;
import ij.process.ImageConverter;
import lombok.experimental.UtilityClass;
import org.springframework.lang.Nullable;

import java.awt.*;
import java.awt.image.BufferedImage;

@UtilityClass
public class ImageJUtils {

    public BufferedImage rescale(BufferedImage image, Integer dpi) {
        return ScaleToDpi.scaleToDPI(
                toImagePlus(image),
                dpi,
                ScaleToDpi.Method.BILINEAR
        )
                .getBufferedImage();
    }

    public BufferedImage threshold(BufferedImage image, AutoThreshold.Method method) {
        final ImagePlus imagePlus = toImagePlus(image);
        toGrayscale(imagePlus);

        Object[] result = AutoThreshold.exec(
                imagePlus,
                method.getMethod(),
                false,
                false,
                true,
                false,
                false,
                false
        );
        if (result.length < 2 || result[1] == null) {
            return null;
        }
        final ImagePlus processed = (ImagePlus) result[1];
        return binary8BitTo1Bit(processed.getBufferedImage());
    }

    /**
     * ImageJ returns binary image as 8bit [0,255], having only one of two values.
     *
     * @param image
     * @return
     */
    public BufferedImage binary8BitTo1Bit(BufferedImage image) {
        if (image.getType() == BufferedImage.TYPE_BYTE_GRAY) {
            final BufferedImage binary = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
            Graphics g = binary.getGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();
            return binary;
        }
        return image;
    }

    @Nullable
    public BufferedImage localThreshold(BufferedImage image, AutoLocalThreshold.Method method) {
        return localThreshold(image, method, null);
    }

    @Nullable
    public BufferedImage localThreshold(BufferedImage image, AutoLocalThreshold.Method method, @Nullable AutoLocalThreshold.Config config) {
        final ImagePlus imagePlus = toImagePlus(image);
        toGrayscale(imagePlus);

        final ImagePlus processed = AutoLocalThreshold.localThreshold(imagePlus, method, config);

        if (processed == null) {
            return null;
        }

        return binary8BitTo1Bit(processed.getBufferedImage());
    }

    /**
     * defualt k = 0.5, default r = 128
     */
    public BufferedImage sauvolaLocalThreshold(BufferedImage image, double k, double r) {
        final ImagePlus imagePlus = toImagePlus(image);
        toGrayscale(imagePlus);

        ImagePlus processed = AutoLocalThreshold.localThreshold(
                imagePlus,
                AutoLocalThreshold.Method.SAUVOLA,
                new AutoLocalThreshold.Config(
                        10, k, r
                )
        );
        return processed.getBufferedImage();
    }


    public static void toGrayscale(ImagePlus imagePlus) {
        ImageConverter imageConverter = new ImageConverter(imagePlus);
        imageConverter.convertToGray8();
        imagePlus.updateAndDraw();
    }

    private static ImagePlus toImagePlus(BufferedImage image) {
        return new ImagePlus("file", image);
    }


}
