package com.mszust.receiptscanner.utils.imagej;

import ij.ImagePlus;
import ij.gui.Overlay;
import ij.measure.Calibration;
import ij.process.ImageProcessor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;

import java.awt.*;

@UtilityClass
public class ScaleToDpi {

    public ImagePlus scaleToDPI(ImagePlus imagePlus, int scale, Method method) {
        final ImagePlus imp = imagePlus;
        final Integer dpi = scale;
        double wPix = imp.getWidth();
        double hPix = imp.getHeight();
        double wInches = wPix / 600;
        double hInches = hPix / 600;

        // compute width and height in pixels from DPI
        wPix = wInches * dpi;
        hPix = hInches * dpi;

        // rescale image
        final StringBuilder arg = new StringBuilder();
        arg.append("x=- y=-");
        arg.append(" width=").append(wPix);
        arg.append(" height=").append(hPix);
        arg.append(" interpolation=").append(method.getMethod());
//        IJ.run(imp, "Scale...", arg.toString());
//        final ImagePlus rescaled = Scaler.resize(
//                imp,
//                wPix,
//                hPix,
//                1,
//                "average bilinear"
//        );
        final ImagePlus rescaled = scale(imp, (int) wPix, (int) hPix);

        // update calibration
        final ImagePlus newImp = rescaled;
        double pixelSize = 1 / dpi;
        final Calibration cal = newImp.getCalibration();
        cal.setUnit("inch");
        cal.pixelWidth = pixelSize;
        cal.pixelHeight = pixelSize;
        cal.pixelDepth = 1;
        newImp.updateAndDraw();
        return newImp;
    }

    private ImagePlus scale(ImagePlus imp, int wPix, int hPix) {

        ImageProcessor ip = imp.getProcessor();
        Rectangle r = ip.getRoi();
        ImagePlus imp2 = imp.createImagePlus();
        imp2.setProcessor("title", ip.resize(wPix, hPix, true));
        Calibration cal = imp2.getCalibration();
        if (cal.scaled()) {
            cal.pixelWidth *= 1.0 / 1.0;
            cal.pixelHeight *= 1.0 / 1.0;
        }
        Overlay overlay = imp.getOverlay();
        if (overlay != null && !imp.getHideOverlay())
            imp2.setOverlay(overlay.scale(1.0, 1.0));
//        imp2.show();
        imp.trimProcessor();
        imp2.trimProcessor();
        imp2.changes = true;
        return imp2;
    }

    @RequiredArgsConstructor
    @Getter
    public enum Method {
        NONE("None"), BILINEAR("Bilinear"), BICUBIC("Bicubic");

        private final String method;
    }
}
