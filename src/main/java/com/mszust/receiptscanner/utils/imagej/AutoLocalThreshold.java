package com.mszust.receiptscanner.utils.imagej;

import fiji.threshold.Auto_Local_Threshold;
import ij.ImagePlus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import org.springframework.lang.Nullable;

import java.util.Map;

import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Config.DEFAULT_CONFIG;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.BERNSEN;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.CONTRAST;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.MEAN;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.MEDIAN;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.MID_GREY;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.NIBLACK;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.OTSU;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.PHANSALKAR;
import static com.mszust.receiptscanner.utils.imagej.AutoLocalThreshold.Method.SAUVOLA;

@UtilityClass
public class AutoLocalThreshold {
    private static final Auto_Local_Threshold AUTO_LOCAL_THRESHOLD = new Auto_Local_Threshold();


    @RequiredArgsConstructor
    @Getter
    public enum Method {
        BERNSEN("Bernsen"),
        CONTRAST("Contrast"),
        MEAN("Mean"),
        MEDIAN("Median"),
        MID_GREY("MidGrey"),
        NIBLACK("Niblack"),
        OTSU("Otsu"),
        PHANSALKAR("Phansalkar"),
        SAUVOLA("Sauvola");

        private final String method;
    }

    @Nullable
    static ImagePlus localThreshold(ImagePlus imagePlus, Method method, boolean overrideDefaults) {

        Config config;
        if (overrideDefaults) {
            config = getConfigFor(method);
        } else {
            config = DEFAULT_CONFIG;
        }

        return localThreshold(imagePlus, method, config);
    }

    @Nullable
    static ImagePlus localThreshold(ImagePlus imagePlus, Method method, Config config) {
        Object[] result = execLocalThreshold(
                imagePlus,
                method,
                config != null
                        ? config
                        : DEFAULT_CONFIG
        );
        if (result.length < 1 || result[0] == null) {
            return null;
        }
        return (ImagePlus) result[0];
    }

    private static Object[] execLocalThreshold(ImagePlus imagePlus, Method method, Config config) {
        return AUTO_LOCAL_THRESHOLD.exec(
                imagePlus,
                method.getMethod(),
                config.getRadius(),
                config.getPar1(),
                config.getPar2(),
                true
        );
    }

    @Getter
    @RequiredArgsConstructor
    public static class Config {
        private final int radius;
        private final double par1;
        private final double par2;

        public static Config DEFAULT_CONFIG = new Config(15, 0, 0);
    }

    private static Config getConfigFor(Method method) {
        return Map.of(
                BERNSEN, new Config(0, 0, 0),
                CONTRAST, new Config(0, 0, 0),
                MEAN, new Config(0, 0, 0),
                MEDIAN, new Config(0, 0, 0),
                MID_GREY, new Config(0, 0, 0),
                NIBLACK, new Config(0, 0, 0),
                OTSU, new Config(0, 0, 0),
                PHANSALKAR, new Config(0, 0, 0),
                SAUVOLA, new Config(0, 0, 0)
        ).getOrDefault(method, null);
    }
}







