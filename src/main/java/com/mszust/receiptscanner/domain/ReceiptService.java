package com.mszust.receiptscanner.domain;

import com.mszust.receiptscanner.ocr.OcrEngine;
import com.mszust.receiptscanner.ocr.model.ScanResult;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.ocr.service.OcrService;
import com.mszust.receiptscanner.preprocessing.Preprocessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.awt.image.BufferedImage;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReceiptService {

    private final List<OcrService> ocrServices;

    public ScanResult scanReceipt(Scannable scannable, OcrEngine ocrEngine, boolean preprocessing) {
        log.debug("Scanning file {} with engine {} and preprocessing {}", scannable.getFileName(), ocrEngine, preprocessing);
        final OcrService service = getServiceFor(ocrEngine);

        if (preprocessing) {
            final var stopWatch = new StopWatch();
            stopWatch.start();
            final BufferedImage preprocessed = Preprocessor.preprocess(scannable);
            stopWatch.stop();
            final var preprocessingDuration = stopWatch.getTotalTimeMillis();

            return service.processFile(new Scannable(preprocessed, scannable.getFileName()))
                    .withPreprocessingDuration(preprocessingDuration);
        }
        return service.processFile(scannable);
    }


    private OcrService getServiceFor(OcrEngine ocrEngine) {
        return ocrServices.stream()
                .filter(service -> service.getOcrEngineName().equals(ocrEngine))
                .findAny()
                .orElseThrow();
    }
}
