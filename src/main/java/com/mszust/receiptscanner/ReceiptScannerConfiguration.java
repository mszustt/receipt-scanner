package com.mszust.receiptscanner;

import nu.pattern.OpenCV;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
public class ReceiptScannerConfiguration {

    static {
        OpenCV.loadShared();
    }
}
