package com.mszust.receiptscanner.ocr.engine;

import com.mszust.receiptscanner.ocr.model.Scannable;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

@Slf4j
public class Tess4jOcr {
    private static final String LANGUAGE_POLISH = "pol";
    private final static ITesseract INSTANCE = new Tesseract();  // JNA Interface Mapping

    public Tess4jOcr(final Tess4jConfiguration configuration, TrainingDataType trainingDataType) {
        this.setTrainingData(configuration, trainingDataType);
        INSTANCE.setLanguage(LANGUAGE_POLISH);
    }

    public String doOcr(final Scannable scannable) throws TesseractException {
        if (scannable.isFile()){
            return INSTANCE.doOCR(scannable.getFile());
        }
        return INSTANCE.doOCR(scannable.getBufferedImage());
    }

    public void setTrainingData(Tess4jConfiguration configuration, TrainingDataType trainingData) {
        switch (trainingData) {
            case FAST:
                log.debug("Using training data {}", configuration.getDataPathFast());
                INSTANCE.setDatapath(configuration.getDataPathFast());
                break;
            case MIXED:
                log.debug("Using training data {}", configuration.getDataPathMixed());
                INSTANCE.setDatapath(configuration.getDataPathMixed());
                break;
            case TRAINED:
                log.debug("Using training data {}", configuration.getDataPathTrained());
                INSTANCE.setDatapath(configuration.getDataPathTrained());
                break;
            default:
                log.debug("Using training data {}", configuration.getDataPathBest());
                INSTANCE.setDatapath(configuration.getDataPathBest());
        }
    }

    public void setDpi(Integer dpi) {
        INSTANCE.setTessVariable("user_defined_dpi", String.valueOf(dpi));
    }

    public enum TrainingDataType {
        BEST, MIXED, FAST, TRAINED
    }
}
