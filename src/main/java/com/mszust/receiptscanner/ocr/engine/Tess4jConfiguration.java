package com.mszust.receiptscanner.ocr.engine;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "tess")
@Getter
@Setter
public class Tess4jConfiguration {
//    private static final String TESSERACT_PATH = "/usr/share/tesseract-ocr/4.00/tessdata";
//    public static final String DATA_PATH_BEST = "/home/user/Code/moje/receipt-scanner/src/main/resources/tess-data/best";
//    public static final String DATA_PATH_MIXED = "/home/user/Code/moje/receipt-scanner/src/main/resources/tess-data/mixed";
//    public static final String DATA_PATH_FAST = "/home/user/Code/moje/receipt-scanner/src/main/resources/tess-data/fast";

    private String tesseractBinariesPath;
    private String dataPathBest;
    private String dataPathMixed;
    private String dataPathFast;
    private String dataPathTrained;

}
