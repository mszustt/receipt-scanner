package com.mszust.receiptscanner.ocr.model;

import com.mszust.receiptscanner.ocr.OcrEngine;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.List;

@Value
@Builder
public class ScanResult {
    String fileName;
    List<String> recognizedLines;
    Long ocrDuration;
    OcrEngine ocrEngine;
    @With
    Long preprocessingDuration;
}
