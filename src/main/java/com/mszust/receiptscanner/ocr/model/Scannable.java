package com.mszust.receiptscanner.ocr.model;

import lombok.EqualsAndHashCode;
import org.apache.commons.io.FilenameUtils;

import java.awt.image.BufferedImage;
import java.io.File;

@EqualsAndHashCode
public class Scannable {
    private File file;

    private BufferedImage bufferedImage;
    private String fileName;


    public Scannable(File file) {
        this.file = file;
    }

    public Scannable(BufferedImage bufferedImage, String fileName) {
        this.bufferedImage = bufferedImage;
        this.fileName = fileName;
    }

    public boolean isFile() {
        return file != null;
    }

    public String getFileName() {
        if (isFile()) {
            return FilenameUtils.removeExtension(file.getName());
        }
        return fileName;
    }

    public File getFile() {
        if (isFile()) {
            return file;
        }
        throw new IllegalArgumentException("Scannable is a buffered image, not a file");
    }

    public BufferedImage getBufferedImage() {
        if (isFile()) {
            throw new IllegalArgumentException("Scannable is a file, not a buffered image");
        }
        return bufferedImage;
    }
}
