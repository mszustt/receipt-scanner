package com.mszust.receiptscanner.ocr.service;

import com.mszust.receiptscanner.ocr.OcrEngine;
import com.mszust.receiptscanner.ocr.engine.Tess4jConfiguration;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.ScanResult;
import com.mszust.receiptscanner.ocr.model.Scannable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
@RequiredArgsConstructor
@Slf4j
public class TesseractOcr implements OcrService {

    private final Tess4jConfiguration tess4jConfiguration;
    private Tess4jOcr ocr;

    @PostConstruct
    public void setUpTesst4j() {
        ocr = new Tess4jOcr(tess4jConfiguration, Tess4jOcr.TrainingDataType.BEST);
    }

    @Override
    public OcrEngine getOcrEngineName() {
        return OcrEngine.TESSERACT;
    }

    @Deprecated //only for testing purposes
    public void setTrainingData(Tess4jOcr.TrainingDataType trainingData) {
        ocr.setTrainingData(tess4jConfiguration, trainingData);
    }

    @Deprecated //only for testing purposes
    public void setDpi(Integer dpi) {
        ocr.setDpi(dpi);
    }

    public ScanResult processFile(final File file) {
        return this.processFile(new Scannable(file));
    }

    @Override
    public ScanResult processFile(final Scannable scannable) {
        log.debug("Processing file: {}", scannable.getFileName());
        final var stopWatch = new StopWatch();
        stopWatch.start();
        final var ocrResult = processLines(scannable);
//        OcrResult ocrResult;
//
//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        Future future = executor.submit(() -> processLines(scannable));
//        try {
//            ocrResult = (OcrResult) future.get(30, TimeUnit.SECONDS);
//        } catch (TimeoutException | InterruptedException | ExecutionException e) {
//            future.cancel(true);
//            ocrResult = new OcrResult(List.of());
//        }
//

//        CompletableFuture<OcrResult> future = new CompletableFuture()
//                .completeOnTimeout(new OcrResult(List.of()), 30, TimeUnit.SECONDS);
//
//        Executors.newSingleThreadExecutor().submit(() -> {
//            final OcrResult result = processLines(scannable);
//            future.complete(result);
//            return null;
//        });
//
//        OcrResult ocrResult;
//        try {
//            ocrResult = future.get();
//        } catch (InterruptedException | ExecutionException e) {
//            log.warn("Exception: ", e);
//            ocrResult = new OcrResult(List.of());
//        }

        stopWatch.stop();
        final var ocrDuration = stopWatch.getTotalTimeMillis();
        log.debug("Finished processing file: {} in {}ms", scannable.getFileName(), ocrDuration);
        return ScanResult.builder()
                .ocrDuration(ocrDuration)
                .fileName(scannable.getFileName())
                .recognizedLines(ocrResult.lines)
                .ocrEngine(getOcrEngineName())
                .build();
    }

    private OcrResult processLines(final Scannable scannable) {
        final var singleResult = this.process(scannable);

        if (isNotBlank(singleResult)) {
            return new OcrResult(Arrays.asList(singleResult.split("\\r?\\n")));
        }

        return new OcrResult(emptyList());
    }

    public String process(final Scannable scannable) {
        try {
            return ocr.doOcr(scannable);
        } catch (TesseractException e) {
            return null;
        }
    }

    @Getter
    @Setter
    @RequiredArgsConstructor
    private class OcrResult {
        private final List<String> lines;
    }
}
