package com.mszust.receiptscanner.ocr.service;

import com.mszust.receiptscanner.ocr.OcrEngine;
import com.mszust.receiptscanner.ocr.model.ScanResult;
import com.mszust.receiptscanner.ocr.model.Scannable;

public interface OcrService {

    ScanResult processFile(final Scannable scannable);

    OcrEngine getOcrEngineName();
}
