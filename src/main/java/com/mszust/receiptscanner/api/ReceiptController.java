package com.mszust.receiptscanner.api;

import com.mszust.receiptscanner.api.model.OcrEngineDto;
import com.mszust.receiptscanner.api.model.ScanResultDto;
import com.mszust.receiptscanner.domain.ReceiptService;
import com.mszust.receiptscanner.ocr.OcrEngine;
import com.mszust.receiptscanner.ocr.model.ScanResult;
import com.mszust.receiptscanner.ocr.model.Scannable;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/receipts")
@RequiredArgsConstructor
@Slf4j
public class ReceiptController {

    private final ReceiptService receiptService;

    @Operation(summary = "Perform OCR on receipt")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully performed OCR.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ScanResultDto.class))),
            @ApiResponse(responseCode = "422", description = "Image could not be processed.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class))),
            @ApiResponse(responseCode = "415", description = "Incorrect format. Accepted formats are JPG/JPEG and PNG.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)))
    })
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ScanResultDto> ocrReceipt(@Parameter(description = "Receipt image in JPG/JPEG or PNG format.") @RequestParam("file") @NotNull MultipartFile file,
                                                    @Parameter(description = "OCR engine to be used.") @RequestHeader(name = "ocr-engine", defaultValue = "TESSERACT") OcrEngineDto ocrEngine,
                                                    @Parameter(description = "Boolean flag determining if image preprocessing should be applied before OCR.") @RequestHeader(name = "preprocessing", defaultValue = "true") boolean preprocessing) {

        validateFileType(file);
        return ResponseEntity.ok()
                .body(
                        toScanResultDto(
                                receiptService.scanReceipt(
                                        new Scannable(getBufferedImage(file), file.getOriginalFilename()),
                                        toOcrEngine(ocrEngine),
                                        preprocessing
                                )
                        )
                );
    }

    private void validateFileType(@RequestParam("file") MultipartFile file) {
        try {
            final String mediaType = new Tika().detect(file.getBytes());
            if (mediaType.equals(MediaType.IMAGE_JPEG_VALUE) || mediaType.equals(MediaType.IMAGE_PNG_VALUE)) {
                return;
            }
        } catch (IOException e) {
            log.warn("Could not detect media type");
        }
        throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    private static ScanResultDto toScanResultDto(ScanResult scanResult) {
        return ScanResultDto.builder()
                .ocrDuration(scanResult.getOcrDuration())
                .fileName(scanResult.getFileName())
                .ocrEngine(toOcrEngineDto(scanResult.getOcrEngine()))
                .recognizedLines(
                        String.join("\n", scanResult.getRecognizedLines())
                )
                .preprocessingDuration(scanResult.getPreprocessingDuration())
                .totalDuration(toTotalDuration(scanResult))
                .build();
    }

    private static Long toTotalDuration(ScanResult scanResult) {
        if (scanResult.getPreprocessingDuration() == null) {
            return scanResult.getOcrDuration();
        }
        return scanResult.getOcrDuration() + scanResult.getPreprocessingDuration();
    }

    private static BufferedImage getBufferedImage(@RequestParam("file") MultipartFile file) {
        try {
            return ImageIO.read(new ByteArrayInputStream(file.getBytes()));
        } catch (IOException e) {
            log.warn("Could not parse incoming image due to exception.", e);
            throw new ResponseStatusException(
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    "Could not parse image"
            );
        }
    }

    private static OcrEngine toOcrEngine(OcrEngineDto ocrEngine) {
        switch (ocrEngine) {
            case TESSERACT:
            default:
                return OcrEngine.TESSERACT;
        }
    }

    private static OcrEngineDto toOcrEngineDto(OcrEngine ocrEngine) {
        switch (ocrEngine) {
            case TESSERACT:
                return OcrEngineDto.TESSERACT;
            default:
                return null;
        }

    }

}
