package com.mszust.receiptscanner.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ScanResultDto {
    private String fileName;
    private Long ocrDuration;
    private OcrEngineDto ocrEngine;
    private String recognizedLines;
    private Long preprocessingDuration;
    private Long totalDuration;
}
