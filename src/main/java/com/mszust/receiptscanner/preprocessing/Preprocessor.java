package com.mszust.receiptscanner.preprocessing;

import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.opencv.*;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.awt.image.BufferedImage;
import java.io.File;

@UtilityClass
public class Preprocessor {

    public BufferedImage preprocess(Scannable scannable) {
        return scannable.isFile()
                ? preprocess(scannable.getFile())
                : preprocess(scannable.getBufferedImage());
    }

    @SneakyThrows
    public BufferedImage preprocess(final File file) {
        final Mat src = Imgcodecs.imread(file.getAbsolutePath());

        final Mat rescaled = cropAndScale(src);

        return OpenCvUtils.toBufferedImage(rescaled);
    }

    @SneakyThrows
    public BufferedImage preprocess(final BufferedImage bufferedImage) {
        final Mat src = OpenCvUtils.bufferedImageToMat(bufferedImage);

        final Mat rescaled = cropAndScale(src);

        return OpenCvUtils.toBufferedImage(rescaled);
    }

    @SneakyThrows
    public BufferedImage preprocess(final Mat mat) {
        final Mat rescaled = cropAndScale(mat);

        return OpenCvUtils.toBufferedImage(rescaled);
    }

    private static Mat cropAndScale(Mat src) {
        try {
            final Mat croppedMat = Cropping.cropImageExact(src);
            final Mat rescaled = rescale(croppedMat);
            return Contrast.clahe(rescaled);
        } catch (ExactCroppingFailedException e) {
            final Mat cropped = Cropping.cropImageApprox(src);
            return Contrast.clahe(cropped);
        }

    }

    public Mat rescale(Mat croppedMat) {
        final Double scale = findScale(croppedMat);

        if (scale == null || scale == 1.0) {
            return croppedMat;
        }
        return Scaling.rescale(croppedMat, scale);
    }

    private Double findScale(Mat croppedMat) {
        final Double lineHeight = LineHeight.findLineHeight(croppedMat);

        if (lineHeight == null) {
            return 1.0;
        }

        return Scaling.findScale(lineHeight);
    }

}
