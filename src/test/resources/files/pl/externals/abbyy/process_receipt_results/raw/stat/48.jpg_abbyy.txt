﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_Clockwise">
		<vendor>
			<name>
				<recognizedValue>
					<text>Lim sp. z o.o.</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[ sp. k.
Zegrzyńska 39
* 05-1-10 jabłonna <
& NIP 781-18-97-358 ?
202r03-06" *** ** ' nrwydr.1Ó64007
PARAGON FISKALNY
ŁOSOŚ WĘDŹ. 100G - JV 2 * 9,99 19,98 C
RABAT DO
. ŁOSOŚ WĘDZ�JQOG ./ % .,K -4,00?C
WODA MlRER.NlW.*� )t T2 * 1.89 22,68 A
KROJ.POMID. Z ZIÓŁ. F? 2 * 0,49 6,98'C
& KAWA NESCAFE GOLD �X 1 *27,09 27,99 A
% PLASTRY SENSIPLAST I 1 * 4,49 4,49 B
BAGIETKAlj0DfUO#3 F 1 * 2,49 2,49,C
BAGIETKA ~ F 1 * 2,49 2,49 C
PIR.SZYNKA Z IND., ■ F ,-1 * 2,-99 2,991C
PASTRAMI DELUXE F 1 * 6,99 6,99 C
TIRAMISU 85G F 2 * 5,49 10,98 C
POMID.MAL.POL.,LUZ F]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[sp. k.
Zegrzyńska 39
* 05-1-10 jabłonna <
& NIP 781-18-
 ?
202r03-06" *** ** ' nrwydr.1Ó64007
PARAGON FISKALNY
ŁOSOŚ WĘDŹ. 100G - JV 2 * 9,99 19,98 C
 DO
. ŁOSOŚ WĘDZ�JQOG ./ % .,K -4,00?C
WODA MlRER.NlW.*� )t T2 * 1.89 22,68 A
KROJ.POMID. Z ZIÓŁ. F? 2 * 0,49 6,98'C
& KAWA NESCAFE GOLD �X 1 *27,09 27,99 A
% PLASTRY SENSIPLAST I 1 * 4,49 4,49 B
BAGIETKAlj0DfUO#3 F 1 * 2,49 2,49,C
BAGIETKA ~ F 1 * 2,49 2,49 C
PIR.SZYNKA Z IND., ■ F ,-1 * 2,-99 2,991C
PASTRAMI DELUXE F 1 * 6,99 6,99 C
TIRAMISU 85G F 2 * 5,49 10,98 C
POMID.MAL.POL.,LUZ F]]></text>
			</address>
			<city>
				<normalizedValue>RABAT</normalizedValue>
				<recognizedValue>
					<text>RABAT</text>
				</recognizedValue>
			</city>
			<zip>
				<normalizedValue>62080</normalizedValue>
				<recognizedValue>
					<text>62
080</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7811897358</normalizedValue>
				<recognizedValue>
					<text>781-18-97-358</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-03-06</normalizedValue>
			<recognizedValue>
				<text>2021-03-06</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>14:18:00</normalizedValue>
			<recognizedValue>
				<text>14:18</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>136.22</normalizedValue>
			<recognizedValue>
				<text>136,22</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="2.6">
			<normalizedValue>0.99</normalizedValue>
			<recognizedValue>
				<text>2,6% F i * 0.99 0,99</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="1">
			<normalizedValue>3.49</normalizedValue>
			<recognizedValue>
				<text>1 * 3,49 3,49</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>136.22</normalizedValue>
				<recognizedValue>
					<text>136,22</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="23">
			<item index="1">
				<name>
					<text>ŁOSOŚ WĘDŹ. 100G - JV 2 * 9,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>19.98</normalizedValue>
					<recognizedValue>
						<text>19,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[ŁOSOŚ WĘDŹ. 100G - JV 2 * 9,99 19,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>. ŁOSOŚ WĘDZ�JQOG ./ % .,K </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.00</normalizedValue>
					<recognizedValue>
						<text>4,00?</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[. ŁOSOŚ WĘDZ�JQOG ./ % .,K 
4,00?]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>WODA MlRER.NlW.*� )t T2 * 1.89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>22.68</normalizedValue>
					<recognizedValue>
						<text>22,68</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[WODA MlRER.NlW.*� )t T2 * 1.89 22,68]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>KROJ.POMID. Z ZIÓŁ. F? 2 * 0,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>6.98</normalizedValue>
					<recognizedValue>
						<text>6,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[KROJ.POMID. Z ZIÓŁ. F? 2 * 0,49 6,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>&amp; KAWA NESCAFE GOLD �X 1 *27,09 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>27.99</normalizedValue>
					<recognizedValue>
						<text>27,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[& KAWA NESCAFE GOLD �X 1 *27,09 27,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>% PLASTRY SENSIPLAST I 1 * 4,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.49</normalizedValue>
					<recognizedValue>
						<text>4,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[% PLASTRY SENSIPLAST I 1 * 4,49 4,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>BAGIETKAlj0DfUO#3 F 1 * 2,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.49</normalizedValue>
					<recognizedValue>
						<text>2,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[BAGIETKAlj0DfUO#3 F 1 * 2,49 2,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>BAGIETKA ~ F 1 * 2,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.49</normalizedValue>
					<recognizedValue>
						<text>2,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[BAGIETKA ~ F 1 * 2,49 2,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="9">
				<name>
					<text>PIR.SZYNKA Z IND., ■ F ,-1 * 2,-99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.99</normalizedValue>
					<recognizedValue>
						<text>2,991</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PIR.SZYNKA Z IND., ■ F ,-1 * 2,-99 2,991]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="10">
				<name>
					<text>PASTRAMI DELUXE F 1 * 6,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>6.99</normalizedValue>
					<recognizedValue>
						<text>6,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PASTRAMI DELUXE F 1 * 6,99 6,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="11">
				<name>
					<text>TIRAMISU 85G F 2 * 5,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>10.98</normalizedValue>
					<recognizedValue>
						<text>10,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[TIRAMISU 85G F 2 * 5,49 10,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="12">
				<name>
					<text>K��-0,356 * 17,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>6.40</normalizedValue>
					<recognizedValue>
						<text>6,40</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[K��-0,356 * 17,99 6,40]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="13">
				<name>
					<text>KUBUŚ SOK ■p t 1 * 1,29 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.29</normalizedValue>
					<recognizedValue>
						<text>1,29</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[KUBUŚ SOK ■p t 1 * 1,29 1,29]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="14">
				<name>
					<text>JOGURT OWOC.2,6% F i * 0.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.99</normalizedValue>
					<recognizedValue>
						<text>0,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[JOGURT OWOC.2,6% F i * 0.99 0,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="15">
				<name>
					<text>PRALINYFERR.RÓCHER X 1 * 19.Ś9 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>19.99</normalizedValue>
					<recognizedValue>
						<text>19,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PRALINYFERR.RÓCHER X 1 * 19.Ś9 19,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="16">
				<name>
					<text>SUPER SMOOTHIE OWOC H 1 * 3,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.49</normalizedValue>
					<recognizedValue>
						<text>3,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUPER SMOOTHIE OWOC H 1 * 3,49 3,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="17">
				<name>
					<text>Sprzed, opod. PTU A Jj </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>70.66</normalizedValue>
					<recognizedValue>
						<text>70,66</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU A Jj 70,66]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="18">
				<name>
					<text>Kwota A pnnmfek </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>13.21</normalizedValue>
					<recognizedValue>
						<text>13,21</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota A pnnmfek 13,21]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="19">
				<name>
					<text>Sprzed, opod. PTU B ** </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.49</normalizedValue>
					<recognizedValue>
						<text>4,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU B ** 4,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="20">
				<name>
					<text>Kwota B 08,00% ™ </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.33</normalizedValue>
					<recognizedValue>
						<text>0,33</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota B 08,00% ™ 0,33]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="21">
				<name>
					<text>Sprzed, opod. PTU C </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>61.07</normalizedValue>
					<recognizedValue>
						<text>61,07</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU C 61,07]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="22">
				<name>
					<text>KwdtaC 05,00% A • P/T </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.90</normalizedValue>
					<recognizedValue>
						<text>2,9</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[KwdtaC 05,00% A • P/T 
2,9]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="23">
				<name>
					<text>Podafelt™� — - * </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>16.45</normalizedValue>
					<recognizedValue>
						<text>16,45</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Podafelt™� — - * 16,45]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[Lim sp. z o.o. sp. k.                                                  
Zegrzyńska 39                                                          
* 05-1-10 jabłonna <                                                   
& NIP 781-18-97-358          ?                                         
202r03-06" * * * **   ' n rwydr. 1Ó64007                               
PARAGON FISKALNY                                                       
ŁOSOŚ WĘDŹ. 100G     - JV 2 * 9,99 19,98 C                             
RABAT DO                                                               
. ŁOSOŚ WĘDZ^JQOG ./ % .,K           -4,00?C                           
WODA MlRER.NlW.*^ )t T2 * 1.89 22,68 A                                 
KROJ.POMID. Z ZIÓŁ. F? 2 * 0,49 6,98'C                                 
& KAWA NESCAFE GOLD ^X 1 *27,09 27,99 A                                
% PLASTRY SENSIPLAST     I   1 * 4,49 4,49 B                          
BAGIETKAlj0DfUO#3 F  1 * 2,49 2,49,C                                   
BAGIETKA ~           F   1 * 2,49 2,49 C                               
PIR.SZYNKA Z IND.,  ■    F , -1 * 2,-99 2,991C                         
PASTRAMI DELUXE        F   1 * 6,99 6,99 C                             
TIRAMISU 85G           F 2 * 5,49 10,98 C                              
POMID.MAL.POL.,LUZ     F                                               
k^^-0,356 * 17,99 6,40 C                                               
KUBUŚ SOK ■p t 1 * 1,29 1,29 C                                         
 JOGURT OWOC.2,6%       F   i * 0.99 0,99 C                           
PRALINYFERR.RÓCHER     X 1 * 19.Ś9 19,99 A                             
SUPER SMOOTHIE OWOC   H 1 * 3,49 3,49 C                                
Sprzed, opod. PTU A Jj 70,66                                           
 Kwota A pnnmfek             13,21                                     
Sprzed, opod. PTU B   **           4,49                                
Kwota B 08,00%         ™        0,33                                   
Sprzed, opod. PTU C                 61,07                              
KwdtaC 05,00% A • P/T -2,9V                                            
Podafelt™^ --- -       * 16,45                                         
SUMA PLN 136,22                                                        
 000175 #3    25 1726 rnr:275480 A14:18                               
5EABYiT13TC7.KPMpH-3HWBN-36GJB.                                        
^'66114145173:,':                                                      
Płatność            Karta-’p?atnicz< 136,22                            
RAZEM PLN Tlrnial 136/22                                               
Odrśs' siedziby sprzedawcy: 1. '1i a                                   
                      ul. Poznańska 48, Jankówice                      
W 62-080 Tarnowo-Podgórne                                              
' iiiiiii iii i 'li iii iw iii                                         
2021-03-06 Np ^®r wydr. 1064008                                        
NIEFISKALNY                                                            
Data transak(&Y **''    06103.2021 14.18                               
Do paragonu:175          .*8 Kasa:0003                                 
TID:31115376 jj^-UID; 000000000171009                                  
 Kasjer:   25 "                                                        
Karta:   VISA B                   PSN: 00                              
8Q8Qx TfTT ? / /Ważna do:00/00<                                        
Nr autoryzacji:   601917 1N ARC:00                                     
‘Sprzedaż:                               J&N                           
Proszę obciąży© moje' konto/                                           
AID:                  ^^0000000031010                                  
ATC:                                                                   
TC:                   u*.** i **• i pul 1                              
NIEFISKALNY I                                                          
#3*. 25 1726    ™                                                      
]]></recognizedText>
	</receipt>
</receipts>
