﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_Clockwise">
		<vendor>
			<name>
				<recognizedValue>
					<text>&quot;Largsas&quot;</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[. Skrzypczak karisa
Dom Handlowy Maxim
Ul. Siwińskiego 2
05-120 Legionowo]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[Skrzypczak karisa
Dom Handlowy Maxim
Ul. Siwińskiego 2
 Legionowo]]></text>
			</address>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>1250830387</normalizedValue>
				<recognizedValue>
					<text>125-083-03-87</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-03-06</normalizedValue>
			<recognizedValue>
				<text>2021-03-06</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>12:24:00</normalizedValue>
			<recognizedValue>
				<text>12:24</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>8.00</normalizedValue>
			<recognizedValue>
				<text>8,00</text>
			</recognizedValue>
		</total>
		<tax total="false">
			<normalizedValue>0.00</normalizedValue>
			<recognizedValue>
				<text>0,00</text>
			</recognizedValue>
		</tax>
		<payment type="Undefined">
			<value>
				<normalizedValue>8.00</normalizedValue>
				<recognizedValue>
					<text>8,00</text>
				</recognizedValue>
			</value>
		</payment>
		<recognizedItems count="2">
			<item index="1">
				<name>
					<text>SUMA PTU - </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.00</normalizedValue>
					<recognizedValue>
						<text>0,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU - 0,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>SPRZEDA? 2WOLN. E </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>8.00</normalizedValue>
					<recognizedValue>
						<text>8,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDA? 2WOLN. E 8,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA["Largsas"                                
. Skrzypczak karisa                      
Dom Handlowy Maxim                       
Ul. Siwińskiego 2                        
05-120 Legionowo                         
125-083-03-87                            
2021-03-06 12:24                   46895 
PARAGON FISKALNY                         
MASKA BAWEŁNA              1 x8,00 8,00E 
SUMA PTU             -               0,00
SPRZEDA? 2WOLN. E                   8,00 
SUMAPLN-      8,00                       
00006 #00000001 KIEROWNIK                
2021-03-06 12:24 ,                       
BD1F0D6C0OE6874F942BOB1FBD6FDC12D2541939 
IP CAB 1501216989                        
^GOTÓWKA           ,          8,00 PIN   
]]></recognizedText>
	</receipt>
</receipts>
