﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_Clockwise">
		<vendor>
			<name>
				<recognizedValue>
					<text>Nr rei. BDO</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[Lidl sp. z o.o. sp. k.
?n?i 11 ?i NIP 781-18-97-358]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[Lidl sp. z o.o. sp. k.
?n?i 11 ?i NIP 781-18]]></text>
			</address>
			<zip>
				<normalizedValue>62080</normalizedValue>
				<recognizedValue>
					<text>62
080</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7811897358</normalizedValue>
				<recognizedValue>
					<text>781-18-97-358</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-11-21</normalizedValue>
			<recognizedValue>
				<text>2021-11-21</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>14:38:37</normalizedValue>
			<recognizedValue>
				<text>14:38:37</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>41.49</normalizedValue>
			<recognizedValue>
				<text>41,49</text>
			</recognizedValue>
		</total>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>41.49</normalizedValue>
				<recognizedValue>
					<text>41,49</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue>XXXXXXXXXXXX8080</normalizedValue>
				<recognizedValue>
					<text>*8080</text>
				</recognizedValue>
			</cardNumber>
		</payment>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[Lidl sp. z o.o. sp. k.                                          
?n?i 11 ?i NIP 781-18-97-358                                    
<021-11-21             nr wydr.062350/0179                      
HM: %'    $                                                     
MASŁO EKSTRA            F 1 * 5,99 5’99 C                       
OGÓRKI ZIEL.Dt.ŚW. F              '                             
POMID.MAL.POL.,LUZ     F’^ * *'**     %                         
CHLEB ŚW.WIOSKI Y^1**1'89 2’89 C                                
PÓtBAGIETKA ŚW. F 2 * 0’89 1’78 C                               
            Sprzed, opod. PTU A                18’49”           
Kwota A 23,00%                      3 46                        
Sprzed, opod. PTU C                23 00                        
SUMA PLN 41,49                                                  
000071 #86 86 1726     nr:23056’’”l4:38                         
64E7C35AA4AB6C9E3A72A55226707B0F5A600BCB                        
                7E> CCO 1901691350                              
Płatność             Karta płatnicza 41,49                      
RAZEM PLN       41,49                                           
Adres siedziby sprzedawcy:                                      
ul. Poznańska 48, Jankowice                                     
62-080 Tarnowo Podgórne                                         
Nr rei. BDO 000002265                                           
lllillllll Allllilllllllll IIIIIIH                              
2021-11-21              nr wydr.062351/0179                     
NIEFISKALNY                                                     
************************************                            
*  Wybrane sklepy otwarte także w *                             
*             NIEDZIELĘ!             *                          
*     Sprawdznawww.lidl.pl     *                                
************************************                            
NIEFISKALNY                                                     
#86 86 1726                         14:38                       
1901691350                                                      
2021-11-21              nr wydr.062352/0179                     
NIEFISKALNY                                                     
Data transakcji:      2021-11-21 14:38:37                       
Nr dowodu sprz: 17937011        Kasa:0086                       
TID:32224005 MID: 000000000171009                               
P: 20 S:                   2536                                 
Kasjer: 86                               ,                      
Karta: VISA                    (P)H                             
438659******8080 Ważna do:                                      
CONTACTLESS                                                     
Kod autoryzacji:                n. u ,/U                        
Sprzedaż:                        PLN 41,49                      
Transakcja potwierdzona                                         
Karta bezstykowa, podpi s niewymagany                           
Prósz? obciążyć moje konto.^^^                                  
 *#:                   61EBFCAAA2762B92                        
NIEFISKALNY                                                     
]]></recognizedText>
	</receipt>
</receipts>
