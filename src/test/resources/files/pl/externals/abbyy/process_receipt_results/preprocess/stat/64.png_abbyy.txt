﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>sos BBQ</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[cuiCken
2E5! SP. Z 0.0.
RED HOT CHICKEN
ul. Warszawska 600, 05-120 legionowo
HIP 125-164-07-72]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[cuiCken
2E5! SP. Z 0.0.
RED HOT CHICKEN
ul. Warszawska 600, 
 legionowo
HIP 125-164-07-72]]></text>
			</address>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>1251640772</normalizedValue>
				<recognizedValue>
					<text>125-164-07-72</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2020-11-28</normalizedValue>
			<recognizedValue>
				<text>2020-11-28</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>17:09:00</normalizedValue>
			<recognizedValue>
				<text>17:09</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>22.90</normalizedValue>
			<recognizedValue>
				<text>22,90</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>0.75</normalizedValue>
			<recognizedValue>
				<text>23,00 % 0,75</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="8">
			<normalizedValue>1.40</normalizedValue>
			<recognizedValue>
				<text>8,00 % 1,40</text>
			</recognizedValue>
		</tax>
		<payment type="Undefined">
			<value>
				<normalizedValue>22.90</normalizedValue>
				<recognizedValue>
					<text>22,90</text>
				</recognizedValue>
			</value>
		</payment>
		<recognizedItems count="8">
			<item index="1">
				<name>
					<text>Quesadilla Set Gold 1 *17,90 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>17.91</normalizedValue>
					<recognizedValue>
						<text>17,908</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Quesadilla Set Gold 1 *17,90 17,908]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>sos BBQ 1 *1,00 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.01</normalizedValue>
					<recognizedValue>
						<text>1,008</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[sos BBQ 1 *1,00 1,008]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>7UP 0.331 Puszka 1 *4,00 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.00</normalizedValue>
					<recognizedValue>
						<text>4,000</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[7UP 0.331 Puszka 1 *4,00 4,000]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.00</normalizedValue>
					<recognizedValue>
						<text>4,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA A 4,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>PTU A 23,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.75</normalizedValue>
					<recognizedValue>
						<text>0,75</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU A 23,00 % 0,75]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>SPRZEDA? OPODATKOWANA 8 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>18.90</normalizedValue>
					<recognizedValue>
						<text>18,90</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDA? OPODATKOWANA 8 18,90]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>PTU B 8,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.40</normalizedValue>
					<recognizedValue>
						<text>1,40</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU B 8,00 % 1,40]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>SONA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.15</normalizedValue>
					<recognizedValue>
						<text>2,15</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SONA PTU 2,15]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[cuiCken                                   
2E5! SP. Z 0.0.                           
RED HOT CHICKEN                           
ul. Warszawska 600, 05-120 legionowo      
HIP 125-164-07-72                         
2020-11-28 17:09                   78937  
PARAGON FISKALNA                          
Quesadilla Set Gold 1 *17,90 17,908       
sos BBQ                     1 *1,00 1,008 
7UP 0.331 Puszka 1 *4,00 4,000            
SPRZEDAŻ OPODATKOWANA A             4,00  
PTU A 23,00 %                        0,75 
SPRZEDA? OPODATKOWANA 8            18,90  
PTU B 8,00 %                          1,40
SONA PTU                              2,15
SUMA PEN       22,90                      
00021 #001 KIEROWNIK 2020-11-28 17:09     
6CC50098AC28E8D5FFDE488928O253150D29DD48  
/P C8U 1601380623                         
Karta                           22,90 PIN 
Nr transakcji:            948/11/2020/K1  
Nazw. kasjera:                BogusEau 0. 
]]></recognizedText>
	</receipt>
</receipts>
