﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_Clockwise">
		<vendor>
			<name>
				<recognizedValue>
					<text>7*9 Biedronka</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[\jf Codziennignijkieceny/
BIEDRONKA "CODZIENNIE NISKIE CENY" 7110
05-110 JABŁONNA UL. ZEGRZYŃSKA
JERONIMO MARTINS POLSKA 5.A.
62-025 KOSTRZYN UL. ŻNIWNA 5
NIP 779-10-11-327]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>05110</normalizedValue>
				<recognizedValue>
					<text>05
110</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7791011327</normalizedValue>
				<recognizedValue>
					<text>779-10-11-327</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-11-06</normalizedValue>
			<recognizedValue>
				<text>2021-11-06</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>14:57:00</normalizedValue>
			<recognizedValue>
				<text>14:57</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>69.40</normalizedValue>
			<recognizedValue>
				<text>69,4</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>10.09</normalizedValue>
			<recognizedValue>
				<text>23,00 % 10,09</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="5">
			<normalizedValue>0.74</normalizedValue>
			<recognizedValue>
				<text>5,00 % 0,74</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>69.40</normalizedValue>
				<recognizedValue>
					<text>69,4</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="12">
			<item index="1">
				<name>
					<text>Ręcznik Milla X2 A 1 x3,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.99</normalizedValue>
					<recognizedValue>
						<text>3,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Ręcznik Milla X2 A 1 x3,99 3,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>Rum Morgan SG 0,71 A 1x54,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>54.99</normalizedValue>
					<recognizedValue>
						<text>54,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Rum Morgan SG 0,71 A 1x54,99 54,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>Rabat </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.00</normalizedValue>
					<recognizedValue>
						<text>5,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Rabat 
5,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text/>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>49.99</normalizedValue>
					<recognizedValue>
						<text>49,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[49,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>Antipasti 180/1500 C 1 x7,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>7.49</normalizedValue>
					<recognizedValue>
						<text>7,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Antipasti 180/1500 C 1 x7,49 7,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>TrioWarzMroKra750g C 2 x4,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.98</normalizedValue>
					<recognizedValue>
						<text>9,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[TrioWarzMroKra750g C 2 x4,99 9,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>Rabat </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.00</normalizedValue>
					<recognizedValue>
						<text>2,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Rabat 
2,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text/>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>7.98</normalizedValue>
					<recognizedValue>
						<text>7,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[7,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="9">
				<name>
					<text>SPRZEDA?&apos; OPODATKOWANA&apos; A...........................</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>53.98</normalizedValue>
					<recognizedValue>
						<text>53,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDA?' OPODATKOWANA' A...........................53,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="10">
				<name>
					<text>PTU A 23,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>10.09</normalizedValue>
					<recognizedValue>
						<text>10,09</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU A 23,00 % 10,09]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="11">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA C </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>15.47</normalizedValue>
					<recognizedValue>
						<text>15,47</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA C 15,47]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="12">
				<name>
					<text>PTU C 5,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.74</normalizedValue>
					<recognizedValue>
						<text>0,74</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU C 5,00 % 0,74]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[(7*9 Biedronka )                                                
\jf Codziennignijkieceny/                                       
BIEDRONKA "CODZIENNIE NISKIE CENY" 7110                         
05-110 JABŁONNA UL. ZEGRZYŃSKA                                  
JERONIMO MARTINS POLSKA 5.A.                                    
62-025 KOSTRZYN UL. ŻNIWNA 5                                    
NIP 779-10-11-327                                               
2021-11-06 14=57                                  240060        
PARAGON FISKALNY                                                
Ręcznik Milla X2 A                 1 x3,99 3,99A                
Rum Morgan SG 0,71 A               1x54,99 54,99A               
Rabat                                            -5,00          
49,99#                                                          
Antipasti 180/1500        C                 1 x7,49 7,49C       
TrioWarzMroKra750g       C                 2 x4,99 9,98C        
Rabat                                            -2,00          
7,98C                                                           
SPRZEDA?' OPODATKOWANA' A 53,98                                 
 PTU A 23,00 %                                       10,09     
SPRZEDAŻ OPODATKOWANA C                             15,47       
PTU C 5,00 %                                         0,74       
SUMA PTU                                            10,81       
SUMA PLN               69,4®                                    
00265 #Kasa 1 Kasjer nr 12               2021-11-06 14=         
CBD49BEAOAF95680FB7774FBC535331C53D12007                        
CCH 1903122113                                                  
■iiiiiiir                                                       
Nr sys. 8748                                                    
Karta VISA 07 1                                 69,45 PLN       
Nr transakcji:j                                        8748     
(Numer:         1                         7110211106874801      
NIP 779-10-11-327                                               
2021-11-06 14:57                                  240061        
N 1 E F I 5 K A L N Y                                           
*                                                               
Dr,ogi Kliencie,                                                
aktualna liczba naklejek                                        
Gang Swojaków zapisanych                                        
na/Twojej karcie Moja Biedronka                                 
wynosi: 4.                                                      
Naklejki można odebrać                                          
na podstawie karty                                              
f Moja Biedronka w dowolnym                                     
/sklepie Biedronka do 20.11.2021                                
Szczegóły i wyłączenia w                                        
regulaminie akcji w sklepie i na                                
www.gangswojakow.pl                                             
KUPUJ CO POLSKIE                                                
WARTOŚĆ "KUPUJ CO POLSKIE" W TWOICH ZAKUPACH,                   
Z WYŁĄCZENIEM PRODUKTÓW NIEBIORĄCYCH UDZIAŁU W AKCJI, WYNOSI:   
19,46 2ł %%%                                                    
^^^^WięceunabiedronkZpl/kupujcopoukie^™'""™                     
N 1 E F I S K A L N Y                                           
#Kasa 1 Kasjer nr 12              2021-11-06 14=57              
CCH 1903122113                                                  
nZIFKIHFMV I 7APRAS7AMV PPMWF RPA *                             
]]></recognizedText>
	</receipt>
</receipts>
