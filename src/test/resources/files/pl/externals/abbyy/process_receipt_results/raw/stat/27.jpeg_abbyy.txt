﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>lenarcik</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[06
126]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>06126</normalizedValue>
				<recognizedValue>
					<text>06
126</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>5660001787</normalizedValue>
				<recognizedValue>
					<text>5660001787</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-02-27</normalizedValue>
			<recognizedValue>
				<text>2021.02.27</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>11:47:04</normalizedValue>
			<recognizedValue>
				<text>11:47:04</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>25.52</normalizedValue>
			<recognizedValue>
				<text>25,52</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="5">
			<normalizedValue>1.22</normalizedValue>
			<recognizedValue>
				<text>5,00% 1,22</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>25.52</normalizedValue>
				<recognizedValue>
					<text>25,52</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue>XXXXXXXXXXXX3298</normalizedValue>
				<recognizedValue>
					<text>*3298</text>
				</recognizedValue>
			</cardNumber>
		</payment>
		<recognizedItems count="5">
			<item index="1">
				<name>
					<text>POLĘDWICA SOPOCKA 0,21 x22,</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>95.40</normalizedValue>
					<recognizedValue>
						<text>95 4</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[POLĘDWICA SOPOCKA 0,21 x22,95 4]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>0,152 x39,</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>90.60</normalizedValue>
					<recognizedValue>
						<text>90 6</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0,152 x39,90 6]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>BOCZEK WĘDZONY 0,614 x23,</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>84.14</normalizedValue>
					<recognizedValue>
						<text>84 14</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[BOCZEK WĘDZONY 0,614 x23,84 14]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>SPRZEDAZ&apos;OPODATKOWANA C.................</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>25.52</normalizedValue>
					<recognizedValue>
						<text>25,52</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAZ'OPODATKOWANA C.................25,52]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>PTUC5,00% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.22</normalizedValue>
					<recognizedValue>
						<text>1,22</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTUC5,00% 1,22]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[lenarcik                                      
ZAKŁAD MIĘSNY                                 
SŁAWOMIR LENARCIK                             
GOTARDY 37 , 06-126 GZY                       
SKLEP NR 62                                   
Legionowo ul, Marysieńki 1a                   
NIP 5660001787                                
2021-02-27 11:46                   170269     
N I E F I 5 K A L N Y                         
Potwierdzenie zapłaty kartę                   
Karta: Visa Contactless(B)                    
************3298 Ważna do: **/**              
TID: 34013216 MID: 000000000298753            
       Nr transakcji:       001670            
AID: A0000000031010 '                         
Kod autoryzacji: (1)019660                    
Sprzedaż: PLN 25,52                           
zweryfikowano na urządzeniu                   
TC: AAC: 4AD0F3AOB935DFD4                     
Zapraszany ponownie                           
Proszą zachować rachunek                      
Dziękujemy za zakupy                          
2021.02.27 11:47:04                           
U ISA Contactless                             
ORYGINAŁ                                      
N I E F I 5 K A L N Y                         
#1 mczajkowska 2021-02-27 11:46               
CCS 1801070554                                
lęnaMik                                       
■"ZAKŁAD mięsny                               
SŁAWOMIR LENARCIK                             
GOTARDY 37 , 06-126 GZY                       
SKLEP NR 62                                   
Legionowo ul. Marysieńki la                   
NIP 5660001787                                
2021-02-27 11:46                   170270     
PARAGON FISKALNY                              
POLĘDWICA SOPOCKA 0,21 x22,95 4,82C           
KIEŁBASA KRAKOWSKA SUCHA                      
0,152 x39,90 6.06C                            
BOCZEK WĘDZONY 0,614 x23,84 14,64C            
 SPRZEDAZ'OPODATKOWANA C 25,52               
PTUC5,00%                    1,22             
SUMAPTU                           1,22        
SUMA PLN       25,52                          
00180 #1 mczajkowska    2021-02-27 11:46      
77D6FF0B3C72F7542FBD993342777F87AAE3750B      
/F CCS 1801070554                             
Karta                          25,52 PLN      
000 000018383                                 
]]></recognizedText>
	</receipt>
</receipts>
