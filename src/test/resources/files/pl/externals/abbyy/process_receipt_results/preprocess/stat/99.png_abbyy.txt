﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>IKEA RETAIL SP.</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[U1. MALBORSKA 51
03-286 WARSZAWA]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[U1. MALBORSKA 51
 WARSZAWA]]></text>
			</address>
			<phone>
				<normalizedValue>5270103385</normalizedValue>
				<recognizedValue>
					<text>527-010-33-85</text>
				</recognizedValue>
			</phone>
			<zip>
				<normalizedValue>03286</normalizedValue>
				<recognizedValue>
					<text>03
286</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>5270103385</normalizedValue>
				<recognizedValue>
					<text>527-010-33-85</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-08-11</normalizedValue>
			<recognizedValue>
				<text>2021-08-11</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>19:56:00</normalizedValue>
			<recognizedValue>
				<text>19:56</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>344.93</normalizedValue>
			<recognizedValue>
				<text>344,93</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>64.50</normalizedValue>
			<recognizedValue>
				<text>23,00 % 64,50</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Undefined">
			<value>
				<normalizedValue>344.93</normalizedValue>
				<recognizedValue>
					<text>344,93</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue>XXXXXXXXXXXX8080</normalizedValue>
				<recognizedValue>
					<text>* 8080</text>
				</recognizedValue>
			</cardNumber>
		</payment>
		<recognizedItems count="21">
			<item index="1">
				<name>
					<text>4 x6,00 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>24.00</normalizedValue>
					<recognizedValue>
						<text>24,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[4 x6,00 24,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>1 x39,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>39.99</normalizedValue>
					<recognizedValue>
						<text>39,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x39,99 39,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>1 x29,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>29.99</normalizedValue>
					<recognizedValue>
						<text>29.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x29,99 29.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>1 x3,00 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.00</normalizedValue>
					<recognizedValue>
						<text>3,00</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x3,00 3,00]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>1 x9 99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.99</normalizedValue>
					<recognizedValue>
						<text>9 99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x9 99 9 99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>1 X39.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>39.99</normalizedValue>
					<recognizedValue>
						<text>39.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X39.99 39.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>1 xO.10 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.10</normalizedValue>
					<recognizedValue>
						<text>0,10</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 xO.10 0,10]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>4 x7,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>31.96</normalizedValue>
					<recognizedValue>
						<text>31,96</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[4 x7,99 31,96]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="9">
				<name>
					<text>1 X14.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>14.99</normalizedValue>
					<recognizedValue>
						<text>14.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X14.99 14.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="10">
				<name>
					<text>1 X14.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>14.99</normalizedValue>
					<recognizedValue>
						<text>14.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X14.99 14.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="11">
				<name>
					<text>1 X14.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>14.99</normalizedValue>
					<recognizedValue>
						<text>14.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X14.99 
14.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="12">
				<name>
					<text>1 x19 99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>19.99</normalizedValue>
					<recognizedValue>
						<text>19 99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x19 99 19 99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="13">
				<name>
					<text>1 x59,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>59.99</normalizedValue>
					<recognizedValue>
						<text>59.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 x59,99 59.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="14">
				<name>
					<text>3 x4,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>14.97</normalizedValue>
					<recognizedValue>
						<text>14.97</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[3 x4,99 14.97]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="15">
				<name>
					<text>10412029 KOPPLA przedl3 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.50</normalizedValue>
					<recognizedValue>
						<text>1.5</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[10412029 KOPPLA przedl3 1.5]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="16">
				<name>
					<text>1 X15.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>15.99</normalizedValue>
					<recognizedValue>
						<text>15.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X15.99 15.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="17">
				<name>
					<text>1 X19.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>19.99</normalizedValue>
					<recognizedValue>
						<text>19.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X19.99 19.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="18">
				<name>
					<text>1 X19.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>19.99</normalizedValue>
					<recognizedValue>
						<text>19.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 X19.99 19.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="19">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>344.93</normalizedValue>
					<recognizedValue>
						<text>344,93</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA A 344,93]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="20">
				<name>
					<text>PTU A 23,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>64.50</normalizedValue>
					<recognizedValue>
						<text>64,50</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU A 23,00 % 64,50]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="21">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>64.50</normalizedValue>
					<recognizedValue>
						<text>64,50</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 64,50]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[.....n I li hl I I V                      
IKEA RETAIL SP. Z 0.0.                    
U1. MALBORSKA 51                          
03-286 WARSZAWA                           
Tel. 22 275 00 00                         
NIP 527-010-33-85                         
2021-08-11                        489012  
PARAGON FISKALNY                          
20317079 SPRUTTIG wieszak                 
4 x6,00 24,00A                            
40392893 ODDRUN pled 130x170              
1 x39,99 39,99A                           
50334685 SINNESRO podst n*                
1 x29,99 29.99A                           
17228340 FRAKTA duża torba                
1 x3,00 3,00A                             
10387261 RINNIG suszarka                  
1 x9 99 9 99A                             
80400004 SKUBB Pudło na buty              
1 X39.99 39.99A                           
80409311 pudełko na                       
1 xO.10 0,10A                             
80278367 IKEA 365+ kubek 36               
4 x7,99 31,96A                            
70387258 RINNIG uchw tai                  
1 X14.99 14.99A                           
70387258 RINNIG uchw tai                  
1 X14.99 14.99A                           
#STORNO#                                  
70387258 RINNIG uchw tai                  
1 X14.99 -14.99A                          
30090382 DRAGON wideł sał/des             
1 x19 99 19 99A                           
90091760 DRAGON N sztućce24               
1 x59,99 59.99A                           
40343807 M RIT podkładka                  
3 x4,99 14.97A                            
10412029 KOPPLA przedl3 1.5 m             
1 X15.99 15.99A                           
90370539 BLOMDOFT br Swie zap             
1 X19.99 19.99A                           
50432426 LINDRANDE podst                  
1 X19.99 19.99A                           
SPRZEDAŻ OPODATKOWANA A           344,93  
PTU A 23,00 %                       64,50 
SUMA PTU                           64,50  
SUMA PLN      344,93                      
00118 #68 Kasjer: 68         19:56        
DC6A5CC06BF3CB5AFD83B98E8C005B8B106C548C  
/E BOY 13300824                           
9900307006800123210811                    
Karta płatnicza               344,93 PLN  
Klient stały:                  IKEAFamily 
Klient stały:        6275980xxxxxxxx2025  
Numer karty:             ************ 8080
Nr transakcji :                 307/68/123
Paragon do 450,00 zł z NIP nabywcy        
.///: tanowi fakturę VAT.                 
]]></recognizedText>
	</receipt>
</receipts>
