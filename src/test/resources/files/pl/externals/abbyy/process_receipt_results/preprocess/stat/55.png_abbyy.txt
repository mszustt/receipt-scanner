﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>PIEKARNIA</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[K. i fi. Cichowscy
E. Federowicz, Sp. Jawna
ul. fil. St. Zjednoczonych 63 lok. 81
04-028 Warszawa
SKLEP FIRMOWY
ul. Sowińskiego 9
05-120 Legionowo]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>04028</normalizedValue>
				<recognizedValue>
					<text>04
028</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>1130026398</normalizedValue>
				<recognizedValue>
					<text>1130026398</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-03-04</normalizedValue>
			<recognizedValue>
				<text>2021-03-04</text>
			</recognizedValue>
		</date>
		<total>
			<normalizedValue>1.96</normalizedValue>
			<recognizedValue>
				<text>1,96</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="5">
			<normalizedValue>0.09</normalizedValue>
			<recognizedValue>
				<text>5% 0,09</text>
			</recognizedValue>
		</tax>
		<payment type="Undefined">
			<value>
				<normalizedValue>1.96</normalizedValue>
				<recognizedValue>
					<text>1,96</text>
				</recognizedValue>
			</value>
		</payment>
		<recognizedItems count="2">
			<item index="1">
				<name>
					<text>PTU C 5% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.09</normalizedValue>
					<recognizedValue>
						<text>0,09</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU C 5% 0,09]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.09</normalizedValue>
					<recognizedValue>
						<text>0,09</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 0,09]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[PIEKARNIA                                                 
K. i fi. Cichowscy                                        
E. Federowicz, Sp. Jawna                                  
ul. fil. St. Zjednoczonych 63 lok. 81                     
04-028 Warszawa                                           
SKLEP FIRMOWY                                             
ul. Sowińskiego 9                                         
05-120 Legionowo                                          
NIP 1130026398                                  nr=100819 
PARAGON FISKALNY                                          
BULKA KAJZERKA C                          4st X0.491 1,96C
SPRZEDAŻ' OPODATKOWANA' C 1 ’ 96                          
PTU C 5%                                             0,09 
SUMA PTU                                             0,09 
SUMAPLN               1,96                                
rozu'czeni'e''platno$ci                                   
GOTÓWKA                                          1,96 PLN 
00091 #74 461                            2021-03-04 09=04 
A2782B54066FD42785E62F961803A661DA20A0A9                  
P EAZ 1901841692                                          
Nr transakcji:                                       17556
Numer:                                      BDO 000099138 
]]></recognizedText>
	</receipt>
</receipts>
