﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>SPOŁEM PSS SKLEP</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[05-120 LEGIONOWO, OL.SOBIESKIEGO 6
NIP: 5310004288]]></text>
			</fullAddress>
			<address>
				<text><![CDATA[LEGIONOWO, OL.SOBIESKIEGO 6
NIP: 5310004288]]></text>
			</address>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>5310004288</normalizedValue>
				<recognizedValue>
					<text>5310004288</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-02-24</normalizedValue>
			<recognizedValue>
				<text>24-02;2021</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>09:12:00</normalizedValue>
			<recognizedValue>
				<text>09:12</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>9.13</normalizedValue>
			<recognizedValue>
				<text>9.13</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="3.9">
			<normalizedValue>0.31</normalizedValue>
			<recognizedValue>
				<text>8.002 0.31</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="4.8">
			<normalizedValue>0.24</normalizedValue>
			<recognizedValue>
				<text>5.002 0.24</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>9.13</normalizedValue>
				<recognizedValue>
					<text>9.13</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="8">
			<item index="1">
				<name>
					<text/>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.49</normalizedValue>
					<recognizedValue>
						<text>1.49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1.49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>M/VITA MLEKO BE2 LAKTOZY 3.22 42996 1*</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.45</normalizedValue>
					<recognizedValue>
						<text>3.45</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[M/VITA MLEKO BE2 LAKTOZY 3.22 42996 1*3.45]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text/>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.45</normalizedValue>
					<recognizedValue>
						<text>3.45</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[3.45]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>MAJONEZ NAPOLEON. 3206 4920 1*</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.19</normalizedValue>
					<recognizedValue>
						<text>4.19</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[MAJONEZ NAPOLEON. 3206 4920 1*4.19]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>RAZEM:....... </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.13</normalizedValue>
					<recognizedValue>
						<text>9.13</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[RAZEM:....... 9.13]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>SP.OP.B: 4.19 PTU 8.002 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.31</normalizedValue>
					<recognizedValue>
						<text>0.31</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SP.OP.B: 4.19 PTU 8.002 0.31]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>SP.OP.E: 4.94 PTU 5.002 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.24</normalizedValue>
					<recognizedValue>
						<text>0.24</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SP.OP.E: 4.94 PTU 5.002 0.24]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.55</normalizedValue>
					<recognizedValue>
						<text>0.55</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 0.55]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[SPOŁEM PSS SKLEP NR 37                         
05-120 LEGIONOWO, OL.SOBIESKIEGO 6             
NIP: 5310004288                                
 24-02-2021                      JM82654       
PARAGON FISKALNY "                             
TYM.SOK HALINA JAB 3001 12562501 1*1.49        
1.49E                                          
M/VITA MLEKO BE2 LAKTOZY 3.22 42996 1*3.45     
3.45C                                          
MAJONEZ NAPOLEON. 3206 4920 1*4.19 4.198       
RAZEM:                         9.13            
SP.OP.B: 4.19 PTU 8.002               0.31     
SP.OP.E: 4.94 PTU 5.002               0.24     
SUMA PTU                              0.55     
SUMA: " JLN 9.13                               
 F7751Ó 'mi.kasjer!     24-02;2021 09:12      
MnBWikBuoBLOJkY+cbMVidO: 66......              
TP CBT 1601327868                              
Karta:                                 9.13    
]]></recognizedText>
	</receipt>
</receipts>
