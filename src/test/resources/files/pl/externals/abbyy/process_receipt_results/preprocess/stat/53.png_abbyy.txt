﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>LEROY - MERLIN</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[ POLSKA sp. z o.o.
ul. Targowa 72, 03-734 Warszawa
SKLEP LEROY MERLIN JABŁONNA
Ul.Akademijna 51, 05-110 Jabłonna
Nr rej.BDO:000003773177
NIP 113-00-89-950]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>03734</normalizedValue>
				<recognizedValue>
					<text>03
734</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>1130089950</normalizedValue>
				<recognizedValue>
					<text>113-00-89-950</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-03-06</normalizedValue>
			<recognizedValue>
				<text>2021-03-06</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>13:44:00</normalizedValue>
			<recognizedValue>
				<text>13:44</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>48.89</normalizedValue>
			<recognizedValue>
				<text>48,89</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>9.14</normalizedValue>
			<recognizedValue>
				<text>23,00% 9,14</text>
			</recognizedValue>
		</tax>
		<payment type="Undefined">
			<value>
				<normalizedValue>48.89</normalizedValue>
				<recognizedValue>
					<text>48,89</text>
				</recognizedValue>
			</value>
		</payment>
		<recognizedItems count="5">
			<item index="1">
				<name>
					<text>1 * 16,90 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>16.90</normalizedValue>
					<recognizedValue>
						<text>16,90</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 * 16,90 16,90]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>1 * 31,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>31.99</normalizedValue>
					<recognizedValue>
						<text>31,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 * 31,99 31,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>Sprzed, opod. PTU A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>48.89</normalizedValue>
					<recognizedValue>
						<text>48,89</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU A 48,89]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>Kwota A 23,00% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.14</normalizedValue>
					<recognizedValue>
						<text>9,14</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota A 23,00% 9,14]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>Podatek PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.14</normalizedValue>
					<recognizedValue>
						<text>9,14</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Podatek PTU 9,14]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[LEROY - MERLIN POLSKA sp. z o.o.                 
ul. Targowa 72, 03-734 Warszawa                  
SKLEP LEROY MERLIN JABŁONNA                      
Ul.Akademijna 51, 05-110 Jabłonna                
Nr rej.BDO:000003773177                          
NIP 113-00-89-950                                
2021-03-06                   nr wydr.147656/0573 
PARAGON FISKALNY                                 
45634190 ODBOJNIK WYSOKI NIKIEL                  
1        *       16,90 16,90 A                   
43861643 KOSZ NA ŚMIECI 25L FLIP BIN SR          
1        *      31,99 31,99 A                    
Sprzed, opod. PTU A                       48,89  
Kwota A 23,00%                            9,14   
Podatek PTU                               9,14   
SUMA PLN           48,89                         
000136 #740 KA                              13:44
EC62A322CF1FOA9D263320FB07 FFB9902741DC48        
7E> CBH 1601157833                               
IINIIIIIIIIlilllllllllllllillllllllllllllllllM   
Karta                                      48.89 
RAZEM PLN          48,89                         
Nr transakcji                066-047-000047 4240 
DZIĘKUJEMY ZA WIZYTĘ!                            
ZAPRASZAMY PONOWNIE!                             
www.Ieroymer1in.pl                               
]]></recognizedText>
	</receipt>
</receipts>
