﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>Lidl sp. z o.o.</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[Zegrzyńska 39
05-110 Jabłonna
NIP 781-18-97-358]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>05110</normalizedValue>
				<recognizedValue>
					<text>05
110</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7811897358</normalizedValue>
				<recognizedValue>
					<text>781-18-97-358</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-11-13</normalizedValue>
			<recognizedValue>
				<text>2021-11-13</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>15:36:23</normalizedValue>
			<recognizedValue>
				<text>15:36:23</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>23.11</normalizedValue>
			<recognizedValue>
				<text>23,11</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="1">
			<normalizedValue>2.49</normalizedValue>
			<recognizedValue>
				<text>1 * 2,49 2,49</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="1">
			<normalizedValue>2.89</normalizedValue>
			<recognizedValue>
				<text>1 * 2,89 2,89</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>23.11</normalizedValue>
				<recognizedValue>
					<text>23,11</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue>XXXXXXXXXXXX8080</normalizedValue>
				<recognizedValue>
					<text>*8080</text>
				</recognizedValue>
			</cardNumber>
		</payment>
		<recognizedItems count="12">
			<item index="1">
				<name>
					<text>NAP.IZOT.SPORTS X 1 * 2,29 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.29</normalizedValue>
					<recognizedValue>
						<text>2,29</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[NAP.IZOT.SPORTS X 1 * 2,29 2,29]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>MASŁO EKSTRA F 1 * 5,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.99</normalizedValue>
					<recognizedValue>
						<text>5,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[MASŁO EKSTRA F 1 * 5,99 5,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>SER MOZ. BEZ GMO F 1 * 2,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.49</normalizedValue>
					<recognizedValue>
						<text>2,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SER MOZ. BEZ GMO F 1 * 2,49 2,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>, 0,058 * 29,90 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.73</normalizedValue>
					<recognizedValue>
						<text>1,73</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[, 0,058 * 29,90 1,73]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>0,328 * 6,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.29</normalizedValue>
					<recognizedValue>
						<text>2,29</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0,328 * 6,99 2,29]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>0,388 * 13,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.43</normalizedValue>
					<recognizedValue>
						<text>5,43</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0,388 * 13,99 5,43]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>CHLEB ŚW.WŁOSKI f 1 * 2,89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.89</normalizedValue>
					<recognizedValue>
						<text>2,89</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[CHLEB ŚW.WŁOSKI f 1 * 2,89 2,89]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>Sprzed, opod. PTU A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.29</normalizedValue>
					<recognizedValue>
						<text>2,29</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU A 2,29]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="9">
				<name>
					<text>Kwota A 23,00% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.43</normalizedValue>
					<recognizedValue>
						<text>0,43</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota A 23,00% 0,43]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="10">
				<name>
					<text>Sprzed, opod. PTU C </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>20.82</normalizedValue>
					<recognizedValue>
						<text>20,82</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU C 20,82]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="11">
				<name>
					<text>Kwota 0 05,00% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.99</normalizedValue>
					<recognizedValue>
						<text>0,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota 0 05,00% 0,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="12">
				<name>
					<text>Podatek PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.42</normalizedValue>
					<recognizedValue>
						<text>1,42</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Podatek PTU 1,42]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[Lidl sp. z o.o. sp. k.                         
Zegrzyńska 39                                  
05-110 Jabłonna                                
NIP 781-18-97-358                              
2021-11-13             nr wydr.046497/0171     
PARAGON FISKALNY                               
NAP.IZOT.SPORTS        X   1 * 2,29 2,29 A     
MASŁO EKSTRA           F   1 * 5,99 5,99 C     
SER MOZ. BEZ GMO       F   1 * 2,49 2,49 C     
CZOSN.ŚW.,LUZ           F                      
,               0,058 * 29,90 1,73 0           
OGÓRKI ZIEL.DLŚW.     A                        
0,328 * 6,99 2,29 0                            
POMID.MAL.POL.,LUZ     F                       
0,388 * 13,99 5,43 0                           
CHLEB ŚW.WŁOSKI        f   1 * 2,89 2,89 C     
Sprzed, opod. PTU A                 2,29       
Kwota A 23,00%                      0,43       
Sprzed, opod. PTU C                20,82       
Kwota 0 05,00%                      0,99       
Podatek PTU                         1,42       
SUMA PLN 23,11                                 
000062 #81   81 1726 nr:17383    15:36         
102140A1EE7918C790ADDBD1AFBCA2838AFF268C       
        7E> CCO 1901691472                     
Płatność             Karta płatnicza 23,11     
RAZEM PLN       23,11                          
Adres siedziby sprzedawcy:                     
ul. Poznańska 48, Jankowice                    
62-080 Tarnowo Podgórne                        
lilii                                          
2021-11-13             nr wydr.046498/0171     
NIEFISKALNY                                    
Data transakcji:      2021-11-13 15:36:23      
Nr dowodu sprz: 257720890       Kasa:0081      
TID:32224000 MID: 000000000171009              
 P: . 152 S:                   14778          
Kasier: 81                                     
Karta: VISA                       (P)(-)       
438659******8080 Ważna do:                     
PSN:O                                          
CONTACTLESS                                    
Kod autoryzacji:         282303       (1)      
Sprzedaż:                        PLN 23,11     
Transakcja potwierdzona                        
Karta bezstykowa, podpis niewymagany           
Proszę obciążyć moje konto.                    
AID:                       A0000000031010      
TC:                     12840E3723FDBD14       
NIEFISKALNY                                    
#81 81 1726                          15:36     
1901691472                                     
]]></recognizedText>
	</receipt>
</receipts>
