﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>LEROY - MERLIN</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[ POLSKA sp. z o.o.
ul. Targowa 72, 03-734 Warszawa
SKLEP LEROY MERLIN JABŁONNA
Ul.Akademijna 51, 05-110 Jabłonna]]></text>
			</fullAddress>
			<phone>
				<normalizedValue>1130089950</normalizedValue>
				<recognizedValue>
					<text>113-00-89-950</text>
				</recognizedValue>
			</phone>
			<zip>
				<normalizedValue>03734</normalizedValue>
				<recognizedValue>
					<text>03
734</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>1130089950</normalizedValue>
				<recognizedValue>
					<text>113-00-89-950</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-09-04</normalizedValue>
			<recognizedValue>
				<text>2021-09-04</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>13:29:00</normalizedValue>
			<recognizedValue>
				<text>13:29</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>29.99</normalizedValue>
			<recognizedValue>
				<text>29.99</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>5.61</normalizedValue>
			<recognizedValue>
				<text>23,00% 5,61</text>
			</recognizedValue>
		</tax>
		<payment type="Undefined">
			<value>
				<normalizedValue>29.99</normalizedValue>
				<recognizedValue>
					<text>29.99</text>
				</recognizedValue>
			</value>
		</payment>
		<recognizedItems count="4">
			<item index="1">
				<name>
					<text>1 * 29,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>29.99</normalizedValue>
					<recognizedValue>
						<text>29,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[1 * 29,99 29,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>Sprzed, opod. PTU A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>29.99</normalizedValue>
					<recognizedValue>
						<text>29,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Sprzed, opod. PTU A 29,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>Kwota A 23,00% </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.61</normalizedValue>
					<recognizedValue>
						<text>5,61</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kwota A 23,00% 5,61]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>Podatek PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.61</normalizedValue>
					<recognizedValue>
						<text>5,61</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Podatek PTU 5,61]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[LEROY - MERLIN POLSKA sp. z o.o.                 
ul. Targowa 72, 03-734 Warszawa                  
SKLEP LEROY MERLIN JABŁONNA                      
Ul.Akademijna 51, 05-110 Jabłonna                
Nr rej.600:000003773177                          
tel(22)765-78-00                                 
NIP 113-00-89-950                                
2021-09-04                   nr wydr.201310/0701 
PARAGON FISKALNY                                 
45599344 PROFIL DYLAT TYTAN 30X1860 NR2          
1       *      29,99 29,99 A                     
Sprzed, opod. PTU A                      29,99   
Kwota A 23,00%                            5,61   
Podatek PTU                               5,61   
SUMA PLN          29.99                          
000109 #740 KA                             13:29 
0781195D2DCC02E678B90616940FA684114E8754         
W           TE CBH 1601157833                    
u***                                             
PLN          z*                                  
Nr transakcji                066-047- 000047 1361
DZIĘKUJEMY ZA WIZYTĘ'                            
ZAPRASZAMY PONOWNIEl                             
www.1eroymer1in.pl                               
]]></recognizedText>
	</receipt>
</receipts>
