﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>��e�Bfedronka�</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[\jf ** * Codziennie niskie ceny/
Sklep 4890 ul. Siwińskiego 2, 05-120 L
eg ionowoJeronimo Martins Polska S.A.
ul. Żniwna 5, 62-025 Kostrzyn
NIP 779-10-11-327]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7791011327</normalizedValue>
				<recognizedValue>
					<text>779-10-11-327</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-09-27</normalizedValue>
			<recognizedValue>
				<text>2021-09-27</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>10:20:00</normalizedValue>
			<recognizedValue>
				<text>10:20</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>23.53</normalizedValue>
			<recognizedValue>
				<text>23,53</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>2.80</normalizedValue>
			<recognizedValue>
				<text>23,00 % 2,80</text>
			</recognizedValue>
		</tax>
		<tax total="false">
			<normalizedValue>3.21</normalizedValue>
			<recognizedValue>
				<text>3,21</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>23.53</normalizedValue>
				<recognizedValue>
					<text>23,53</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="7">
			<item index="1">
				<name>
					<text>Kpi Noży Uniwer Sz A 1 ,99 l</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.00</normalizedValue>
					<recognizedValue>
						<text>4,996</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Kpi Noży Uniwer Sz A 1 ,99 l4,996]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>Bułka poranna 65 g C 4 x0,b4 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.56</normalizedValue>
					<recognizedValue>
						<text>2,56</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Bułka poranna 65 g C 4 x0,b4 2,56]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>Muffin Wede</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.12</normalizedValue>
					<recognizedValue>
						<text>l 115</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Muffin Wedel 115]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>SPRZEDAŻ&apos;OPODATKOWANA A&apos;&quot; </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>14.99</normalizedValue>
					<recognizedValue>
						<text>14,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ'OPODATKOWANA A'" 14,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>PTU A 23,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.80</normalizedValue>
					<recognizedValue>
						<text>2,80</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU A 23,00 % 2,80]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA C </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>8.54</normalizedValue>
					<recognizedValue>
						<text>8,54</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA C 8,54]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.21</normalizedValue>
					<recognizedValue>
						<text>3,21</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 3,21]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[^^e^Bfedronka^                                              
\jf ** * Codziennie niskie ceny/                            
Sklep 4890 ul. Siwińskiego 2, 05-120 L                      
 eg ionowoJeronimo Martins Polska S.A.                     
ul. Żniwna 5, 62-025 Kostrzyn                               
NIP 779-10-11-327                                           
2021-09-27 10:20                                  116014    
PARAGON FISKALNY                                            
Kpi Noży Uniwer Sz A                 1     ,99 l4,996       
Bułka poranna 65 g       C                 4 x0,b4 2,56C    
Muffin Wedel 115g        C                 2 x2 98C         
SPRZEDAŻ'OPODATKOWANA A'"                          14,99    
PTU A 23,00 %                                        2,80   
SPRZEDAŻ OPODATKOWANA C                            8,54     
PTU C 5,00 %                                       6h1      
SUMA PTU                                             3,21   
SUMA PLN               23,53                                
00027 #Kasa 13 Kasjer nr 24             2021-09 27 10:20    
ED8CAB83B1F0CBE559DE8AF65696F4D90L52E72                     
7E CCH 190311/356                                           
Nr sys. 2004                                                
Karta VISA 07 1                                 23,53 PLN   
Nr transakcji'                                       2004   
Numer:                                  4890210927200413    
Numer karty:                                99515*****908   
NIP 779-10-11-327                                           
2021-09-27 10=20                                   116015   
N I E F I 5 K A L N Y                                       
]]></recognizedText>
	</receipt>
</receipts>
