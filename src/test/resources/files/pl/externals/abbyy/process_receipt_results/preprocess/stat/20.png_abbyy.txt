﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>—</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[Sklep 4890 ul. Siwińskiego 2, 05-120 L
egionowoJeronimo Martins Polska S.A.
ul. Żniwna 5, 62-025 Kostrzyn
NIP 779-10-11-327]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7791011327</normalizedValue>
				<recognizedValue>
					<text>779-10-11-327</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-11-25</normalizedValue>
			<recognizedValue>
				<text>2021-11-25</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>10:11:00</normalizedValue>
			<recognizedValue>
				<text>10:11</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>24.45</normalizedValue>
			<recognizedValue>
				<text>24,45</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="5">
			<normalizedValue>1.16</normalizedValue>
			<recognizedValue>
				<text>5,00 % 1,16</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Undefined">
			<value>
				<normalizedValue>24.45</normalizedValue>
				<recognizedValue>
					<text>24,45</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="6">
			<item index="1">
				<name>
					<text>Antipasti180/150g C 1 x7,49 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>7.49</normalizedValue>
					<recognizedValue>
						<text>7,49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Antipasti180/150g C 1 x7,49 7,49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>Ser RycEd 135 C 1 x3,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.99</normalizedValue>
					<recognizedValue>
						<text>3.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Ser RycEd 135 C 1 x3,99 3.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>LunchboxMix200g C 1 x6,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>6.99</normalizedValue>
					<recognizedValue>
						<text>6,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[LunchboxMix200g C 1 x6,99 6,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>Muffin Wedel 115g C 2 x2,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.98</normalizedValue>
					<recognizedValue>
						<text>5,98</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Muffin Wedel 115g C 2 x2,99 5,98]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>PTU C 5,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.16</normalizedValue>
					<recognizedValue>
						<text>1,16</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU C 5,00 % 1,16]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.16</normalizedValue>
					<recognizedValue>
						<text>1 16</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 1 16]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[---                                                               
Sklep 4890 ul. Siwińskiego 2, 05-120 L                            
egionowoJeronimo Martins Polska S.A.                              
ul. Żniwna 5, 62-025 Kostrzyn                                     
NIP 779-10-11-327                                                 
2021-11-25 10=11                                    119291        
PARAGON FISKALNY                                                  
Antipasti180/150g        C                  1 x7,49 7,49C         
Ser RycEd 135            C                  1 x3,99 3.99C         
LunchboxMix200g          C                  1 x6,99 6,99C         
Muffin Wedel 115g        C                  2 x2,99 5,98C         
SPRZEDAŻ OPODATKOWANA' C.......................................2U5
PTU C 5,00 %                                            1,16      
SUMA PTU                                                 1 16     
SUMA PLN               24,45                                      
00020 Masa 14 Kasjer nr 23             2021-11-25 10:11           
B9CFAF6AAF04A7F88BB5CBCD51E6EC4D66D5B049                          
IE CCH 1903117558                                                 
miiiiiiiiiiiiiiiiiiiiiiiii                                        
1000048904811069414638                                            
Nr sys. 4811                                                      
Karta VISA 07 1                                  24,45 PLN        
Nr transakcji:                                         4811       
Numer:                                   4890211125481114         
Numer karty:                                99515*****908         
NIP 779-10-11-327                                                 
2021-11-25 10:11                                   119292         
N I E F I 5 K A L N Y                                             
KUPUJ CO POLSKIE                                                  
WARTOŚĆ "KUPUJ CO POLSKIE" W TWOICH ZAKUPACH,                     
Z WYŁĄCZENIEM PRODUKTÓW NIEBIORĄCYCH UDZIAŁU W AKCJI, WYNOSI:     
24,45 zł                                                          
' ..........                                                      
WIĘCEJ NA BIEDRONKA.pl/KUPUJCOPOLSKIE                             
N I E F I 5 K A L N Y                                             
Masa 14 Kasjer nr 23             2021-11-25 10:11                 
CCH 1903117558                                                    
DZIĘKUJEMY I ZAPRASZAMY PONOWNIEBDO                               
000004585                                                         
]]></recognizedText>
	</receipt>
</receipts>
