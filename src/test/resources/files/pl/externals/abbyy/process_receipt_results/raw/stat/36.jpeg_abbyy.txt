﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>Kaufland Polska</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[ Markety Sp.z o.o.Sp.J
Al.Armii Krajowej 47, 50-541 Wroclaw
Nr BDO 000013346
ul. Zegrzyńska 7
05-110 Jabłonna 6160
NIP 899-23-67 273]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>50541</normalizedValue>
				<recognizedValue>
					<text>50
541</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>8992367273</normalizedValue>
				<recognizedValue>
					<text>899-23-67 273</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-02-25</normalizedValue>
			<recognizedValue>
				<text>2021-02-25</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>09:00:00</normalizedValue>
			<recognizedValue>
				<text>09:00</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>196.85</normalizedValue>
			<recognizedValue>
				<text>196,85</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="23">
			<normalizedValue>25.39</normalizedValue>
			<recognizedValue>
				<text>23,00 % 25,39</text>
			</recognizedValue>
		</tax>
		<tax total="false" rate="8">
			<normalizedValue>0.58</normalizedValue>
			<recognizedValue>
				<text>8,00 % 0,58</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="EuroCard">
			<value>
				<normalizedValue>196.85</normalizedValue>
				<recognizedValue>
					<text>196,85</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue>XXXXXXXXXXXX0773</normalizedValue>
				<recognizedValue>
					<text>X0773</text>
				</recognizedValue>
			</cardNumber>
		</payment>
		<recognizedItems count="34">
			<item index="1">
				<name>
					<text>Iszt. xll,10 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>11.10</normalizedValue>
					<recognizedValue>
						<text>11.10</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Iszt. xll,10 11.10]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>BecksPiwo0,5L H Iszt. x2,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.99</normalizedValue>
					<recognizedValue>
						<text>2.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[BecksPiwo0,5L H Iszt. x2,99 2.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>BacardiCartaBlanc</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.70</normalizedValue>
					<recognizedValue>
						<text>O,7</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[BacardiCartaBlancO,7]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>Iszt. x49,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>49.99</normalizedValue>
					<recognizedValue>
						<text>49,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Iszt. x49,99 49,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>Pepsi IL H Iszt. x2,89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.89</normalizedValue>
					<recognizedValue>
						<text>2,89</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Pepsi IL H Iszt. x2,89 2,89]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>Pepsi IL H Iszt. x2,89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.89</normalizedValue>
					<recognizedValue>
						<text>2.89</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Pepsi IL H Iszt. x2,89 2.89]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>Iszt. xl0,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>10.99</normalizedValue>
					<recognizedValue>
						<text>10.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Iszt. xl0,99 10.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="8">
				<name>
					<text>Iszt. X10.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>10.99</normalizedValue>
					<recognizedValue>
						<text>10.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Iszt. X10.99 10.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="9">
				<name>
					<text>VelvetPapierTodl8szt H Iszt. x5,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.99</normalizedValue>
					<recognizedValue>
						<text>5,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[VelvetPapierTodl8szt H Iszt. x5,99 5,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="10">
				<name>
					<text>Budweiser0.33BTL H Iszt. x2 99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.99</normalizedValue>
					<recognizedValue>
						<text>2,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Budweiser0.33BTL H Iszt. x2 99 2,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="11">
				<name>
					<text>UHT BezLaktozy3,2%lL t Iszt. x2,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.99</normalizedValue>
					<recognizedValue>
						<text>2,990</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[UHT BezLaktozy3,2%lL t Iszt. x2,99 2,990]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="12">
				<name>
					<text>LenorPłynDoPł</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.42</normalizedValue>
					<recognizedValue>
						<text>l,42</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[LenorPłynDoPłl,42]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="13">
				<name>
					<text>Iszt. X16.99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>16.99</normalizedValue>
					<recognizedValue>
						<text>16,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Iszt. X16.99 16,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="14">
				<name>
					<text>MajonezNapoleońskii Iszt. x3,89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.90</normalizedValue>
					<recognizedValue>
						<text>3,89B</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[MajonezNapoleońskii Iszt. x3,89 3,89B]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="15">
				<name>
					<text>MajonezNapoleońskii Iszt. x3,89 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.90</normalizedValue>
					<recognizedValue>
						<text>3,89B</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[MajonezNapoleońskii Iszt. x3,89 3,89B]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="16">
				<name>
					<text>MorlinyBerlinki250Gt Iszt. x4.59 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.59</normalizedValue>
					<recognizedValue>
						<text>4,590</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[MorlinyBerlinki250Gt Iszt. x4.59 4,590]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="17">
				<name>
					<text>LactacydFeminaPłyn H Iszt. x9,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>9.99</normalizedValue>
					<recognizedValue>
						<text>9.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[LactacydFeminaPłyn H Iszt. x9,99 9.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="18">
				<name>
					<text>OlejWielkopolskiIIt Iszt x3,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.99</normalizedValue>
					<recognizedValue>
						<text>3,990</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[OlejWielkopolskiIIt Iszt x3,99 3,990]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="19">
				<name>
					<text>OlejWielkopolskilit Iszt. x3,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.99</normalizedValue>
					<recognizedValue>
						<text>3,990</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[OlejWielkopolskilit Iszt. x3,99 3,990]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="20">
				<name>
					<text>0,248kg x47,90 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>11.88</normalizedValue>
					<recognizedValue>
						<text>11,880</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0,248kg x47,90 11,880]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="21">
				<name>
					<text>LaysKarbPaprykl30gt Iszt. x3,79 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.79</normalizedValue>
					<recognizedValue>
						<text>3,790</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[LaysKarbPaprykl30gt Iszt. x3,79 3,790]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="22">
				<name>
					<text>LaysPaprykalSOgt Iszt. x4,85 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.85</normalizedValue>
					<recognizedValue>
						<text>4,850</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[LaysPaprykalSOgt Iszt. x4,85 4,850]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="23">
				<name>
					<text>ChlebWiejskiSOOg t Iszt. x3,79 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.79</normalizedValue>
					<recognizedValue>
						<text>3,790</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[ChlebWiejskiSOOg t Iszt. x3,79 3,790]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="24">
				<name>
					<text>ToffifeeBomboniel25g H Iszt. x7,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>7.99</normalizedValue>
					<recognizedValue>
						<text>7.99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[ToffifeeBomboniel25g H Iszt. x7,99 7.99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="25">
				<name>
					<text>0.172kg x58,90 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>10.13</normalizedValue>
					<recognizedValue>
						<text>10,130</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0.172kg x58,90 10,130]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="26">
				<name>
					<text>0,548kg x5,99 </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>3.28</normalizedValue>
					<recognizedValue>
						<text>3,280</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[0,548kg x5,99 3,280]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="27">
				<name>
					<text>Podsuma: </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>196.85</normalizedValue>
					<recognizedValue>
						<text>196,85</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Podsuma: 196,85]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="28">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA A </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>135.79</normalizedValue>
					<recognizedValue>
						<text>135,79</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA A 135,79]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="29">
				<name>
					<text>PTU A 23,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>25.39</normalizedValue>
					<recognizedValue>
						<text>25,39</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU A 23,00 % 25,39]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="30">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA B </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>7.78</normalizedValue>
					<recognizedValue>
						<text>7.78</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA B 7.78]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="31">
				<name>
					<text>PTU B 8,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>0.58</normalizedValue>
					<recognizedValue>
						<text>0,58</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU B 8,00 % 0,58]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="32">
				<name>
					<text>SPRZEDAŻ OPODATKOWANA C </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>53.28</normalizedValue>
					<recognizedValue>
						<text>53,28</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SPRZEDAŻ OPODATKOWANA C 53,28]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="33">
				<name>
					<text>PTU 0 5,00 % </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.54</normalizedValue>
					<recognizedValue>
						<text>2,54</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[PTU 0 5,00 % 2,54]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="34">
				<name>
					<text>SUMA PTU </text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>28.51</normalizedValue>
					<recognizedValue>
						<text>28,51</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[SUMA PTU 28,51]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[Kaufland Polska Markety Sp.z o.o.Sp.J    
Al.Armii Krajowej 47, 50-541 Wroclaw     
Nr BDO 000013346                         
ul. Zegrzyńska 7                         
05-110 Jabłonna 6160                     
NIP 899-23-67 273                        
2021 02-25 08:59                  628809 
PARAGON FISKAL MY                        
Żyw iecZdrójWoda6x1,5 H                  
Iszt. xll,10 11.10A                      
BecksPiwo0,5L H Iszt. x2,99 2.99A        
BacardiCartaBlancO,7 H#                  
Iszt. x49,99 49,99A                      
Pepsi IL              H Iszt. x2,89 2,89A
Pepsi IL              H Iszt. x2,89 2.89A
NiveaDeoSpray250mI H                     
Iszt. xl0,99 10.99A                      
NiveaDeoSpray250ml H                     
Iszt. X10.99 10.99A                      
VelvetPapierTodl8szt H Iszt. x5,99 5,99A 
Budweiser0.33BTL     H Iszt. x2 99 2,99A 
UHT BezLaktozy3,2%lL t Iszt. x2,99 2,990 
LenorPłynDoPłl,42    H                   
Iszt. X16.99 16,99A                      
MajonezNapoleońskii    Iszt. x3,89 3,89B 
MajonezNapoleońskii    Iszt. x3,89 3,89B 
MorlinyBerlinki250Gt   Iszt. x4.59 4,590 
LactacydFeminaPłyn   H Iszt. x9,99 9.99A 
OlejWielkopolskiIIt    Iszt  x3,99 3,990 
OlejWielkopolskilit    Iszt. x3,99 3,990 
KabanosDrobiowyExtKg t                   
0,248kg x47,90 11,880                    
LaysKarbPaprykl30gt    Iszt. x3,79 3,790 
LaysPaprykalSOgt       Iszt. x4,85 4,850 
ChlebWiejskiSOOg t Iszt. x3,79 3,790     
Toff ifeeBomboniel25g H Iszt. x7,99 7.99A
PapryczkIZSeremKg t                      
0.172kg x58,90 10,130                    
Pomarańcze kg        t                   
0,548kg x5,99 3,280                      
Podsuma:                           196,85
SPRZEDAŻ OPODATKOWANA A           135,79 
PTU A 23,00 %                       25,39
SPRZEDAŻ OPODATKOWANA B             7.78 
PTU B 8,00 %                         0,58
SPRZEDAŻ OPODATKOWANA C            53,28 
PTU 0 5,00 %                         2,54
SUMA PTU                           28,51 
SUMA PLN 196,85                          
00057 #008 0111         2021-02-25 09:00 
628775D37AA56EB1BBI1C974573EEE90A06B61EC 
/*P CBR 1601312338                       
9 26.5(2: :225X8)1): 06                  
Karta                          196,85 PLN
Nr transakcji:                  8-111-106
# Za te produkty nie są                  
przyznawane labaty, ani                  
punkty PAYBACK                           
Twoje dzisiejsze korzyści                
z PAYBACK w Kauflandzie                  
Numer PAYBACK:XXXXXXXXX0773              
Punkty podstawowe:       48              
Dostępne punkty:        265              
BEZPŁATNA INI-OI TNIA. 800 300 062       
Koi hamy oszczędzanie!                   
Naprawdę niskie ceny w Kalif landzie!    
]]></recognizedText>
	</receipt>
</receipts>
