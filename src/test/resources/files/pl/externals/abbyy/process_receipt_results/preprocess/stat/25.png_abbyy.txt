﻿<?xml version="1.0" encoding="UTF-8"?>
<receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd">
	<receipt currency="PLN" rotation="RT_NoRotation">
		<vendor>
			<name>
				<recognizedValue>
					<text>Biedrpnkd</text>
				</recognizedValue>
			</name>
			<fullAddress>
				<text><![CDATA[X* Codziennie niskie ceny /
Sklep 4890 ul. Siwińskiego 2, 05-120 L
eg ionowoJeronimo Martins Polska 5.A.
ul. Żniwna 5, 62-025 Kostrzyn
NIP 779-10-11-327]]></text>
			</fullAddress>
			<zip>
				<normalizedValue>05120</normalizedValue>
				<recognizedValue>
					<text>05
120</text>
				</recognizedValue>
			</zip>
			<vatNumber>
				<normalizedValue>7791011327</normalizedValue>
				<recognizedValue>
					<text>779-10-11-327</text>
				</recognizedValue>
			</vatNumber>
		</vendor>
		<date>
			<normalizedValue>2021-02-06</normalizedValue>
			<recognizedValue>
				<text>2021-02-06</text>
			</recognizedValue>
		</date>
		<time>
			<normalizedValue>13:42:00</normalizedValue>
			<recognizedValue>
				<text>13:42</text>
			</recognizedValue>
		</time>
		<total>
			<normalizedValue>61.61</normalizedValue>
			<recognizedValue>
				<text>61,61</text>
			</recognizedValue>
		</total>
		<tax total="false" rate="5">
			<normalizedValue>2.93</normalizedValue>
			<recognizedValue>
				<text>5,00 % 2,93</text>
			</recognizedValue>
		</tax>
		<payment type="Card" cardType="Visa">
			<value>
				<normalizedValue>61.61</normalizedValue>
				<recognizedValue>
					<text>61,61</text>
				</recognizedValue>
			</value>
			<cardNumber>
				<normalizedValue/>
			</cardNumber>
		</payment>
		<recognizedItems count="7">
			<item index="1">
				<name>
					<text>FrySteFrAviko750g 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.99</normalizedValue>
					<recognizedValue>
						<text>5,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[FrySteFrAviko750g 6 1 x5,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="2">
				<name>
					<text>Polędwica Sop 40g 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>5.99</normalizedValue>
					<recognizedValue>
						<text>5,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Polędwica Sop 40g 6 1 x5,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="3">
				<name>
					<text>TrioWarzMroKra750g 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>4.49</normalizedValue>
					<recognizedValue>
						<text>4 49</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[TrioWarzMroKra750g 6 1 x4 49]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="4">
				<name>
					<text>Poii 6herr Gał 5 009 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>8.99</normalizedValue>
					<recognizedValue>
						<text>8,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Poii 6herr Gał 5 009 6 1 x8,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="5">
				<name>
					<text>luz 0,525 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>18.99</normalizedValue>
					<recognizedValue>
						<text>18,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[luz 0,525 x18,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="6">
				<name>
					<text>CZOSNEK szt 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>1.99</normalizedValue>
					<recognizedValue>
						<text>1,99</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[CZOSNEK szt 6 1 x1,99]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
			<item index="7">
				<name>
					<text>Ehleb rodzmny410g 6 1 x</text>
				</name>
				<count>
					<normalizedValue>1.000</normalizedValue>
				</count>
				<total>
					<normalizedValue>2.39</normalizedValue>
					<recognizedValue>
						<text>2 39</text>
					</recognizedValue>
				</total>
				<recognizedText><![CDATA[Ehleb rodzmny410g 6 1 x2 39]]></recognizedText>
				<amountUnits>unit</amountUnits>
			</item>
		</recognizedItems>
		<country>
			<normalizedValue>POL</normalizedValue>
		</country>
		<recognizedText><![CDATA[Biedrpnkd)                                                        
X* Codziennie niskie ceny /                                       
Sklep 4890 ul. Siwińskiego 2, 05-120 L                            
eg ionowoJeronimo Martins Polska 5.A.                             
ul. Żniwna 5, 62-025 Kostrzyn                                     
NIP 779-10-11-327                                                 
 2021-02-06 13:42                                   153760       
PARAGON FISKALNY                                                  
Rosati fil kg r 0,322 x59,90 19,296                               
 FrySteFrAviko750g        6                 1 x5,99 5,996        
Polędwica Sop 40g       6                  1 x5,99 5,996          
TrioWarzMroKra750g       6                  1 x4 49 4 496         
NAT SZ!       C              1 xU9 M9C                            
Poii 6herr Gał 5 009        6                   1 x8,99 8^996     
luz                         0,525 x18,99 9,976                    
CZOSNEK szt               6                  1 x1,99 1,996        
Ehleb rodzmny410g       6                 1 x2 39 2 396           
Pomarańcze Des Luz       6             0,410 x5^99 2^466          
Rabat                                                  44         
<026                                                              
SPRZEDAŻ OPODATKOWANA 6  6T,'61                                   
PTU 6 5,00 %                                           2,93       
SUMA PTU                                             2,93         
SUMA PLN! /           61,61                                       
00708 Masa 15 Kasjer nr 4              2021-02-06 13:42           
F90664BD5996A0077F7325E28B3714AD2D462454                          
/zP CCH 1701376811                                                
Nr sys. 7155                                                      
Karta VISA 07 1                                 61,61 PLN         
Nr transakcji:                                        7155        
Numer:                                    4890210206/15515        
Udzielono łącznie rabatów:                           1,44         
Numer karty:         '                       99515*****908        
NIP 779-10-11-327                                                 
2021-02-06 13,42 „ , „ ,                       «3761              
KUPUJ CO POLSKIE                                                  
WARTOŚĆ "KUPUJ CO POLSKIE" W TWOICH ZAKUPACH,                     
Z WYŁ ĄCZENIEM PRODUKTÓW NIEBIORĄ CYCH UDZIAŁU W AKCJI, WYNOSI:   
%%% 42,13 zł %%%                                                  
"““"WIĘCEJ NA BIEDRONKA.Pl/KUPUJCOPOLSKIE                         
]]></recognizedText>
	</receipt>
</receipts>
