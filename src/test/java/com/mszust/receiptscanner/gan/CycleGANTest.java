package com.mszust.receiptscanner.gan;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

class CycleGANTest extends IntegrationTest {


    @Test
    void cycleGANTest20() throws URISyntaxException, IOException, DiffException {
        for (int testNo : List.of(20, 21)) {
            for (int epoch : List.of(5, 10, 20, 30, 40)) {
                cycleGANTest(testNo, epoch);
            }
        }
    }

    void cycleGANTest(int testNo, int epoch) throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        final File parentDir = FileUtils.file(
                String.format(
                        "/files/pl/paragon1/gan/cyclegan/test%d/epoch_%d/images",
                        testNo,
                        epoch
                )
        );

        final File[] files = parentDir.listFiles();

        for (File file : files) {
            final String fileName = getFileName(file) + String.format("-cycleGAN-test%d-epoch%d", testNo, epoch);
            final BufferedImage processed = ImageIO.read(file);

            final var result = scannerService.processFile(new Scannable(processed, fileName));

            final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }

        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, String.format("cycleGAN-test%d-epoch%d", testNo, epoch));
    }

    @Test
    void cycleGANTest2() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();


        for (var test : testFiles().entrySet()) {
            for (int epoch : test.getValue()) {

                final String testName = test.getKey();
                final File parentDir = FileUtils.file(
                        String.format(
                                "/files/pl/paragon1/gan/cyclegan/%s/epoch_%d/",
                                testName,
                                epoch
                        )
                );

                final File[] files = parentDir.listFiles();

                for (File file : files) {
                    final String fileName = getFileName(file) + String.format("-cycleGAN-%s-epoch_%d", testName, epoch);
                    final BufferedImage processed = ImageIO.read(file);

                    final var result = scannerService.processFile(new Scannable(processed, fileName));

                    final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                    comparisonResult.setDuration(result.getOcrDuration());

                    comparisonResults.add(comparisonResult);
                }
            }
        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cycleGAN-generated1");
    }

    private Map<String, List<Integer>> testFiles() {
        return Map.of(
                "test-cyclegan-c-c-256-128", List.of(5, 10, 20, 40, 60),
                "test-cyclegan-c-c-256-256", List.of(5, 10, 20, 40, 60),
                "test-cyclegan-c-r-100-100", List.of(5, 10, 25, 50, 100, 150, 200),
                "test-cyclegan-c-r-100-100-default-preprocess", List.of(5, 10, 25, 50, 100, 150, 200),
                "test-cyclegan-c-r-400-200", List.of(5, 10, 25, 50, 100, 200, 300, 400, 600),
                "test-cyclegan-r-c-256-128", List.of(5, 10, 20, 40, 60),
                "test-cyclegan-r-c-256-256", List.of(5, 10, 20, 40, 60),
                "test-cyclegan-r-r-100-100", List.of(5, 10, 25, 50, 100, 150, 200),
                "test-cyclegan-r-r-100-100-default-preprocess", List.of(5, 10, 25, 50, 100, 150, 200),
                "test-cyclegan-r-r-400-200", List.of(5, 10, 25, 50, 100, 200, 300, 400, 600)
        );
    }
}
