package com.mszust.receiptscanner.statistics;

import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.preprocessing.Preprocessor;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.util.StopWatch;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

class BasicTesseractTest extends StatisticsTest {

    @SneakyThrows
    @Test
    void basicTesseractWithBestData() {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        final var testFilesWithBenchmark = getTestFilesWithBenchmark();
        for (File testFile : testFilesWithBenchmark.keySet()) {
            System.out.println("Reading file " + testFile.getName());

            final StopWatch sw = new StopWatch();
            sw.start();
            final Mat src = Imgcodecs.imread(testFile.getAbsolutePath());
            final BufferedImage bufferedImage = Preprocessor.preprocess(src);
//            final BufferedImage bufferedImage = OpenCvUtils.toBufferedImage(cropped);
//            final BufferedImage bufferedImage = OpenCvUtils.toBufferedImage(preprocessed);
//            OpenCvUtils.writeImage(file(WORKING_DIR).getParentFile().getAbsolutePath() + "/result/zdjecia/preprocess/" + getFileName(testFile) + ".png", bufferedImage);
//            OpenCvUtils.writeImage(file(BENCHMARK_LOCATION).getParentFile().getAbsolutePath() + "/result/zdjecia/stat/preprocess/" + getFileName(testFile) + ".png", bufferedImage);
//
//            final var result = scannerService.processFile(testFile);
            final var result = scannerService.processFile(new Scannable(bufferedImage, FilenameUtils.removeExtension(testFile.getName())));
//            final var result = scannerService.processFile(scannable);
            sw.stop();

            final var benchmarkFile = testFilesWithBenchmark.get(testFile);
            final var comparisonResult = lineComparator.compareLines(
                    getFileName(testFile),
                    getFileName(benchmarkFile),
                    readFile(benchmarkFile),
                    result.getRecognizedLines()
            );
//            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResult.setDuration(sw.getTotalTimeMillis());
            comparisonResults.add(comparisonResult);
        }

        final var htmlReport = reportGenerator.generateHtmlReport(comparisonResults);
        final var csvReport = reportGenerator.generateCsvReport(comparisonResults);

        saveResultFile(htmlReport, WORKING_DIR, "stats-preprocess-all-imgs", "html");
        saveResultFile(csvReport, WORKING_DIR, "stats-preprocess-all-imgs", "csv");
    }
}
