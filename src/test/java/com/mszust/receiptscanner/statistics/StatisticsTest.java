package com.mszust.receiptscanner.statistics;

import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.file.FileUtils;
import lombok.SneakyThrows;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StatisticsTest extends IntegrationTest {
    private static final Pattern TEST_FILE_PATTERN = Pattern.compile(".*\\.(jpg|jpeg|png|bmp)$");
    protected static final Pattern BENCHMARK_FILE_PATTERN = Pattern.compile(".*\\.(txt)$");
    protected static final String WORKING_DIR = "/files/pl/paragony/";

    @SneakyThrows
    protected Map<File, File> getTestFilesWithBenchmark() {
        final Map<File, File> testFilesWithBenchmark = new LinkedHashMap<>();

        final File[] files = getFilesInDir(WORKING_DIR);

        if (files == null) {
            return testFilesWithBenchmark;
        }

        final Map<String, File> testFiles = Arrays.stream(files)
                .filter(file -> TEST_FILE_PATTERN.matcher(file.getName().toLowerCase()).matches())
                .collect(
                        Collectors.toMap(
                                File::getName,
                                Function.identity()
                        )
                );

        final Map<String, File> benchmarkFiles = Arrays.stream(files)
                .filter(file -> BENCHMARK_FILE_PATTERN.matcher(file.getName()).matches())
                .collect(
                        Collectors.toMap(
                                File::getName,
                                Function.identity()
                        )
                );

        testFiles.forEach((k, v) -> {
                    final File benchmarkFile = benchmarkFiles.get(FileUtils.getFileName(v) + ".txt");
                    if (benchmarkFile != null) {
                        testFilesWithBenchmark.put(v, benchmarkFile);
                    }
                }
        );

        return testFilesWithBenchmark;
    }

    @SneakyThrows
    protected Map<String, File> getBenchmarks() {
        final File[] files = getFilesInDir(WORKING_DIR);

        if (files == null) {
            return Map.of();
        }

        return Arrays.stream(files)
                .filter(file -> BENCHMARK_FILE_PATTERN.matcher(file.getName()).matches())
                .collect(
                        Collectors.toMap(
                                FileUtils::getFileName,
                                Function.identity()
                        )
                );
    }

    protected File[] getFilesInDir(String directory) throws URISyntaxException {
        final File workingDir = FileUtils.file(directory);
        return workingDir.listFiles();
    }

}
