package com.mszust.receiptscanner.statistics;

import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.preprocessing.Preprocessor;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.springframework.util.StopWatch;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@Slf4j
class FullPreprocessingTest extends StatisticsTest {

    @SneakyThrows
    @Test
    void fullPreprocessing() {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        final var testFilesWithBenchmark = getTestFilesWithBenchmark();
        for (File testFile : testFilesWithBenchmark.keySet()) {
            log.debug("Processing file: " + testFile.getName());

            final StopWatch sw = new StopWatch();
            sw.start();
            final BufferedImage preprocessed = Preprocessor.preprocess(testFile);
            final var result = scannerService.processFile(new Scannable(preprocessed, FilenameUtils.removeExtension(testFile.getName())));
            sw.stop();

            final var benchmarkFile = testFilesWithBenchmark.get(testFile);
            final var comparisonResult = lineComparator.compareLines(
                    getFileName(testFile),
                    getFileName(benchmarkFile),
                    readFile(benchmarkFile),
                    result.getRecognizedLines()
            );
            comparisonResult.setDuration(sw.getTotalTimeMillis());
            comparisonResults.add(comparisonResult);
        }

        final var report = reportGenerator.generateCsvReport(comparisonResults);

        saveResultFile(report, WORKING_DIR, "stats_preprocessed");
    }
}
