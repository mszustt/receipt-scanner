package com.mszust.receiptscanner.externals.opensource;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

class GocrResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Test
    void testGocrResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : gocrFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "gocr_uczenie2", "csv");
    }

    private static List<String> gocrFiles() {
        return List.of(
                "/files/pl/externals/gocr/uczenie/IMG_0297_podczas_uczenia.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0299_po_uczeniu.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0300_po_uczeniu.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0297_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0299_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0300_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0301_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0302_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0304_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0305_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/IMG_0306_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/scan_300dpi_uczenie2.txt",
                "/files/pl/externals/gocr/uczenie/scan_600dpi_uczenie2.txt"
        );
    }

    @Test
    @SneakyThrows
    void gocrStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.txt)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "preprocess_stat", "preprocess/stat",
                "preprocess_stat_db", "preprocess/stat-db",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/gocr/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));
                final var result = readFile(testFile.getValue());
                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "gocr_" + test.getKey(), "csv");
        }
    }
}
