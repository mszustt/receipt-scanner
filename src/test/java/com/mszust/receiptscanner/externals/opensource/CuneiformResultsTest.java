package com.mszust.receiptscanner.externals.opensource;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.report.ReportGenerator;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CuneiformResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private ReportGenerator reportGenerator;

    @Test
    void testCuneiFormResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : cuneiformFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cuneiform_", "csv");
    }

    private static List<String> cuneiformFiles() {
        return List.of(
                "/files/pl/externals/cuneiform/IMG_0301_no_margin_cuneiform.txt",
                "/files/pl/externals/cuneiform/scan_300dpi_cuneiform.txt"
        );
    }

    @Test
    @SneakyThrows
    void cuneiFormStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.txt)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "preprocess_stat", "preprocess/stat",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/cuneiform/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));
                final var result = readFile(testFile.getValue());
                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "cuneiform_30_" + test.getKey(), "csv");
        }
    }

    @Test
    @SneakyThrows
    void testCuneiformFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/cuneiform/" + test.getValue()));
            for (File testFile : testFiles) {
                final var result = readFile(testFile);

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, result);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "cuneiform_" + test.getKey(), "html");
        }
    }
}
