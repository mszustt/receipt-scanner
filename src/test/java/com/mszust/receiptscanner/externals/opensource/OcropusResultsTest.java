package com.mszust.receiptscanner.externals.opensource;

import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

@Slf4j
class OcropusResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Test
    @SneakyThrows
    void ocropusStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.html)");
        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_stat", "preprocess/stat",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/ocropus/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));
                final var result = parseHtml(testFile.getValue());


                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "ocropus" + test.getKey(), "csv");
        }
    }

    @Test
    @SneakyThrows
    void ocropusFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/ocropus/" + test.getValue()));
            for (File testFile : testFiles) {
                final var result = parseHtml(testFile);

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, result);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "ocropus_" + test.getKey(), "html");
            final var csvReport = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(csvReport, BENCHMARK_LOCATION, "ocropus_" + test.getKey(), "csv");
        }
    }

    @SneakyThrows
    private static List<String> parseHtml(File file) {
        final Document document = loadDocument(file);
        final Elements lineSpans = document.getElementsByClass("ocr_line");
        final List<String> lines = new ArrayList<>();
        for (Element element : lineSpans) {
            log.info(element.text());
            lines.add(element.text());
        }
        return lines;
    }

    private static Document loadDocument(File file) throws IOException {
        try {
            return Jsoup.parse(file, "UTF-8", "");
        } catch (IOException e) {
            log.error("Could not parse HTML file {}", file.getAbsolutePath());
            throw e;
        }
    }
}
