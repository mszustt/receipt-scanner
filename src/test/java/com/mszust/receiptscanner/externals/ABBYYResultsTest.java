package com.mszust.receiptscanner.externals;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.trimAndReplaceWhitespaces;
import static java.util.stream.Collectors.toMap;

class ABBYYResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    private static final XmlMapper xmlMapper = new XmlMapper();

    @BeforeAll
    static void setUp() {
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Test
    @SneakyThrows
    void abbyyImageStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.json)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "preprocess_stat", "preprocess/stat",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/abbyy/process_image_results/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));

                final var result = readFile(testFile.getValue());

                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "abby_image_" + test.getKey(), "csv");
        }
    }

    @Test
    @SneakyThrows
    void abbyImageFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/abbyy/process_image_results/" + test.getValue()));
            for (File testFile : testFiles) {
                final var result = trimAndReplaceWhitespaces(readFile(testFile));
                result.forEach(System.out::println);

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, result);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "abby_image_" + test.getKey(), "csv");
            final var htmlReport = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(htmlReport, BENCHMARK_LOCATION, "abby_image_" + test.getKey(), "html");
        }
    }

    @Test
    @SneakyThrows
    void abbyyReceiptStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.txt)");
        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_stat", "preprocess/stat",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/abbyy/process_receipt_results/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));

                final var result = xmlMapper.readValue(testFile.getValue(), ABBYYResultsTest.Receipts.class);
//                final var result = readFile(testFile);
                final var textLines = trimAndReplaceWhitespaces(
                        result.getReceipt().getRecognizedText() != null
                                ? Arrays.stream(result.getReceipt().getRecognizedText().split("\\n")).collect(Collectors.toList())
                                : List.of()
                );

                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        textLines
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "abby_receipt_" + test.getKey(), "csv");
        }
    }

    @Test
    @SneakyThrows
    void abbyReceiptFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/abbyy/process_receipt_results/" + test.getValue()));
            for (File testFile : testFiles) {

                final var result = xmlMapper.readValue(testFile, ABBYYResultsTest.Receipts.class);
//                final var result = readFile(testFile);
                final var textLines = trimAndReplaceWhitespaces(
                        Arrays.stream(result.getReceipt().getRecognizedText().split("\\n")).collect(Collectors.toList())
                );

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, textLines);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "abby_receipt_" + test.getKey(), "csv");
            final var htmlReport = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(htmlReport, BENCHMARK_LOCATION, "abby_receipt_" + test.getKey(), "html");
        }
    }

    @Data
    @NoArgsConstructor
    public static class Receipts {
        private Receipt receipt;
    }

    @Data
    @NoArgsConstructor
    public static class Receipt {
        private String recognizedText;
    }

}
