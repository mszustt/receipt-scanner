package com.mszust.receiptscanner.externals;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

class GoogleVisionAiResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGoogleVisionResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : googleVisionAiFiles()) {
            final var file = file(fileLocation);

            final var result = objectMapper.readValue(file, GoogleVisionAiModel.class);
            final var textLines = Arrays.stream(result.getFullTextAnnotation().getText().split("\\n")).collect(Collectors.toList());

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, textLines);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "google_vision_ai");
    }

    @Test
    void testGoogleVisionRawResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : googleVisionAiRawFiles()) {
            final var file = file(fileLocation);

            final var result = objectMapper.readValue(file, GoogleVisionAiModel.class);
            final var textLines = Arrays.stream(result.getFullTextAnnotation().getText().split("\\n")).collect(Collectors.toList());

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, textLines);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "google_vision_ai_raw");
    }

    private static List<String> googleVisionAiFiles() {
        return List.of(
                "/files/pl/externals/google_vision_ai/IMG_0297.txt",
                "/files/pl/externals/google_vision_ai/IMG_0299.txt",
                "/files/pl/externals/google_vision_ai/IMG_0300.txt",
                "/files/pl/externals/google_vision_ai/IMG_0301.txt",
                "/files/pl/externals/google_vision_ai/IMG_0302.txt",
                "/files/pl/externals/google_vision_ai/IMG_0304.txt",
                "/files/pl/externals/google_vision_ai/IMG_0305.txt",
                "/files/pl/externals/google_vision_ai/IMG_0306.txt",
                "/files/pl/externals/google_vision_ai/scan_300dpi.txt",
                "/files/pl/externals/google_vision_ai/scan_600dpi.txt"
        );
    }

    private static List<String> googleVisionAiRawFiles() {
        return List.of(
                "/files/pl/externals/google_vision_ai/raw/IMG_0297.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0299.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0300.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0301.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0302.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0304.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0305.txt",
                "/files/pl/externals/google_vision_ai/raw/IMG_0306.txt",
                "/files/pl/externals/google_vision_ai/raw/scan_300dpi.txt",
                "/files/pl/externals/google_vision_ai/raw/scan_600dpi.txt"
        );
    }

    @Data
    private static class GoogleVisionAiModel {
        @JsonProperty("fullTextAnnotation")
        private FullTextAnnotation fullTextAnnotation;

        @Data
        private class FullTextAnnotation {
            @JsonProperty("text")
            private String text;
        }
    }


    @Test
    @SneakyThrows
    void visionStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.txt)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
//                "preprocess_stat", "preprocess/stat",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/google_vision_ai/gcloud/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));
                final var result = readFile(testFile.getValue());
                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResult.setDuration(0L);
                comparisonResults.add(comparisonResult);
            }
//            final var report = reportGenerator.generateCsvReport(comparisonResults);
//            saveResultFile(report, BENCHMARK_LOCATION, "vision_" + test.getKey(), "csv");
            final var report = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "vision_" + test.getKey(), "html");
        }
    }

    @Test
    @SneakyThrows
    void visionFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/google_vision_ai/gcloud/" + test.getValue()));
            for (File testFile: testFiles) {
                final var result = readFile(testFile);

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, result);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "vision_" + test.getKey(), "html");
            final var reportCsv = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(reportCsv, BENCHMARK_LOCATION, "vision_" + test.getKey(), "csv");
        }
    }
}
