package com.mszust.receiptscanner.externals;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class NanonetsBasicResultsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/externals/nanonets/pro-basic/nanonets-benchmark.txt";

    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;

    @Test
    void testNanonetsBasicResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : nanonetsFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "nanonets-basic");
    }

    private static List<String> nanonetsFiles() {
        return List.of(
                "/files/pl/externals/nanonets/pro-basic/IMG_0297.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0299.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0300.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0301.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0302.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0304.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0305.txt",
                "/files/pl/externals/nanonets/pro-basic/IMG_0306.txt",
                "/files/pl/externals/nanonets/pro-basic/scan_300dpi.txt",
                "/files/pl/externals/nanonets/pro-basic/scan_600dpi.txt"
        );
    }
}
