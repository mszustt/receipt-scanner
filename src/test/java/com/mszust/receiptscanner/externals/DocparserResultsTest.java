package com.mszust.receiptscanner.externals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class DocparserResultsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testDocparserResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : docparserFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "docparser");
    }

    private static List<String> docparserFiles() {
        return List.of(
                "/files/pl/externals/docparser/IMG_0297.txt",
                "/files/pl/externals/docparser/IMG_0298.txt",
                "/files/pl/externals/docparser/IMG_0299.txt",
                "/files/pl/externals/docparser/IMG_0300.txt",
                "/files/pl/externals/docparser/IMG_0301.txt",
                "/files/pl/externals/docparser/IMG_0302.txt",
                "/files/pl/externals/docparser/IMG_0303.txt",
                "/files/pl/externals/docparser/IMG_0304.txt",
                "/files/pl/externals/docparser/IMG_0305.txt",
                "/files/pl/externals/docparser/IMG_0306.txt",
                "/files/pl/externals/docparser/scan_300dpi.txt",
                "/files/pl/externals/docparser/scan_600dpi.txt"
        );
    }
}
