package com.mszust.receiptscanner.externals;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.report.csv.CsvReportBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class AwsRekognitionResultsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private CsvReportBuilder csvReportBuilder;

    @Test
    void testAwsRekognitionResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : awsRekognitionFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = csvReportBuilder.generateReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "aws-rekognition", "csv");
    }

    private static List<String> awsRekognitionFiles() {
        return List.of(
                "/files/pl/externals/rekognition/raw/IMG_0297.txt",
                "/files/pl/externals/rekognition/raw/IMG_0299.txt",
                "/files/pl/externals/rekognition/raw/IMG_0300.txt",
                "/files/pl/externals/rekognition/raw/IMG_0301.txt",
                "/files/pl/externals/rekognition/raw/IMG_0302.txt",
                "/files/pl/externals/rekognition/raw/IMG_0304.txt",
                "/files/pl/externals/rekognition/raw/IMG_0305.txt",
                "/files/pl/externals/rekognition/raw/IMG_0306.txt",
                "/files/pl/externals/rekognition/raw/scan_300dpi.txt",
                "/files/pl/externals/rekognition/raw/scan_600dpi.txt"
        );
    }

}
