package com.mszust.receiptscanner.externals;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

class BlinkReceiptResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testBlinkReceiptResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : blinkReceiptFiles()) {
            final var file = file(fileLocation);

            final var result = objectMapper.readValue(file, BlinkReceiptResultModel.class);
            final var textLines = Arrays.stream(result.getText().split("\\n")).collect(Collectors.toList());

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, textLines);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "blinkreceipt");
    }

    private static List<String> blinkReceiptFiles() {
        return List.of(
                "/files/pl/externals/blinkreceipt/response_IMG_0297.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0298.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0299.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0300.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0301.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0302.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0303.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0304.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0305.json",
                "/files/pl/externals/blinkreceipt/response_IMG_0306.json",
                "/files/pl/externals/blinkreceipt/response_scan_300.json",
                "/files/pl/externals/blinkreceipt/response_scan_600.json"
        );
    }

    @Test
    @SneakyThrows
    void blinkReceiptStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.json)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "preprocess_stat", "preprocess/stat",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/blinkreceipt/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));

                final var result = objectMapper.readValue(testFile.getValue(), BlinkReceiptResultModel.class);

                final var textLines = Arrays.stream(result.getText().split("\\n")).collect(Collectors.toList());
                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        textLines
                );

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "blinkreceipt_" + test.getKey(), "csv");
        }
    }

    @Test
    @SneakyThrows
    void testBlinkReceiptFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/blinkreceipt/" + test.getValue()));
            for (File testFile: testFiles) {
                final var result = objectMapper.readValue(testFile, BlinkReceiptResultModel.class);
                final var textLines = Arrays.stream(result.getText().split("\\n")).collect(Collectors.toList());

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, textLines);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "blinkreceipt_" + test.getKey(), "csv");
        }
    }

    @Data
    private static class BlinkReceiptResultModel {
        @JsonProperty("raw_text")
        private String text;
    }
}
