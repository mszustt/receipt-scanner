package com.mszust.receiptscanner.externals;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.statistics.StatisticsTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static java.util.stream.Collectors.toMap;

class AwsTextractResultsTest extends StatisticsTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Test
    void testAwsRekognitionResults() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : awsTextractFiles()) {
            final var file = file(fileLocation);

            final var result = readFile(fileLocation);

            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result);
            comparisonResult.setDuration(0L);

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "aws-textract", "csv");
    }

    private static List<String> awsTextractFiles() {
        return List.of(
                "/files/pl/externals/textract/IMG_0297.txt",
                "/files/pl/externals/textract/IMG_0299.txt",
                "/files/pl/externals/textract/IMG_0300.txt",
                "/files/pl/externals/textract/IMG_0301.txt",
                "/files/pl/externals/textract/IMG_0302.txt",
                "/files/pl/externals/textract/IMG_0304.txt",
                "/files/pl/externals/textract/IMG_0305.txt",
                "/files/pl/externals/textract/IMG_0306.txt",
                "/files/pl/externals/textract/scan_300dpi.txt",
                "/files/pl/externals/textract/scan_600dpi.txt"
        );
    }

    @Test
    @SneakyThrows
    void textractStatResults() {
        final Pattern filePattern = Pattern.compile("^(\\d+)(?:\\..*\\.txt)");
        final Map<String, String> testsWithDirs = Map.of(
//                "preprocess_fun", "preprocess/fun/",
                "preprocess_stat", "preprocess/stat",
//                "raw_fun", "raw/fun",
                "raw_stat", "raw/stat"
        );

        final Map<String, File> benchmarkFiles = getBenchmarks();

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final File[] testFiles = getFilesInDir("/files/pl/externals/textract/" + test.getValue());

            final Map<String, File> namedTestFiles = Stream.of(testFiles)
                    .collect(
                            toMap(
                                    file -> {
                                        final Matcher matcher = filePattern.matcher(file.getName());
                                        matcher.find();
                                        return matcher.group(1);
                                    },
                                    Function.identity()
                            )
                    );

            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            for (Map.Entry<String, File> testFile : namedTestFiles.entrySet()) {
                final var benchmark = readFile(benchmarkFiles.get(testFile.getKey()));
                final var result = readFile(testFile.getValue());
                final var comparisonResult = lineComparator.compareLines(
                        getFileName(testFile.getValue()),
                        benchmark,
                        result
                );

                comparisonResult.setDuration(0L);
                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "textract_" + test.getKey(), "csv");
            final var htmlReport = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(htmlReport, BENCHMARK_LOCATION, "textract_" + test.getKey(), "html");
        }
    }

    @Test
    @SneakyThrows
    void textractFuncTestResults() {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final Map<String, String> testsWithDirs = Map.of(
                "preprocess_fun", "preprocess/fun/",
                "raw_fun", "raw/fun"
        );

        for (Map.Entry<String, String> test : testsWithDirs.entrySet()) {
            final List<LineComparisonResult> comparisonResults = new ArrayList<>();
            final List<File> testFiles = List.of(getFilesInDir("/files/pl/externals/textract/" + test.getValue()));
            for (File testFile: testFiles) {
                final var result = readFile(testFile);

                final var comparisonResult = lineComparator.compareLines(getFileName(testFile), benchmark, result);
                comparisonResult.setDuration(0L);

                comparisonResults.add(comparisonResult);
            }
            final var report = reportGenerator.generateHtmlReport(comparisonResults);
            saveResultFile(report, BENCHMARK_LOCATION, "textract_" + test.getKey(), "html");
            final var reportCsv = reportGenerator.generateCsvReport(comparisonResults);
            saveResultFile(reportCsv, BENCHMARK_LOCATION, "textract_" + test.getKey(), "csv");
        }
    }

}
