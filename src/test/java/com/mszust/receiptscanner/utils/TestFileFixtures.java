package com.mszust.receiptscanner.utils;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class TestFileFixtures {

    public static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/paragon1/IMG_0297.jpeg",
//                "/files/pl/paragon1/IMG_0298.jpeg",
                "/files/pl/paragon1/IMG_0299.jpeg",
                "/files/pl/paragon1/IMG_0300.jpeg",
                "/files/pl/paragon1/IMG_0301.jpeg",
                "/files/pl/paragon1/IMG_0302.jpeg",
//                "/files/pl/paragon1/IMG_0303.jpeg",
                "/files/pl/paragon1/IMG_0304.jpeg",
                "/files/pl/paragon1/IMG_0305.jpeg",
                "/files/pl/paragon1/IMG_0306.jpeg",
                "/files/pl/paragon1/scan_300dpi.jpg",
                "/files/pl/paragon1/scan_600dpi.jpg"
        );
    }

    public static List<String> testFilesCropped() {
        return Arrays.asList(
//                "/files/pl/paragon1_cropped/IMG_0297-cropped-hough.jpeg",
                "/files/pl/paragon1_cropped/IMG_0298-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0299-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0300-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0301-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0302-cropped-hough.jpeg",
                "/files/pl/paragon1_cropped/IMG_0303-cropped-hough.jpeg"
//                "/files/pl/paragon1_cropped/IMG_0304-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0305-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/IMG_0306-cropped-hough.jpeg",
//                "/files/pl/paragon1_cropped/scan_300dpi-cropped-hough.jpg",
//                "/files/pl/paragon1_cropped/scan_600dpi-cropped-hough.jpg"
        );
    }

    public static List<String> testFiles2() {
        return Arrays.asList(
//                "/files/pl/paragon1/IMG_0297.jpeg"
////                "/files/pl/paragon1/IMG_0298.jpeg",
//                "/files/pl/paragon1/IMG_0299.jpeg",
//                "/files/pl/paragon1/IMG_0300.jpeg"
//                "/files/pl/paragon1/IMG_0301.jpeg"
////                "/files/pl/paragon1/IMG_0302.jpeg",
//                "/files/pl/paragon1/IMG_0303.jpeg",
//                "/files/pl/paragon1/IMG_0304.jpeg"
//                "/files/pl/paragon1/IMG_0305.jpeg"
                "/files/pl/paragon1/IMG_0306.jpeg"
//                "/files/pl/paragon1/scan_300dpi.jpg",
//                "/files/pl/paragon1/scan_600dpi.jpg"
        );
    }
}
