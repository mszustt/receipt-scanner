package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.file.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.opencv.OpenCvUtils.writeImage;
import static org.opencv.imgproc.Imgproc.INTER_AREA;

@SpringBootTest
class HistogramUtilsTest {

    @Test
    void drawHistogramGreyscale() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());

            Mat grey = new Mat();
            Imgproc.cvtColor(matFile, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat histogram = HistogramUtils.histogram(grey);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-histogram-grey." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
        }
    }

    @Test
    void drawHistogramBGR() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());

            final Mat histogram = HistogramUtils.histogram(matFile);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-histogram-rgb." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
        }
    }

    @SneakyThrows
    @Test
    void drawHistogramGreyscaleCropped() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat histogram = HistogramUtils.histogram(grey);

            final Histogram histogramArray = HistogramUtils.histogramArray(grey);
//            printMat(histogramArray);
//            HistogramUtils.findModes(histogramArray);
            final int thresh = HistogramUtils.findBackgroundThreshold(histogramArray, 10000);
            HistogramUtils.recalculate(grey, thresh);

            Mat toPrint = new Mat(histogram, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("histogram", toPrint);
            HighGui.waitKey(0);

//            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-grey." + FilenameUtils.getExtension(file.getAbsolutePath());
//            writeImage(processedFilePath, histogram);
        }
    }

    @SneakyThrows
    @Test
    void drawHistogramBGRCropped() throws URISyntaxException {
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            final Mat histogram = HistogramUtils.histogram(cropped);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-rgb." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
        }
    }

}