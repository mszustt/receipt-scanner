package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.file.FileUtils;
import com.mszust.receiptscanner.utils.opencv.utils.Boundary;
import com.mszust.receiptscanner.utils.opencv.utils.Line;
import com.mszust.receiptscanner.utils.opencv.utils.LineUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.opencv.Cropping.houghLinePContour;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.INTER_AREA;
import static org.opencv.imgproc.Imgproc.RETR_LIST;

@SpringBootTest
class FindingContoursTest2 {

    @SneakyThrows
    @Test
    void findContourAndWarp() {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());


            final MatOfPoint contour = FindingContours.findBoundingContour(matFile);

            Mat houghLIneContour = houghLinePContour(matFile, contour);

            Mat toPrint = new Mat(houghLIneContour, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("cont-" + file.getName(), toPrint);
            HighGui.waitKey(0);

        }
    }

    private Mat findContour(Mat matFile, File file) {
        Mat toPrint = new Mat();

        //find contour
        final MatOfPoint contour = FindingContours.findBoundingContour(matFile);

        final Mat contourMat = new Mat(matFile.size(), CV_8UC1);
        Imgproc.drawContours(contourMat, List.of(contour), -1, new Scalar(255), 4);
        toPrint = new Mat(contourMat, Range.all());
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("contour", toPrint);
        HighGui.waitKey(0);

        //convex hull
//            MatOfInt hullInt = new MatOfInt();
//            Imgproc.convexHull(contour, hullInt);
//            Point[] contourArray = contour.toArray();
//            Point[] hullPoints = new Point[hullInt.rows()];
//            List<Integer> hullContourIdxList = hullInt.toList();
//            for (int i = 0; i < hullContourIdxList.size(); i++) {
//                hullPoints[i] = contourArray[hullContourIdxList.get(i)];
//            }
//            final MatOfPoint hull =  new MatOfPoint(hullPoints);
//            toPrint = new Mat(matFile.size(), CV_8UC1);
//            Imgproc.drawContours(toPrint, List.of(hull), -1, new Scalar(255));
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("hull", toPrint);
//            HighGui.waitKey(0);
//
//            final Mat hullMat = new Mat(matFile.size(), CV_8UC1);
//            Imgproc.drawContours(hullMat, List.of(hull), -1, new Scalar(255));

        //find lines - unusable
//            final Mat lines = findLines(contourMat);
//            List<Line> linesList = extractLines(lines);
//            final Mat withLines = new Mat(contourMat.size(), CV_8UC3);
//            drawAllLines(withLines, linesList);
//            toPrint = new Mat(withLines, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("lines", toPrint);
//            HighGui.waitKey(0);

        //find lines with P manner
        final Mat linesP = findLinesP(preprocess(contourMat), 1);
        final Mat withLinesP = drawLines2(new Mat(contourMat.size(), CV_8UC3), linesP);
        toPrint = new Mat(withLinesP, Range.all());
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("linesP-1", toPrint);
        HighGui.waitKey(0);

        //drawLines separately
        final List<Line> linesPList = new ArrayList<>();
        for (int i = 0; i < linesP.rows(); i++) {
            double[] data = linesP.get(i, 0);
            linesPList.add(getLineP(data));
        }
//            drawLinesSeparately(new Mat(contourMat.size(), CV_8UC3), linesPList);

        //determine those 4 main lines
        List<List<Line>> partitions = LineUtils.filterRectangle(LineUtils.partition(linesPList, matFile), matFile);

//            Mat partitionsMat = new Mat(contourMat.size(), CV_8UC3);
        int i = 0;
        for (List<Line> partition : partitions) {
            Mat partitionsMat = new Mat(contourMat.size(), CV_8UC3);
            drawAllLines(partitionsMat, partition, randomColor());
            toPrint = new Mat(partitionsMat, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("partition" + i, toPrint);
            HighGui.waitKey(0);
            i++;
        }

        i = 0;
        List<Line> fittedLines = new ArrayList<>();
        for (List<Line> partition : partitions) {
            Mat partitionsMat = new Mat(contourMat.size(), CV_8UC1);
            drawAllLines(partitionsMat, partition, new Scalar(255));
            List<MatOfPoint> contours = new ArrayList<>();
            Imgproc.findContours(partitionsMat, contours, new Mat(), RETR_LIST, CHAIN_APPROX_SIMPLE);
            final List<MatOfPoint> contoursSorted = sort(contours);

            final Mat fitline = new Mat();
            Imgproc.fitLine(contoursSorted.get(0), fitline, 2, 0, 0.01, 0.01);

            final Line fitlineLine = extractFitline(partitionsMat.size(), fitline);
            fittedLines.add(fitlineLine);

//                toPrint = new Mat(partitionsMat.size(),CV_8UC3);
//                drawAllLines(toPrint, List.of(fitlineLine));
//                Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//                HighGui.imshow("fitline" + i, toPrint);
//                HighGui.waitKey(0);
            i++;
        }
        if (partitions.size() != 4) {
            partitions = findMissingLine(contourMat, fittedLines, partitions);
            fittedLines = new ArrayList<>();
            for (List<Line> partition : partitions) {
                Mat partitionsMat = new Mat(contourMat.size(), CV_8UC1);
                drawAllLines(partitionsMat, partition, new Scalar(255));
                List<MatOfPoint> contours = new ArrayList<>();
                Imgproc.findContours(partitionsMat, contours, new Mat(), RETR_LIST, CHAIN_APPROX_SIMPLE);
                final List<MatOfPoint> contoursSorted = sort(contours);

                final Mat fitline = new Mat();
                Imgproc.fitLine(contoursSorted.get(0), fitline, 2, 0, 0.01, 0.01);

                final Line fitlineLine = extractFitline(partitionsMat.size(), fitline);
                fittedLines.add(fitlineLine);
            }

        }

//            toPrint = new Mat(withLinesP, Range.all());
//            for (Point contourPoint : contourPoints.points()) {
//                Imgproc.drawMarker(toPrint, contourPoint, new Scalar(255, 255, 255), 0, 128);
//            }
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("contour points", toPrint);
//            HighGui.waitKey(0);

        toPrint = new Mat(matFile, Range.all());
        drawAllLines(toPrint, contourLines(fittedLines), new Scalar(255, 0, 255));
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("fitlines", toPrint);
        HighGui.waitKey(0);

        //find intersections


        Mat singleChannelFitlines = new Mat(matFile.size(), CV_8UC1);
        drawAllLines(singleChannelFitlines, contourLines(fittedLines), new Scalar(255));
        final List<MatOfPoint> fitlineContours = new ArrayList<>();
        Imgproc.findContours(singleChannelFitlines, fitlineContours, new Mat(), RETR_LIST, CHAIN_APPROX_SIMPLE);

        if (fitlineContours.isEmpty()) {
            //add missing line and contour it
        }

        final List<MatOfPoint> fitlineContoursSorted = sortAndFilter(matFile, fitlineContours);

        for (MatOfPoint fitlineContour : fitlineContoursSorted) {
            toPrint = Imgcodecs.imread(file.getAbsolutePath());
            Imgproc.drawContours(toPrint, List.of(fitlineContour), -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("fitlines-contours", toPrint);
            HighGui.waitKey(0);
        }
        MatOfPoint2f theNewContour2f;
        if (fitlineContoursSorted.size() > 1) {
            toPrint = Imgcodecs.imread(file.getAbsolutePath());
            Imgproc.drawContours(toPrint, List.of(fitlineContoursSorted.get(1)), -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("fitlines-contours-1", toPrint);
            HighGui.waitKey(0);


            theNewContour2f = new MatOfPoint2f(fitlineContoursSorted.get(1).toArray());
//            toPrint = Imgcodecs.imread(file.getAbsolutePath());
        } else {
            toPrint = Imgcodecs.imread(file.getAbsolutePath());
            Imgproc.drawContours(toPrint, List.of(fitlineContoursSorted.get(0)), -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("fitlines-contours-0", toPrint);
            HighGui.waitKey(0);

            theNewContour2f = new MatOfPoint2f(fitlineContoursSorted.get(0).toArray());
        }

        RotatedRect rotatedRect = Imgproc.minAreaRect(theNewContour2f);


//            Imgproc.drawContours(toPrint, List.of(fitlineContoursSorted.get(1)), -1, new Scalar(255), 4);
//            drawRotatedRect(toPrint, rotatedRect);
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("rotatedrect", toPrint);
//            HighGui.waitKey(0);

        //easy way
        final Mat cropped = Cropping.cropRotated(Imgcodecs.imread(file.getAbsolutePath()), fitlineContoursSorted.get(1));
        toPrint = new Mat(cropped, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("cropped", toPrint);
//            HighGui.waitKey(0);

        //harder way - warp contour to its bounding rect, then rotate and crop
        Boundary boundary = new Boundary(theNewContour2f.toList());

        int width = (int) rotatedRect.size.width;
        int height = (int) rotatedRect.size.height;

        if (width > height) {
            width = (int) rotatedRect.size.height; //swap
            height = (int) rotatedRect.size.width; //swap
        }

        MatOfPoint2f src = new MatOfPoint2f(boundary.getTopLeft(), boundary.getTopRight(), boundary.getBottomRight(), boundary.getBottomLeft());

        toPrint = Imgcodecs.imread(file.getAbsolutePath());
        Imgproc.line(toPrint, boundary.getTopLeft(), boundary.getTopRight(), new Scalar(255, 0, 255), 4);
        Imgproc.line(toPrint, boundary.getTopRight(), boundary.getBottomRight(), new Scalar(255, 0, 255), 4);
        Imgproc.line(toPrint, boundary.getTopLeft(), boundary.getBottomLeft(), new Scalar(255, 0, 255), 4);
        Imgproc.line(toPrint, boundary.getBottomLeft(), boundary.getBottomRight(), new Scalar(255, 0, 255), 4);
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("boundary", toPrint);
        HighGui.waitKey(0);

        MatOfPoint2f dst = new MatOfPoint2f(
                new Point(0, 0),
                new Point(width - 1, 0),
                new Point(width - 1, height - 1),
                new Point(0, height - 1)
        );

        final Mat transformed = Imgproc.getPerspectiveTransform(src, dst);


        final Mat dstMat = Imgcodecs.imread(file.getAbsolutePath());
        Imgproc.warpPerspective(dstMat, dstMat, transformed, new Size(width, height));
        toPrint = new Mat(dstMat, Range.all());
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("warpperspective-" + file.getName(), toPrint);
        HighGui.waitKey(0);

        return dstMat;
    }

    private List<List<Line>> findMissingLine(Mat mat, List<Line> fitlines, List<List<Line>> partitions) {
        List<List<Line>> improved = new ArrayList<>(partitions);

        final List<Point> points = new ArrayList<>();
        partitions.forEach(lines ->
                lines.forEach(line ->
                        points.addAll(line.points())
                )
        );

        final Boundary boundary = new Boundary(points);

        final Line top = new Line(boundary.getTopLeft(), boundary.getTopRight());
        final Line bot = new Line(boundary.getBottomLeft(), boundary.getBottomRight());
        final Line left = new Line(boundary.getBottomLeft(), boundary.getTopLeft());
        final Line right = new Line(boundary.getBottomRight(), boundary.getTopRight());

        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(top, 10, 100))) {
            improved.add(List.of(top));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(bot, 10, 100))) {
            improved.add(List.of(bot));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(left, 10, 100))) {
            improved.add(List.of(left));
        }
        if (fitlines.stream().noneMatch(line -> line.isSimilarTo(right, 10, 100))) {
            improved.add(List.of(right));
        }

        Mat toPrint = new Mat(mat.size(), CV_8UC3);
        for (Line line : fitlines) {
            Imgproc.line(toPrint, line.getStart(), line.getEnd(), new Scalar(255, 0, 0), 4);
        }
        for (Line line : List.of(top, bot, left, right)) {
            Imgproc.line(toPrint, line.getStart(), line.getEnd(), new Scalar(0, 0, 255), 4);
        }
        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
        HighGui.imshow("line-comparison", toPrint);
        HighGui.waitKey(0);

        return improved;
    }

    private Mat preprocess(Mat contourMat) {
        final Mat preprocessed = new Mat(contourMat, Range.all());
        Imgproc.GaussianBlur(preprocessed, preprocessed, new Size(3, 3), 0);
        return preprocessed;
    }

    private List<Line> contourLines(List<Line> fittedLines) {
//        return fittedLines.subList(0, 4);
        return fittedLines;
    }

    private Line extractFitline(Size srcSize, Mat fitline) {
        double lefty = ((-1) * fitline.get(2, 0)[0] * fitline.get(1, 0)[0] / fitline.get(0, 0)[0]) + fitline.get(3, 0)[0];
//        int righty = ((gray.cols-lines[2])*lines[1]/lines[0])+lines[3];
        double righty = ((srcSize.width - fitline.get(2, 0)[0]) * fitline.get(1, 0)[0] / fitline.get(0, 0)[0]) + fitline.get(3, 0)[0];
        return new Line(
                new Point(srcSize.width - 1, righty),
                new Point(0, lefty)
        );
    }

    private Scalar randomColor() {
        return new Scalar(
                RandomUtils.nextInt(0, 256),
                RandomUtils.nextInt(0, 256),
                RandomUtils.nextInt(0, 256)
        );
    }

    private List<Line> extractLines(Mat lines) {
        final List<Line> linesList = new ArrayList<>();
        for (int i = 0; i < lines.rows(); i++) {
            linesList.add(getLine(lines.get(i, 0)));
        }
        return linesList;
    }

    private Line getLine(double[] line) {
        double rho = line[0];
        double theta = line[1];
        double a = Math.cos(theta);
        double b = Math.sin(theta);
        double x0 = a * rho;
        double y0 = b * rho;
        //Drawing lines on the image
        Point pt1 = new Point();
        Point pt2 = new Point();
        pt1.x = Math.round(x0 + 1000 * (-b));
        pt1.y = Math.round(y0 + 1000 * (a));
        pt2.x = Math.round(x0 - 1000 * (-b));
        pt2.y = Math.round(y0 - 1000 * (a));

        return new Line(pt1, pt2);
    }

    private Line getLineP(double[] line) {
        Point pt1 = new Point(line[0], line[1]);
        Point pt2 = new Point(line[2], line[3]);
        return new Line(pt1, pt2);
    }

    private Mat findLinesP(Mat src, int maxLineGap) {
        Mat linesP = new Mat();
        Imgproc.HoughLinesP(src, linesP, 1, Math.PI / 180, 100, 100, maxLineGap);
        return linesP;
    }

    private Mat findLines(Mat src) {
        Mat lines = new Mat();
        Imgproc.HoughLines(src, lines, 1, Math.PI / 180, 100, 0, 0);
        return lines;
    }

    private Mat drawLines(Mat src, Mat lines) {
        final Mat withLines = new Mat(src, Range.all());
        for (int x = 0; x < lines.cols(); x++) {
            double[] vec = lines.get(0, x);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(withLines, start, end, new Scalar(255, 0, 255), 4);
        }
        return withLines;
    }

    private Mat drawLinesSeparately(Mat src, List<Line> lines) {
        final Mat mat = new Mat(src, Range.all());
        for (Line line : lines) {
            Imgproc.line(src, line.getStart(), line.getEnd(), new Scalar(0, 0, 255), 4);
            Mat toPrint = new Mat(src, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("lines", toPrint);
            HighGui.waitKey(0);
        }
        return mat;
    }

    private void drawAllLines(Mat src, List<Line> lines, Scalar color) {
        for (Line line : lines) {
            Imgproc.line(src, line.getStart(), line.getEnd(), color, 4);
        }
    }

    private void drawAllLines(Mat src, List<Line> lines) {
        drawAllLines(src, lines, new Scalar(255, 0, 255));
    }

    private Mat drawLines2(Mat src, Mat lines) {
        final Mat withLines = new Mat(src, Range.all());
        for (int y = 0; y < lines.rows(); y++) {
            double[] vec = lines.get(y, 0);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(withLines, start, end, new Scalar(255, 0, 255), 4);
        }
        return withLines;
    }

    private static List<MatOfPoint> sortAndFilter(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        final var list = contours.stream()
                .filter(contour -> 0.3 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    private static List<MatOfPoint> sort(List<MatOfPoint> contours) {
        final var list = contours.stream()
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    private void drawRotatedRect(Mat src, RotatedRect rotatedRect) {
        Point[] points = new Point[4];
        rotatedRect.points(points);

        for (int i = 0; i < 4; i++)
            Imgproc.line(src, points[i], points[(i + 1) % 4], new Scalar(255, 0, 255), 4);
    }

//    @Nullable
//    public static MatOfPoint firstRoundContour(Mat sourceImage) {
//        Mat binaryMat = new Mat();
//        if (sourceImage.channels() > 1) {
//            OpenCvBinarization.otsuBinarization(sourceImage, binaryMat);
//        } else {
//            binaryMat = sourceImage;
//        }
//
//        final List<MatOfPoint> contours = findContours(binaryMat);
//
//        final MatOfPoint primalContour = findBoundingContour(sourceImage, contours);
//
//        if (primalContour == null) {
//            return null;
//        }
//
//        return approxWithPolylines(primalContour);
//    }
}


