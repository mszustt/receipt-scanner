package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.file.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URISyntaxException;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static org.opencv.imgproc.Imgproc.INTER_AREA;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class OpenCvUtilsTest {

    @Test
    void testDetectText() throws URISyntaxException {

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            Mat processedMat = new Mat();

            final List<Rect> rects = OpenCvUtils.detectText(matFile);

            for (int i = 0; i < rects.size(); i++) {
                Imgproc.rectangle(matFile, rects.get(i).br(), rects.get(i).tl(), new Scalar(0, 255, 0), 3, 8, 0);
            }
            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-text-detect-xy-ks3-16s." + FilenameUtils.getExtension(file.getAbsolutePath());
            Imgcodecs.imwrite(processedFilePath, matFile);
        }
    }

//    @Test
//    void testDetectBoundaries() throws URISyntaxException {
//        System.setProperty("java.awt.headless", "false");
//        for (String fileLocation : testFiles()) {
//            final var file = FileUtils.file(fileLocation);
//
//
//            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
//            Mat processedMat = new Mat();
//
//            final List<Point> contour = Cropping.findBoudingRectUsingCanny(matFile);
//            Imgproc.polylines(matFile, List.of(new MatOfPoint(contour.toArray(new Point[0]))), true, new Scalar(255), 4);
//            ;
//
//            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-contours-canny." + FilenameUtils.getExtension(file.getAbsolutePath());
//            Imgcodecs.imwrite(processedFilePath, matFile);
//        }
//    }

//    @Test
//    void testDetectBoundariesOnBinarizedImage() throws URISyntaxException {
//        System.setProperty("java.awt.headless", "false");
//        final var file = FileUtils.file("/files/pl/paragon1/IMG_0297.jpeg");
//
//
//        Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
//        Mat processedMat = new Mat();
//
//        OpenCvBinarization.otsuBinarization(matFile, processedMat);
//
//        final List<MatOfPoint> contours = Cropping.findContours(processedMat);
//        final MatOfPoint biggestContour = Cropping.findBiggestContour(contours);
////            Imgproc.polylines(matFile, List.of(new MatOfPoint(contour.toArray(new Point[0]))), true, new Scalar(255), 4);
////            Imgproc.polylines(matFile, contours, true, new Scalar(255), 4);
//        Mat toPrint = new Mat();
//        Imgproc.drawContours(processedMat, List.of(biggestContour), -1, new Scalar(0), -1);
////        Imgproc.drawContours(processedMat, contours, -1, new Scalar(255), -1);
//        Imgproc.resize(processedMat, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//        HighGui.imshow("src", toPrint);
//        HighGui.waitKey(0);
//
//        final MatOfPoint significantContours = Cropping.chooseBoundingContour(matFile, contours);
//        toPrint = new Mat(matFile.size(), CV_8UC1);
////        Imgproc.drawContours(toPrint, contours, -1, new Scalar(255), 4);
//        Imgproc.drawContours(toPrint, List.of(significantContours), -1, new Scalar(255), 4);
//        Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//        HighGui.imshow("src", toPrint);
//        HighGui.waitKey(0);
//
//        final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-otsu." + FilenameUtils.getExtension(file.getAbsolutePath());
//        Imgcodecs.imwrite(processedFilePath, matFile);
//    }

//    @Test
//    void test() throws URISyntaxException {
//        System.setProperty("java.awt.headless", "false");
//        final var file = FileUtils.file("/files/pl/paragon1/IMG_0297.jpeg");
//
//
//        Mat matFile = Mat.zeros(new Size(100, 100), CV_8UC1);
//        Imgproc.circle(matFile, new Point(50, 50), 15, new Scalar(255), 1);
//        Mat processedMat = matFile;
//
////        OpenCvBinarization.otsuBinarization(matFile, processedMat);
//
//        final List<MatOfPoint> contours = Cropping.findContours(processedMat);
//        final MatOfPoint biggestContour = Cropping.findBiggestContour(contours);
////            Imgproc.polylines(matFile, List.of(new MatOfPoint(contour.toArray(new Point[0]))), true, new Scalar(255), 4);
////            Imgproc.polylines(matFile, contours, true, new Scalar(255), 4);
//        Mat toPrint = new Mat();
////        Imgproc.resize(processedMat, toPrint, new Size(), 2.0d, 2.0d, INTER_LINEAR);
//        Imgproc.drawContours(processedMat, List.of(biggestContour), -1, new Scalar(1000), 4);
//        HighGui.imshow("src", processedMat);
//        HighGui.waitKey(0);
//
////            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-contours-canny." + FilenameUtils.getExtension(file.getAbsolutePath());
////            Imgcodecs.imwrite(processedFilePath, matFile);
//    }

    @SneakyThrows
    @Test
    void cropImages() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            Mat processedMat = Cropping.cropImageExact(matFile);

//            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-mask-black." + FilenameUtils.getExtension(file.getAbsolutePath());
//            Imgcodecs.imwrite(processedFilePath, processedMat);

            Mat toPrint = new Mat(processedMat, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("rotated", toPrint);
            HighGui.waitKey(0);
        }
    }
}
