package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.file.FileUtils;
import com.mszust.receiptscanner.utils.opencv.utils.Pair;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles2;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgproc.Imgproc.INTER_AREA;

@SpringBootTest
class FindingContoursTest {

    @Test
    void findContours() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        for (String fileLocation : testFiles2()) {
            final var file = FileUtils.file(fileLocation);


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            Mat toPrint = matFile.clone();

            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("source", toPrint);
            HighGui.waitKey(0);

            Mat binaryMat = new Mat();
            OpenCvBinarization.otsuBinarization(matFile, binaryMat);

            toPrint = binaryMat.clone();
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("binary", toPrint);
            HighGui.waitKey(0);

            List<MatOfPoint> contours = FindingContours.findContours(binaryMat);


            toPrint = new Mat(matFile.size(), CV_8UC1);
//        Imgproc.drawContours(toPrint, contours, -1, new Scalar(255), 4);
            Imgproc.drawContours(toPrint, contours, -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("all-contours", toPrint);
            HighGui.waitKey(0);

            final var theContour = findBoundingContour(matFile, contours);

//        Imgproc.drawContours(toPrint, contours, -1, new Scalar(255), 4);
            Mat withContour = new Mat(matFile.size(), CV_8UC1);
            Imgproc.drawContours(withContour, List.of(theContour), -1, new Scalar(255), 4);
            toPrint = new Mat(withContour, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("right-contour", toPrint);
            HighGui.waitKey(0);

//            MatOfPoint2f approxCurve = new MatOfPoint2f();
//            MatOfPoint2f contour2f = new MatOfPoint2f(theContour.toArray());
//            //Processing on mMOP2f1 which is in type MatOfPoint2f
//            double approxDistance = Imgproc.arcLength(contour2f, true) * 0.014;
//            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
//            //Convert back to MatOfPoint
//            MatOfPoint points = new MatOfPoint(approxCurve.toArray());
//
//            toPrint = new Mat(matFile.size(), CV_8UC1);
//            Imgproc.polylines(toPrint, List.of(points), true, new Scalar(255), 4);
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("approx", toPrint);
//            HighGui.waitKey(0);

            //Identify lines
            toPrint = new Mat(matFile.size(), CV_8UC1);
//            Imgproc.polylines(toPrint, List.of(points), true, new Scalar(255), 4);
            final Mat lines = new Mat();
//            Imgproc.HoughLinesP(toPrint, lines, 1, Math.PI/360, 100, 100, 1);
            Imgproc.HoughLines(withContour, lines, 1, Math.PI / 180, 100, 10, 1000);
//            toPrint = new Mat(matFile.size(), CV_8UC1);
            Mat withLines = new Mat(matFile.size(), CV_8UC1);
            for (int i = 0; i < lines.rows(); i++) {
                double[] val = lines.get(i, 0);
                Imgproc.line(withLines, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(255), 4);
            }
            toPrint = new Mat(withLines, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("lines", toPrint);
            HighGui.waitKey(0);

            //Identify lines probabilistic
            toPrint = new Mat(matFile.size(), CV_8UC1);
//            Imgproc.polylines(toPrint, List.of(points), true, new Scalar(255), 4);
            final Mat linesP = new Mat();
//            Imgproc.HoughLinesP(toPrint, lines, 1, Math.PI/360, 100, 100, 1);
            Imgproc.HoughLinesP(withContour, linesP, 1, Math.PI / 180, 100, 10, 1000);
//            toPrint = new Mat(matFile.size(), CV_8UC1);
            Mat withLinesP = new Mat(matFile.size(), CV_8UC1);
            for (int i = 0; i < linesP.rows(); i++) {
                double[] val = linesP.get(i, 0);
                Imgproc.line(withLinesP, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(255), 4);
            }
            toPrint = new Mat(withLinesP, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("linesP", toPrint);
            HighGui.waitKey(0);

            System.exit(0);

            //Contour again
            List<MatOfPoint> newContours = FindingContours.findContours(withLines);
            toPrint = new Mat(matFile.size(), CV_8UC1);
            Imgproc.drawContours(toPrint, newContours, -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("all-new-contours", toPrint);
            HighGui.waitKey(0);

            final List<MatOfPoint> newContoursSorted = sort(withLines, newContours);
            MatOfPoint theNewContour = new MatOfPoint();
            if (newContoursSorted.size() > 1) {
                if (Imgproc.matchShapes(newContoursSorted.get(1), theContour, 1, 0d) >
                        Imgproc.matchShapes(newContoursSorted.get(0), theContour, 1, 0d)) {
                    theNewContour = newContoursSorted.get(1);
                } else {
                    theNewContour = newContoursSorted.get(0);
                }
            } else {
                theNewContour = newContoursSorted.get(0);
            }

            toPrint = new Mat(matFile.size(), CV_8UC1);
//        Imgproc.drawContours(toPrint, contours, -1, new Scalar(255), 4);
            Imgproc.drawContours(toPrint, List.of(theNewContour), -1, new Scalar(255), 4);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("the-new-right-contour", toPrint);
            HighGui.waitKey(0);

            //probabilistic hough lines
//            Imgproc.HoughLinesP(toPrint, lines, 1, Math.PI/360, 10, 100, 1);
            // Get bounding rect of contour
            Rect rect = Imgproc.boundingRect(theNewContour);
            toPrint = new Mat(matFile, Range.all());
            Imgproc.rectangle(toPrint, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 3);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("bounding-rect-on-approx", toPrint);
            HighGui.waitKey(0);

//            //min-size (rotated) rect
//            MatOfPoint2f theNewContour2f = new MatOfPoint2f(theNewContour.toArray());
//            RotatedRect rotatedRect = Imgproc.minAreaRect(theNewContour2f);
//            Mat rotatedRectPoints = new Mat();
//            Imgproc.boxPoints(rotatedRect, rotatedRectPoints);
//            Point[] vertices = new Point[4];
//            rotatedRect.points(vertices);
//            List<Point> srcOrdered = orderPoints(Arrays.asList(vertices));
//            int width = (int) rotatedRect.size.height; //swap
//            int height = (int) rotatedRect.size.width; //swap
////            int width = (int) rotatedRect.size.width;
////            int height = (int) rotatedRect.size.height;
//
//            MatOfPoint2f src = new MatOfPoint2f(srcOrdered.get(0), srcOrdered.get(1), srcOrdered.get(2), srcOrdered.get(3));
//
//            MatOfPoint2f dst = new MatOfPoint2f(
//                    new Point(0, 0),
//                    new Point(width - 1, 0),
//                    new Point(width - 1, height - 1),
//                    new Point(0, height - 1)
//            );
//
//            final Mat transformed = Imgproc.getPerspectiveTransform(src, dst);
//
//            final Mat dstMat = new Mat(matFile, Range.all());
//            Imgproc.warpPerspective(matFile, dstMat, transformed, new Size(width, height));
//            toPrint = new Mat(dstMat, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("rotated-rect-on-approx", toPrint);
//            HighGui.waitKey(0);

            Rect roi = Imgproc.boundingRect(theNewContour);
            Mat mask = Mat.zeros(matFile.size(), matFile.type());
            toPrint = new Mat(mask, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("mask", toPrint);
            HighGui.waitKey(0);
            Imgproc.drawContours(mask, List.of(theNewContour), -1, Scalar.all(255), -1);
            toPrint = new Mat(mask, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("mask-filled", toPrint);
            HighGui.waitKey(0);
            Mat masked = new Mat(roi.size(), matFile.type(), new Scalar(255, 0, 255));
            matFile.copyTo(masked, mask);
            toPrint = new Mat(masked, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("mask-copied", toPrint);
            HighGui.waitKey(0);

            Mat backgroundMask = new Mat();
            Core.inRange(masked, new Scalar(0), new Scalar(1), backgroundMask);
            masked.setTo(new Scalar(255, 255, 255), backgroundMask);
            toPrint = new Mat(masked, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("mask-background", toPrint);
            HighGui.waitKey(0);

            toPrint = new Mat(masked, roi);
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("mask-cropped", toPrint);
            HighGui.waitKey(0);

        }
    }

    private static MatOfPoint findBoundingContour(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        return contours.stream()
                .filter(contour -> 0.3 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .max(Comparator.comparingDouble(Imgproc::contourArea))
                .orElse(null);
    }

    private static List<MatOfPoint> sort(Mat srcImage, List<MatOfPoint> contours) {
        final double sourceArea = srcImage.size().area();
        final var list = contours.stream()
                .filter(contour -> 0.3 * sourceArea < Imgproc.contourArea(contour) && Imgproc.contourArea(contour) < 0.95 * sourceArea)
                .sorted(Comparator.comparingDouble(Imgproc::contourArea))
                .collect(Collectors.toList());
        Collections.reverse(list);
        return list;
    }

    private static List<Point> orderPoints(List<Point> contour) {
        List<Pair> sum = new ArrayList<>(contour.size());
        List<Pair> diff = new ArrayList<>(contour.size());

        for (int i = 0; i < contour.size(); ++i) {
            sum.add(i, new Pair(contour.get(i).x + contour.get(i).y, i));
            diff.add(i, new Pair(contour.get(i).x - contour.get(i).y, i));
        }

        var minSum = sum.stream().min(Comparator.comparingDouble(Pair::getX)).get();
        var maxSum = sum.stream().max(Comparator.comparingDouble(Pair::getX)).get();
        var minDiff = diff.stream().min(Comparator.comparingDouble(Pair::getX)).get();
        var maxDiff = diff.stream().max(Comparator.comparingDouble(Pair::getX)).get();

//        Point topLeft = contour.get(minSum.getY());
//        Point topRight = contour.get(maxDiff.getY());
//        Point bottomRight = contour.get(maxSum.getY());
//        Point bottomLeft = contour.get(minDiff.getY());

        Point topLeft = contour.get(minSum.getY());
        Point topRight = contour.get(maxDiff.getY());
        Point bottomRight = contour.get(maxSum.getY());
        Point bottomLeft = contour.get(minDiff.getY());
        return List.of(topLeft, topRight, bottomRight, bottomLeft);
    }


    @SneakyThrows
    @Test
    void findContourAndWarp() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");
        Mat toPrint = new Mat();
        for (String fileLocation : testFiles2()) {
            final var file = FileUtils.file(fileLocation);


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            toPrint = new Mat(cropped, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("source-cropped", toPrint);
            HighGui.waitKey(0);

            final Mat binarized = OpenCvBinarization.otsuInverted(cropped);

//            final Mat lines = findLines(binarized);
//            final Mat withLines = drawLines(cropped, lines);
//            toPrint = new Mat(withLines, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("lines", toPrint);
//            HighGui.waitKey(0);


            final Mat linesP = findLinesP(binarized);
            final Mat withLinesP = drawLines2(cropped, linesP);
            toPrint = new Mat(withLinesP, Range.all());
            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
            HighGui.imshow("linesP", toPrint);
            HighGui.waitKey(0);
        }
    }

    private Mat findLinesP(Mat src) {
        Mat linesP = new Mat();
        Imgproc.HoughLinesP(src, linesP, 1, Math.PI / 180, 100, 100, 1000);
        return linesP;
    }

    private Mat findLines(Mat src) {
        Mat lines = new Mat();
        Imgproc.HoughLines(src, lines, 1, Math.PI / 180, 100, 10, 1000);
        return lines;
    }

    private Mat drawLines(Mat src, Mat lines) {
        final Mat withLines = new Mat(src, Range.all());
        for (int x = 0; x < lines.cols(); x++) {
            double[] vec = lines.get(0, x);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(withLines, start, end, new Scalar(255, 0, 255), 4);
        }
        return withLines;
    }

    private Mat drawLines2(Mat src, Mat lines) {
        final Mat withLines = new Mat(src, Range.all());
        for (int y = 0; y < lines.rows(); y++) {
            double[] vec = lines.get(y, 0);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(withLines, start, end, new Scalar(255, 0, 255), 4);
        }
        return withLines;
    }
}