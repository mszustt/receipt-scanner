package com.mszust.receiptscanner.utils.opencv;

import com.mszust.receiptscanner.utils.file.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static org.opencv.imgproc.Imgproc.COLOR_GRAY2BGR;

@SpringBootTest
class LineHeightTest {

    @SneakyThrows
    @Test
    void findLineHeight() throws URISyntaxException {
        System.setProperty("java.awt.headless", "false");


        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);


            Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            Mat processedMat = Cropping.cropImageExact(matFile);


            //find contours
            Mat threshold = OpenCvBinarization.otsuInverted(processedMat);
            Mat nonZeros = new Mat();
            Core.findNonZero(threshold, nonZeros);
            MatOfPoint nonZeroPts = new MatOfPoint(nonZeros);
            MatOfPoint2f nonZeros2f = new MatOfPoint2f(nonZeroPts.toArray());
            RotatedRect ret = Imgproc.minAreaRect(nonZeros2f);


            var w = ret.size.width;
            var h = ret.size.height;
            var ang = ret.angle;

            if (w > h) {
                w = ret.size.height;
                h = ret.size.width;
                ang = ret.angle + 90d;
            }

            // find rotation matrix and rotate
            Mat rotationMatrix = Imgproc.getRotationMatrix2D(ret.center, ang, 1.0);
            Mat rotated = new Mat();
            Imgproc.warpAffine(threshold, rotated, rotationMatrix, processedMat.size(), Imgproc.INTER_LINEAR);

//            Mat toPrint = new Mat(rotated, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("rotated", toPrint);
//            HighGui.waitKey(0);

            //write line boundaries
            Mat hist = new Mat();
            Core.reduce(rotated, hist, 1, Core.REDUCE_AVG);
            hist.reshape(-1);


            var th = 2;
            var originalHeight = processedMat.size().height;
            var originalWidth = processedMat.size().width;

            byte[][] arr = new byte[hist.cols()][hist.rows()];

            byte[] tmp = new byte[1];
            for (int i = 0; i < hist.rows(); ++i) {
                for (int j = 0; j < hist.cols(); ++j) {
                    hist.get(i, j, tmp);
                    arr[j][i] = tmp[0];
                }
            }

            final List<Integer> uppers = new ArrayList<>();
            final List<Integer> lowers = new ArrayList<>();

            //find first 2 consecutive zeros to reduce noise, as its not what we're looking for anyway
            int j = 0;

            for (int i = 0; i < originalHeight - 1; i++) {
                if (arr[0][i] == 0 && arr[0][i+1] == 0) {
                    j = i;
                    break;
                }
            }

            for (int i = j; i < originalHeight - 1; i++) {
                if (arr[0][i] <= th && arr[0][i + 1] > th) {
                    uppers.add(i);
                }
                if (arr[0][i] > th && arr[0][i + 1] <= th) {
                    lowers.add(i);
                }
            }

            Imgproc.cvtColor(rotated, rotated, COLOR_GRAY2BGR);

            for (Integer y : uppers) {
                Imgproc.line(rotated, new Point(0, y), new Point(originalWidth, y), new Scalar(255, 0, 0), 4);
            }

            for (Integer y : lowers) {
                Imgproc.line(rotated, new Point(0, y), new Point(originalWidth, y), new Scalar(0, 255, 0), 4);
            }

//            toPrint = new Mat(rotated, Range.all());
//            Imgproc.resize(toPrint, toPrint, new Size(), 0.20d, 0.20d, INTER_AREA);
//            HighGui.imshow("reduced", toPrint);
//            HighGui.waitKey(0);

            List<Double> heights = new ArrayList<>();

            for (int i = 0; i < Math.min(uppers.size(), lowers.size()); i++) {
                heights.add((double) lowers.get(i) - uppers.get(i));
            }

            final Double[] heightsFiltered = heights.stream()
                    .filter(height -> height >= 15 && height <= 100)
                    .toArray(Double[]::new);

            final double[] modes = StatUtils.mode(ArrayUtils.toPrimitive(heightsFiltered));

            System.out.println("File: " + fileLocation + ", Mode:" + modes[0]);
        }
    }
}
