package com.mszust.receiptscanner.utils;

import com.github.difflib.algorithm.DiffException;
import lombok.experimental.UtilityClass;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.mszust.receiptscanner.utils.file.FileUtils.file;

@UtilityClass
public class TestFileUtils {

    public static void saveFile(final String path, final String content) throws IOException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        Files.write(Paths.get(path), content.getBytes());
    }

    public static void saveResultFile(String htmlReport, String directory, String filename) throws DiffException, IOException, URISyntaxException {
        saveFile(file(directory).getParentFile().getAbsolutePath() + "/result/" + filename + ".html", htmlReport);
    }

    public static void saveResultFile(String report, String benchmarkFile, String filename, String extension) throws DiffException, IOException, URISyntaxException {
        saveFile(file(benchmarkFile).getParentFile().getAbsolutePath() + "/result/" + filename + "." + extension, report);
    }
}
