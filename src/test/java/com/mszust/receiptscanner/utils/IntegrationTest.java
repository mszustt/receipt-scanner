package com.mszust.receiptscanner.utils;

import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.report.ReportGenerator;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class IntegrationTest {
    protected static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    protected TesseractOcr scannerService;
    @Autowired
    protected LineComparator lineComparator;
    @Autowired
    protected ReportGenerator reportGenerator;

}
