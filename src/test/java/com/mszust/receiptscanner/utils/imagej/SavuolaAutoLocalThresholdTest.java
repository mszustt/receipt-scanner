package com.mszust.receiptscanner.utils.imagej;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

public class SavuolaAutoLocalThresholdTest extends IntegrationTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";


    @Test
    void testSavolaAutoLocalThresholdMethods() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            for (Double k : ks()) {
                for (Double r : rs()) {

                    final String fileName = getFileName(file) + "-imagej-sauvola-k-" + k + "-r-" + r;
                    final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                    final BufferedImage processed = ImageJUtils.sauvolaLocalThreshold(FileUtils.readImage(fileLocation), k, r);

//                OpenCvUtils.writeImage(
//                        processedFilePath,
//                        processed
//                );

                    final var result = scannerService.processFile(new Scannable(processed, fileName));

                    final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                    comparisonResult.setDuration(result.getOcrDuration());
                    comparisonResults.add(comparisonResult);
                }
            }
            final var baseResult = scannerService.processFile(new Scannable(file));
            final var baseComparisonResult = lineComparator.compareLines(getFileName(file), benchmark, baseResult.getRecognizedLines());
            baseComparisonResult.setDuration(baseResult.getOcrDuration());
            comparisonResults.add(baseComparisonResult);
        }


        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "imagej-auto-local-sauvola-all");
    }

    private List<Double> ks() {
        return List.of(
                0.5,
                0.75,
                1.0
        );
    }

    private List<Double> rs() {
        return List.of(
                64.0,
                128.0
        );
    }

    private static List<String> testFile() {
        return List.of(
                "/files/pl/paragon1/IMG_0297.jpeg"
        );
    }
}
