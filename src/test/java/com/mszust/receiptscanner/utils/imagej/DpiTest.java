package com.mszust.receiptscanner.utils.imagej;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

public class DpiTest extends IntegrationTest {

    @Test
    void testDpiRescaling() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            for (Integer dpi : dpis()) {
                final String fileName = getFileName(file) + "-imagej-dpi-" + dpi;
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.rescale(FileUtils.readImage(fileLocation), dpi);
                scannerService.setDpi(dpi);
//                OpenCvUtils.writeImage(
//                        processedFilePath,
//                        processed
//                );

                final var result = scannerService.processFile(new Scannable(processed, fileName));
//
                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                comparisonResult.setDuration(result.getOcrDuration());

                comparisonResults.add(comparisonResult);
            }
            scannerService.setDpi(300);
            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "imagej-dpi-all-recalibrated");
    }

    private List<Integer> dpis() {
        return List.of(
//                70,
//                150,
                300,
                600
        );
    }

    private static List<String> testFile() {
        return List.of(
                "/files/pl/paragon1/IMG_0297.jpeg"
        );
    }
}
