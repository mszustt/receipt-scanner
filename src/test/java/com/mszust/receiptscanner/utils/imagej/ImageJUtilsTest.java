package com.mszust.receiptscanner.utils.imagej;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import com.mszust.receiptscanner.utils.opencv.OpenCvUtils;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFilesCropped;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

class ImageJUtilsTest extends IntegrationTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Test
    void testDifferentAutoThresholdMethods() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFilesCropped()) {
            final var file = file(fileLocation);

            for (AutoThreshold.Method method : AutoThreshold.Method.values()) {
                final String fileName = getFileName(file) + "-cropped-imagej-autothreshold-binary" + method.getMethod();
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.threshold(FileUtils.readImage(fileLocation), method);

                OpenCvUtils.writeImage(
                        processedFilePath,
                        processed
                );

//                final var result = scannerService.processFile(new Scannable(processed, fileName));
//
//                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
//                comparisonResult.setDuration(result.getOcrDuration());
//
//                comparisonResults.add(comparisonResult);
            }
//            final var result = scannerService.processFile(new Scannable(file));
//            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
//            comparisonResult.setDuration(result.getOcrDuration());
//
//            comparisonResults.add(comparisonResult);
//
        }
//        final var report = reportGenerator.generateCsvReport(comparisonResults);
//        saveResultFile(report, BENCHMARK_LOCATION, "cropped-imagej-auto-threshold-binary");
    }


    @Test
    void testDifferentAutoLocalThresholdMethods() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFile()) {
            final var file = file(fileLocation);

            for (AutoLocalThreshold.Method method : AutoLocalThreshold.Method.values()) {
                final String fileName = getFileName(file) + "-imagej-autolocalthreshold-" + method.getMethod();
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.localThreshold(FileUtils.readImage(fileLocation), method);

//                OpenCvUtils.writeImage(
//                        processedFilePath,
//                        processed
//                );

                final var result = scannerService.processFile(new Scannable(processed, fileName));

                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                comparisonResult.setDuration(result.getOcrDuration());

                comparisonResults.add(comparisonResult);
            }
            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "imagej-auto-local-threshold2");
    }

    private static List<String> testFile() {
        return List.of(
                "/files/pl/paragon1/IMG_0297.jpeg"
        );
    }

}