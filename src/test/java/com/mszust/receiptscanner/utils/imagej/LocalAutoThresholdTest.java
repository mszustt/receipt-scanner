package com.mszust.receiptscanner.utils.imagej;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import com.mszust.receiptscanner.utils.opencv.OpenCvUtils;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.TestFileFixtures.testFilesCropped;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

class LocalAutoThresholdTest extends IntegrationTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Test
    void testDifferentLocalAutoThresholdMethodsOnCropped() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFilesCropped()) {
            final var file = file(fileLocation);

            for (AutoLocalThreshold.Method method : AutoLocalThreshold.Method.values()) {
                final String fileName = getFileName(file) + "-cropped-imagej-auto-localthreshold-binary" + method.getMethod();
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.localThreshold(FileUtils.readImage(fileLocation), method);

                OpenCvUtils.writeImage(
                        processedFilePath,
                        processed
                );

                final var result = scannerService.processFile(new Scannable(processed, fileName));

                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                comparisonResult.setDuration(result.getOcrDuration());

                comparisonResults.add(comparisonResult);
            }
            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-imagej-auto-local-threshold-binary");
    }

    @Test
    void testDifferentAutoLocalThresholdMethods() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFile()) {
            final var file = file(fileLocation);

            for (AutoLocalThreshold.Method method : AutoLocalThreshold.Method.values()) {
                final String fileName = getFileName(file) + "-imagej-auto-localthreshold-r15-binary" + method.getMethod();
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.localThreshold(FileUtils.readImage(fileLocation), method);

                OpenCvUtils.writeImage(
                        processedFilePath,
                        processed
                );

                final var result = scannerService.processFile(new Scannable(processed, fileName));

                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                comparisonResult.setDuration(result.getOcrDuration());

                comparisonResults.add(comparisonResult);
            }
            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "imagej-auto-local-threshold-r15-single");
    }

    private static List<String> testFile() {
        return List.of(
                "/files/pl/paragon1/IMG_0297.jpeg"
        );
    }

    @Test
    void testSingleAutoLocalThresholdWithDifferentParams() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final AutoLocalThreshold.Method method = AutoLocalThreshold.Method.SAUVOLA;
            for (AutoLocalThreshold.Config config : sauvolaConfig2()) {
                final String fileName = getFileName(file) + "-imagej-auto-localthreshold-binary-" + method.getMethod()
                        + "-rad-" + config.getRadius()
                        + "-par1-" + config.getPar1()
                        + "-par2-" + config.getPar2();
                final String processedFilePath = file.getParent() + "/" + fileName + ".png";

                final BufferedImage processed = ImageJUtils.localThreshold(FileUtils.readImage(fileLocation), method, config);

//                OpenCvUtils.writeImage(
//                        processedFilePath,
//                        processed
//                );

                final var result = scannerService.processFile(new Scannable(processed, fileName));

                final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
                comparisonResult.setDuration(result.getOcrDuration());

                comparisonResults.add(comparisonResult);
            }
            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "imagej-auto-local-threshold-sauvola2", "csv");
    }

    private Iterable<? extends AutoLocalThreshold.Config> bernsenConfigs() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0, 0),
                new AutoLocalThreshold.Config(15, 15, 0),
                new AutoLocalThreshold.Config(15, 30, 0),
                new AutoLocalThreshold.Config(15, 45, 0),
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(15, 75, 0),
                new AutoLocalThreshold.Config(30, 0, 0),
                new AutoLocalThreshold.Config(30, 15, 0),
                new AutoLocalThreshold.Config(30, 30, 0),
                new AutoLocalThreshold.Config(30, 45, 0),
                new AutoLocalThreshold.Config(30, 60, 0),
                new AutoLocalThreshold.Config(30, 75, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> bernsenConfigs2() {
        return List.of(
                new AutoLocalThreshold.Config(15, 75, 0),
                new AutoLocalThreshold.Config(15, 90, 0),
                new AutoLocalThreshold.Config(15, 105, 0),
                new AutoLocalThreshold.Config(30, 75, 0),
                new AutoLocalThreshold.Config(30, 90, 0),
                new AutoLocalThreshold.Config(30, 105, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> bernsenConfigs3() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0, 0),
                new AutoLocalThreshold.Config(15, 10, 0),
                new AutoLocalThreshold.Config(15, 20, 0),
                new AutoLocalThreshold.Config(15, 30, 0),
                new AutoLocalThreshold.Config(15, 40, 0),
                new AutoLocalThreshold.Config(15, 50, 0),
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(15, 70, 0),
                new AutoLocalThreshold.Config(15, 80, 0),
                new AutoLocalThreshold.Config(15, 90, 0),
                new AutoLocalThreshold.Config(15, 100, 0),
                new AutoLocalThreshold.Config(30, 0, 0),
                new AutoLocalThreshold.Config(30, 10, 0),
                new AutoLocalThreshold.Config(30, 20, 0),
                new AutoLocalThreshold.Config(30, 30, 0),
                new AutoLocalThreshold.Config(30, 40, 0),
                new AutoLocalThreshold.Config(30, 50, 0),
                new AutoLocalThreshold.Config(30, 60, 0),
                new AutoLocalThreshold.Config(30, 70, 0),
                new AutoLocalThreshold.Config(30, 80, 0),
                new AutoLocalThreshold.Config(30, 90, 0),
                new AutoLocalThreshold.Config(30, 100, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> meanConfigs() {
        return List.of(
                new AutoLocalThreshold.Config(15, 10, 0),
                new AutoLocalThreshold.Config(15, 20, 0),
                new AutoLocalThreshold.Config(15, 30, 0),
                new AutoLocalThreshold.Config(15, 40, 0),
                new AutoLocalThreshold.Config(15, 50, 0),
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(30, 10, 0),
                new AutoLocalThreshold.Config(30, 20, 0),
                new AutoLocalThreshold.Config(30, 30, 0),
                new AutoLocalThreshold.Config(30, 40, 0),
                new AutoLocalThreshold.Config(30, 50, 0),
                new AutoLocalThreshold.Config(30, 60, 0)
        );
    }


    private Iterable<? extends AutoLocalThreshold.Config> meanConfigs2() {
        return List.of(
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(15, 70, 0),
                new AutoLocalThreshold.Config(15, 80, 0),
                new AutoLocalThreshold.Config(15, 90, 0),
                new AutoLocalThreshold.Config(30, 60, 0),
                new AutoLocalThreshold.Config(30, 70, 0),
                new AutoLocalThreshold.Config(30, 80, 0),
                new AutoLocalThreshold.Config(30, 90, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> medianConfigs() {
        return List.of(
                new AutoLocalThreshold.Config(15, 10, 0),
                new AutoLocalThreshold.Config(15, 20, 0),
                new AutoLocalThreshold.Config(15, 30, 0),
                new AutoLocalThreshold.Config(15, 40, 0),
                new AutoLocalThreshold.Config(15, 50, 0),
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(30, 10, 0),
                new AutoLocalThreshold.Config(30, 20, 0),
                new AutoLocalThreshold.Config(30, 30, 0),
                new AutoLocalThreshold.Config(30, 40, 0),
                new AutoLocalThreshold.Config(30, 50, 0),
                new AutoLocalThreshold.Config(30, 60, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> medianConfigs2() {
        return List.of(
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(15, 70, 0),
                new AutoLocalThreshold.Config(15, 80, 0),
                new AutoLocalThreshold.Config(15, 90, 0),
                new AutoLocalThreshold.Config(30, 60, 0),
                new AutoLocalThreshold.Config(30, 70, 0),
                new AutoLocalThreshold.Config(30, 80, 0),
                new AutoLocalThreshold.Config(30, 90, 0)
        );
    }


    private Iterable<? extends AutoLocalThreshold.Config> midGreyConfigs() {
        return List.of(
                new AutoLocalThreshold.Config(15, 10, 0),
                new AutoLocalThreshold.Config(15, 20, 0),
                new AutoLocalThreshold.Config(15, 30, 0),
                new AutoLocalThreshold.Config(15, 40, 0),
                new AutoLocalThreshold.Config(15, 50, 0),
                new AutoLocalThreshold.Config(15, 60, 0),
                new AutoLocalThreshold.Config(30, 10, 0),
                new AutoLocalThreshold.Config(30, 20, 0),
                new AutoLocalThreshold.Config(30, 30, 0),
                new AutoLocalThreshold.Config(30, 40, 0),
                new AutoLocalThreshold.Config(30, 50, 0),
                new AutoLocalThreshold.Config(30, 60, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> niblackConfigs() {
        return List.of(
                new AutoLocalThreshold.Config(15, -0.3, 0),
                new AutoLocalThreshold.Config(15, -0.2, 0),
                new AutoLocalThreshold.Config(15, -0.1, 0),
                new AutoLocalThreshold.Config(15, 0.1, 0),
                new AutoLocalThreshold.Config(15, 0.2, 0),
                new AutoLocalThreshold.Config(15, 0.3, 0),
                new AutoLocalThreshold.Config(15, -0.3, 0),
                new AutoLocalThreshold.Config(15, -0.2, 0),
                new AutoLocalThreshold.Config(15, -0.1, 0),
                new AutoLocalThreshold.Config(15, 0.1, 0),
                new AutoLocalThreshold.Config(15, 0.2, 0),
                new AutoLocalThreshold.Config(15, 0.3, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> niblackConfigs2() {
        return List.of(
                new AutoLocalThreshold.Config(15, -0.2, 0),
                new AutoLocalThreshold.Config(15, -0.2, 10),
                new AutoLocalThreshold.Config(15, -0.2, 20),
                new AutoLocalThreshold.Config(15, -0.2, 30),
                new AutoLocalThreshold.Config(15, -0.2, 40),
                new AutoLocalThreshold.Config(15, -0.2, 50),
                new AutoLocalThreshold.Config(15, -0.2, 60),
                new AutoLocalThreshold.Config(15, -0.2, 70),
                new AutoLocalThreshold.Config(15, -0.2, 80),
                new AutoLocalThreshold.Config(15, -0.2, 90),
                new AutoLocalThreshold.Config(15, -0.2, 100),
                new AutoLocalThreshold.Config(30, -0.2, 0),
                new AutoLocalThreshold.Config(30, -0.2, 10),
                new AutoLocalThreshold.Config(30, -0.2, 20),
                new AutoLocalThreshold.Config(30, -0.2, 30),
                new AutoLocalThreshold.Config(30, -0.2, 40),
                new AutoLocalThreshold.Config(30, -0.2, 50),
                new AutoLocalThreshold.Config(30, -0.2, 60),
                new AutoLocalThreshold.Config(30, -0.2, 70),
                new AutoLocalThreshold.Config(30, -0.2, 80),
                new AutoLocalThreshold.Config(30, -0.2, 90),
                new AutoLocalThreshold.Config(30, -0.2, 100)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> onlyRadiusConfig() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0, 0),
                new AutoLocalThreshold.Config(30, 0, 0)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> phansalkarConfig() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0.125, 0.25),
                new AutoLocalThreshold.Config(15, 0.25, 0.25),
                new AutoLocalThreshold.Config(15, 0.375, 0.25),
                new AutoLocalThreshold.Config(15, 0.5, 0.25),
                new AutoLocalThreshold.Config(15, 0.125, 0.5),
                new AutoLocalThreshold.Config(15, 0.25, 0.5),
                new AutoLocalThreshold.Config(15, 0.375, 0.5),
                new AutoLocalThreshold.Config(15, 0.5, 0.5),
                new AutoLocalThreshold.Config(15, 0.125, 0.75),
                new AutoLocalThreshold.Config(15, 0.25, 0.75),
                new AutoLocalThreshold.Config(15, 0.375, 0.75),
                new AutoLocalThreshold.Config(15, 0.5, 0.75),
                new AutoLocalThreshold.Config(30, 0.125, 0.25),
                new AutoLocalThreshold.Config(30, 0.25, 0.25),
                new AutoLocalThreshold.Config(30, 0.375, 0.25),
                new AutoLocalThreshold.Config(30, 0.5, 0.25),
                new AutoLocalThreshold.Config(30, 0.125, 0.5),
                new AutoLocalThreshold.Config(30, 0.25, 0.5),
                new AutoLocalThreshold.Config(30, 0.375, 0.5),
                new AutoLocalThreshold.Config(30, 0.5, 0.5),
                new AutoLocalThreshold.Config(30, 0.125, 0.75),
                new AutoLocalThreshold.Config(30, 0.25, 0.75),
                new AutoLocalThreshold.Config(30, 0.375, 0.75),
                new AutoLocalThreshold.Config(30, 0.5, 0.75)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> phansalkarConfig2() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0.375, 0.5),
                new AutoLocalThreshold.Config(15, 0.5, 0.5),
                new AutoLocalThreshold.Config(15, 0.625, 0.5),
                new AutoLocalThreshold.Config(15, 0.75, 0.5),
                new AutoLocalThreshold.Config(15, 0.375, 0.75),
                new AutoLocalThreshold.Config(15, 0.5, 0.75),
                new AutoLocalThreshold.Config(15, 0.625, 0.75),
                new AutoLocalThreshold.Config(15, 0.75, 0.75),
                new AutoLocalThreshold.Config(15, 0.375, 1),
                new AutoLocalThreshold.Config(15, 0.5, 1),
                new AutoLocalThreshold.Config(15, 0.625, 1),
                new AutoLocalThreshold.Config(15, 0.75, 1),
                new AutoLocalThreshold.Config(30, 0.375, 0.5),
                new AutoLocalThreshold.Config(30, 0.5, 0.5),
                new AutoLocalThreshold.Config(30, 0.625, 0.5),
                new AutoLocalThreshold.Config(30, 0.75, 0.5),
                new AutoLocalThreshold.Config(30, 0.375, 0.75),
                new AutoLocalThreshold.Config(30, 0.5, 0.75),
                new AutoLocalThreshold.Config(30, 0.625, 0.75),
                new AutoLocalThreshold.Config(30, 0.75, 0.75),
                new AutoLocalThreshold.Config(30, 0.375, 1),
                new AutoLocalThreshold.Config(30, 0.5, 1),
                new AutoLocalThreshold.Config(30, 0.625, 1),
                new AutoLocalThreshold.Config(30, 0.75, 1)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> sauvolaConfig() {
        return List.of(
                new AutoLocalThreshold.Config(15, 0.25, 64),
                new AutoLocalThreshold.Config(15, 0.25, 128),
                new AutoLocalThreshold.Config(15, 0.25, 256),
                new AutoLocalThreshold.Config(15, 0.25, 512),
                new AutoLocalThreshold.Config(15, 0.5, 64),
                new AutoLocalThreshold.Config(15, 0.5, 128),
                new AutoLocalThreshold.Config(15, 0.5, 256),
                new AutoLocalThreshold.Config(15, 0.5, 512),
                new AutoLocalThreshold.Config(15, 0.75, 64),
                new AutoLocalThreshold.Config(15, 0.75, 128),
                new AutoLocalThreshold.Config(15, 0.75, 256),
                new AutoLocalThreshold.Config(15, 0.75, 512),
                new AutoLocalThreshold.Config(30, 0.25, 64),
                new AutoLocalThreshold.Config(30, 0.25, 128),
                new AutoLocalThreshold.Config(30, 0.25, 256),
                new AutoLocalThreshold.Config(30, 0.25, 512),
                new AutoLocalThreshold.Config(30, 0.5, 64),
                new AutoLocalThreshold.Config(30, 0.5, 128),
                new AutoLocalThreshold.Config(30, 0.5, 256),
                new AutoLocalThreshold.Config(30, 0.5, 512),
                new AutoLocalThreshold.Config(30, 0.75, 64),
                new AutoLocalThreshold.Config(30, 0.75, 128),
                new AutoLocalThreshold.Config(30, 0.75, 256),
                new AutoLocalThreshold.Config(30, 0.75, 512)
        );
    }

    private Iterable<? extends AutoLocalThreshold.Config> sauvolaConfig2() {
        return List.of(
                new AutoLocalThreshold.Config(15, 1, 64),
                new AutoLocalThreshold.Config(15, 1, 128),
                new AutoLocalThreshold.Config(15, 1, 256),
                new AutoLocalThreshold.Config(15, 1, 512),
                new AutoLocalThreshold.Config(30, 1, 64),
                new AutoLocalThreshold.Config(30, 1, 128),
                new AutoLocalThreshold.Config(30, 1, 256),
                new AutoLocalThreshold.Config(30, 1, 512)
        );
    }
}