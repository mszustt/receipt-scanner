package com.mszust.receiptscanner.utils.diff.line;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class LineComparatorTest {

    @Test
    void shouldProperlyCompareLines() {
        //given
        final List<String> original = Arrays.asList(
                "abc123",
                "def456",
                "ghi789"
        );
        final List<String> other = Arrays.asList(
                "acb12",
                "    ",
                "def701",
                "",
                "ghi7890"
        );

        final LineComparator lineComparator = new LineComparator();

        System.out.println(FuzzySearch.ratio("Rabat -2,50","")); //todo figure out why this is 18 instead of 0 in results
        //when
        final var lineComparisonResult = lineComparator.compareLines(null, original, other);

        //then
        assertThat(lineComparisonResult.getOverallMatch()).isEqualTo((double) 11 / 18);
        assertThat(lineComparisonResult.getLines()).containsExactly(
                new LineComparison(0, "abc123", "acb12", 2.5, 2.5 / 6, true),
                new LineComparison(1, "def456", "def701", 3d, 0.5, true),
                new LineComparison(2, "ghi789", "ghi7890", 5.5, 5.5 / 6, true)
        );

    }

}