package com.mszust.receiptscanner.utils.diff;

import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;


@ExtendWith(SpringExtension.class)
@SpringBootTest
class OtherResultComparisons {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private LineComparator lineComparator;

    @Test
    void blinkReceipt() throws IOException, URISyntaxException {
        final var benchmark = readFile(BENCHMARK_LOCATION);


        final var result = readFile("/files/results/blinkreceipt/IMG_0297.txt");

        final var comparisonResult = lineComparator.compareLines(null, benchmark, result);


        System.out.println("Accuracy for IMG_0297: " + comparisonResult.getOverallPercentage());


        final var result2 = readFile("/files/results/blinkreceipt/IMG_0297.txt");

        final var comparisonResult2 = lineComparator.compareLines(null, benchmark, result2);

        System.out.println("Accuracy for scan_600dpi: " + comparisonResult2.getOverallPercentage());
    }
}
