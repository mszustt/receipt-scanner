package com.mszust.receiptscanner.utils.diff.line;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class FuzzyWuzzyTest {

    @Test
    void shouldCalculateCorrectDistances() {
        System.out.println("Ratio: " + FuzzySearch.ratio("PłynGarnier700ml A 1 x24,99 24,99A", "PłynGarnier700ml 1 x24,99 24,99A"));
        final SoftAssertions softly = new SoftAssertions();
        softly.assertThat(FuzzySearch.ratio("abc", "abc")).isEqualTo(100);
        softly.assertThat(FuzzySearch.ratio("abc", "ab")).isEqualTo(80);
        softly.assertThat(FuzzySearch.ratio("abc", "acb")).isEqualTo(67);
        softly.assertThat(FuzzySearch.ratio("abc", "abbc")).isEqualTo(86);
        softly.assertAll();
    }

}
