package com.mszust.receiptscanner.utils.diff;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

class TextTest {


    @Test
    void shouldShiftKeysUponInsertion() {
        Text text = new Text();

        text.push("line 0");
        text.push("line 1");
        text.push("line 2");

        text.insertAtIndex(1, "new line 1");


        assertThat(text.getLines()).containsExactly(
                entry(0, "line 0"),
                entry(1, "new line 1"),
                entry(2, "line 1"),
                entry(3, "line 2")
        );
    }


    @Test
    void shouldCreateProperMapUponConstructingFromList() {
        final var lines = Arrays.asList(
                "line 0",
                "line 1",
                "line 2"
        );

        final Text text = new Text(lines);

        assertThat(text.getLines()).containsExactly(
                entry(0, "line 0"),
                entry(1, "line 1"),
                entry(2, "line 2")
        );
    }
}