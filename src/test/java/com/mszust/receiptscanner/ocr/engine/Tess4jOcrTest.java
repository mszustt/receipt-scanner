package com.mszust.receiptscanner.ocr.engine;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;

import static com.mszust.receiptscanner.utils.file.FileUtils.file;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class Tess4jOcrTest {

    ITesseract tesseract = new Tesseract();

    @Autowired
    Tess4jConfiguration tess4jConfiguration;

    @BeforeEach
    void setUp() {
        tesseract.setDatapath(tess4jConfiguration.getDataPathBest());
    }


    @Test
    @Disabled
    void shouldGenerateTessReport() throws URISyntaxException, IOException, TesseractException {
        final var file = file("/files/pl/paragon1/IMG_0297.jpeg");
        final var image = ImageIO.read(file);

        tesseract.createDocumentsWithResults(
                image,
                "IMG_0297.jpg",
                "tessreport",
                Collections.singletonList(ITesseract.RenderedFormat.PDF),
                0
        );

    }


}
