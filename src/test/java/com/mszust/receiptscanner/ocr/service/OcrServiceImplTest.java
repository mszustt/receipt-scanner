package com.mszust.receiptscanner.ocr.service;


import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
class OcrServiceImplTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;


    @Test
    void test() throws URISyntaxException {
        final var file = file("/files/de/IMG0001.jpg");
        String result = scannerService.process(new Scannable(file));
        assertThat(result).isNotBlank();
    }

    @Test
    void shouldReadAFile() throws IOException, URISyntaxException {
        final var file = file("/files/de/test");
        InputStream is = new FileInputStream(file);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));

        StringBuilder sb = new StringBuilder();

        buf.lines().forEach(sb::append);

        String fileAsString = sb.toString();
        assertThat(fileAsString).isEqualTo("test");
    }

    @Test
    void allReceiptTest() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(file);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "all_receipts");
    }

    @Test
    void oneReceiptTest() throws IOException, URISyntaxException, DiffException {
        final var benchmark = readFile(BENCHMARK_LOCATION);

        final var file = file(testFiles().get(0));

        final var result = scannerService.processFile(file);
        final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
        comparisonResult.setDuration(result.getOcrDuration());

        final var report = htmlReportBuilder.generateDocument(comparisonResult);
        saveResultFile(report, BENCHMARK_LOCATION, "one_receipt");
    }

    private static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/paragon1/IMG_0297.jpeg",
                "/files/pl/paragon1/IMG_0297_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0298.jpeg",
                "/files/pl/paragon1/IMG_0299.jpeg",
                "/files/pl/paragon1/IMG_0300.jpeg",
                "/files/pl/paragon1/IMG_0301.jpeg",
                "/files/pl/paragon1/IMG_0302.jpeg",
                "/files/pl/paragon1/IMG_0303.jpeg",
                "/files/pl/paragon1/IMG_0304.jpeg",
                "/files/pl/paragon1/IMG_0305.jpeg",
                "/files/pl/paragon1/IMG_0306.jpeg",
                "/files/pl/paragon1/scan_300dpi.jpg",
                "/files/pl/paragon1/scan_600dpi.jpg"
        );
    }

//    private void saveResultFile(final List<SingleDiffResult> results) throws DiffException, IOException, URISyntaxException {
//        final var htmlReport = htmlReportBuilder.generateDocument(new MultipleDiffResult().addAll(results));
//
//        saveFile(file(BENCHMARK_LOCATION).getParentFile().getAbsolutePath() + "/result/diff.html", htmlReport);
//    }

}