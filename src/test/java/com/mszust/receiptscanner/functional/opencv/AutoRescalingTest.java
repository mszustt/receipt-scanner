package com.mszust.receiptscanner.functional.opencv;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.opencv.Cropping;
import com.mszust.receiptscanner.utils.opencv.LineHeight;
import com.mszust.receiptscanner.utils.opencv.OpenCvUtils;
import com.mszust.receiptscanner.utils.opencv.Scaling;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AutoRescalingTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;


    @SneakyThrows
    @Test
    void testDataBest() throws IOException, URISyntaxException, DiffException {
        System.setProperty("java.awt.headless", "false");
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.TRAINED);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
//        for (String fileLocation : List.of("/files/pl/paragon2/20201123_225320.jpg", "/files/pl/paragon2/20201123_225331.jpg", "/files/pl/paragon2/20201123_231041.jpg",
//                "/files/pl/paragon2/20201123_231121.jpg")) {
            final var file = file(fileLocation);

            Mat src = Imgcodecs.imread(file.getAbsolutePath());
            final Mat croppedMat = Cropping.cropImageExact(src);

            final Double lineHeight = LineHeight.findLineHeight(croppedMat);

            if (lineHeight == null) {
                continue;
            }
            System.out.println(String.format("Line height: %f, for image %s", lineHeight, fileLocation));

            final Double scale = Scaling.findScale(lineHeight);

            if (scale == null) {
                continue;
            }
            System.out.println(String.format("Scale: %f, for image %s", scale, fileLocation));

//            final Mat boundary = Cropping.cropImageApprox(src);

            final Mat rescaled = Scaling.rescale(croppedMat, scale);

//            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-autoscaled." + FilenameUtils.getExtension(file.getAbsolutePath());
//            writeImage(processedFilePath, croppedMat);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(rescaled);
//
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));

//            result.getRecognizedLines().forEach(System.out::println);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "autoscaled-wyuczone-40-dawg-slownik2");
    }
}
