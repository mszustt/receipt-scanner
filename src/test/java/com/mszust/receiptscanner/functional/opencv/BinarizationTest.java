package com.mszust.receiptscanner.functional.opencv;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static com.mszust.receiptscanner.utils.opencv.OpenCvUtils.loadImage;
import static com.mszust.receiptscanner.utils.opencv.OpenCvUtils.writeImage;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BinarizationTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;

    @Test
    void testDataBest() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            for (Integer blockSize : blockSizes()) {
                for (Integer C : constants()) {
                    final var file = file(fileLocation);


                    final var result = scannerService.processFile(file);
                    final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
                    comparisonResult.setDuration(result.getOcrDuration());
                    comparisonResults.add(comparisonResult);

                    Mat matFile = loadImage(file.getAbsolutePath());
                    Mat processedMat = new Mat();

                    final LineComparisonResult comparisonResultGauss = getLineComparisonResultGaussian(benchmark, file, matFile, processedMat, blockSize, C);
                    comparisonResults.add(comparisonResultGauss);

                    final LineComparisonResult comparisonResultMean = getLineComparisonResultMean(benchmark, file, matFile, processedMat, blockSize, C);
                    comparisonResults.add(comparisonResultMean);
//            Files.delete(Path.of(processedFilePath));

                }
            }
        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "binarized-C0");
    }

    private LineComparisonResult getLineComparisonResultGaussian(List<String> benchmark, File file, Mat matFile, Mat processedMat, int blockSize, int C) {
//        Imgproc.adaptiveThreshold(matFile, processedMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 15, 40); //reference call
        Imgproc.adaptiveThreshold(matFile, processedMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, blockSize, C);

        final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-processed-gaussian-bl" + blockSize + "-c" + C + "." + FilenameUtils.getExtension(file.getAbsolutePath());

        writeImage(processedFilePath, processedMat);
        final File processedFile = new File(processedFilePath);

        final var result = scannerService.processFile(processedFile);
        final var comparisonResult = lineComparator.compareLines(FileUtils.getFileName(processedFile), benchmark, result.getRecognizedLines());
        comparisonResult.setDuration(result.getOcrDuration());
        return comparisonResult;
    }

    private LineComparisonResult getLineComparisonResultMean(List<String> benchmark, File file, Mat matFile, Mat processedMat, int blockSize, int C) {
        Imgproc.adaptiveThreshold(matFile, processedMat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, blockSize, C);

        final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-processed-mean-bl" + blockSize + "-c" + C + "." + FilenameUtils.getExtension(file.getAbsolutePath());

        writeImage(processedFilePath, processedMat);
        final File processedFile = new File(processedFilePath);

        final var result = scannerService.processFile(processedFile);
        final var comparisonResult = lineComparator.compareLines(FileUtils.getFileName(processedFile), benchmark, result.getRecognizedLines());
        comparisonResult.setDuration(result.getOcrDuration());
        return comparisonResult;
    }

    private static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/paragon1/IMG_0297.jpeg",
                "/files/pl/paragon1/IMG_0298.jpeg",
                "/files/pl/paragon1/IMG_0299.jpeg",
                "/files/pl/paragon1/IMG_0300.jpeg",
                "/files/pl/paragon1/IMG_0301.jpeg",
                "/files/pl/paragon1/IMG_0302.jpeg",
                "/files/pl/paragon1/IMG_0303.jpeg",
                "/files/pl/paragon1/IMG_0304.jpeg",
                "/files/pl/paragon1/IMG_0305.jpeg",
                "/files/pl/paragon1/IMG_0306.jpeg",
                "/files/pl/paragon1/scan_300dpi.jpg",
                "/files/pl/paragon1/scan_600dpi.jpg"
        );
    }

    private static List<Integer> blockSizes() {
        return List.of(
                9,
                15,
                21
        );
    }

    private static List<Integer> constants() {
        return List.of(
                0
//                20,
//                40,
//                60
        );
    }
}
