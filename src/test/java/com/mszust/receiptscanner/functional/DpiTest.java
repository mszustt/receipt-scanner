package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

class DpiTest extends IntegrationTest {

    @Test
    void testDpiRescaledManually() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String key : testFiles().keySet()) {
            final var file = file(testFiles().get(key));

            final String fileName = getFileName(file) + "-dpi-" + key;

            if (key.equals("null") || key.equals("null2")) {
                scannerService.setDpi(300);
            } else {
                scannerService.setDpi(Integer.valueOf(key));
            }
            final var result = scannerService.processFile(new Scannable(file));

            final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "dpi-manual");
    }

    private static Map<String, String> testFiles() {
        return Map.of(
                "null", "/files/pl/dpi/IMG_0297.jpeg",
//                "70", "/files/pl/dpi/IMG_0297-70.png",
//                "150", "/files/pl/dpi/IMG_0297-150.png",
                "null2", "/files/pl/dpi/IMG_0297-300-imagej.png",
                "300", "/files/pl/dpi/IMG_0297-300.png",
                "600", "/files/pl/dpi/IMG_0297-600.png"
        );
    }
}
