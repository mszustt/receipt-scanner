package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import com.mszust.receiptscanner.utils.opencv.Contrast;
import com.mszust.receiptscanner.utils.opencv.Cropping;
import com.mszust.receiptscanner.utils.opencv.Histogram;
import com.mszust.receiptscanner.utils.opencv.HistogramUtils;
import com.mszust.receiptscanner.utils.opencv.OpenCvUtils;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileFixtures.testFiles;
import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;
import static com.mszust.receiptscanner.utils.opencv.OpenCvUtils.writeImage;

public class HistogramModificationsTest extends IntegrationTest {

    @SneakyThrows
    @Test
    void equalizeHistogram() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat equalized = HistogramUtils.equalize(grey);
            final Mat histogram = HistogramUtils.histogram(equalized);

//            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-equalized." + FilenameUtils.getExtension(file.getAbsolutePath());
//            writeImage(processedFilePath, histogram);
//            final String processedFilePathImage = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-equalized." + FilenameUtils.getExtension(file.getAbsolutePath());
//            writeImage(processedFilePathImage, equalized);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(equalized);
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-equalized");
    }

    @SneakyThrows
    @Test
    void normalizeHistogram() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat normalized = HistogramUtils.normalize(grey);
            final Mat histogram = HistogramUtils.histogram(normalized);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-normalized." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
            final String processedFilePathImage = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-normalized." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePathImage, normalized);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(normalized);
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-normalized");
    }

    @SneakyThrows
    @Test
    void increaseContrastManually() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat contrasted = Contrast.contrast(grey, 1.25);
            final Mat histogram = HistogramUtils.histogram(contrasted);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-contrast1_25x." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
            final String processedFilePathImage = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-contrast1_25x." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePathImage, contrasted);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(contrasted);
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-contrast1_25x");
    }

    @SneakyThrows
    @Test
    void clahe() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Mat clahed = Contrast.clahe(grey);
            final Mat histogram = HistogramUtils.histogram(clahed);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-clahe-clip0_5-1_1." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histogram);
            final String processedFilePathImage = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-clahe-clip0_5-1_1." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePathImage, clahed);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(clahed);
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-clahe-clip0_5-1_1");
    }

    @SneakyThrows
    @Test
    void recalculate() throws URISyntaxException, IOException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = FileUtils.file(fileLocation);
            System.out.println("Processing file: " + file.getName());


            final Mat matFile = Imgcodecs.imread(file.getAbsolutePath());
            final Mat cropped = Cropping.cropImageExact(matFile);

            Mat grey = new Mat();
            Imgproc.cvtColor(cropped, grey, Imgproc.COLOR_BGR2GRAY);

            final Histogram histogram = HistogramUtils.histogramArray(grey);
            final int thresh = HistogramUtils.findBackgroundThreshold(histogram, 8000);
            System.out.println("Calculated thresh: " + thresh);

            final Mat recalculated = HistogramUtils.recalculate(grey, thresh);
            final Mat histImage = HistogramUtils.histogram(recalculated);

            final String processedFilePath = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-histogram-recalc-t8k." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePath, histImage);
            final String processedFilePathImage = FilenameUtils.removeExtension(file.getAbsolutePath()) + "-cropped-recalc-t8k." + FilenameUtils.getExtension(file.getAbsolutePath());
            writeImage(processedFilePathImage, recalculated);

            final BufferedImage processed = OpenCvUtils.toBufferedImage(recalculated);
            final var result = scannerService.processFile(new Scannable(processed, FilenameUtils.removeExtension(file.getName())));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
            comparisonResults.add(comparisonResult);
        }
        final var report = reportGenerator.generateHtmlReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "cropped-recalc-t8k");
    }

}
