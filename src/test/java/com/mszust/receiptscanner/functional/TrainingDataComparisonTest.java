package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.report.csv.CsvReportBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class TrainingDataComparisonTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;
    @Autowired
    private CsvReportBuilder csvReportBuilder;

    @Test
    void testDataFast() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            final Mat src = Imgcodecs.imread(file.getAbsolutePath());
//            final BufferedImage srcBI = OpenCvUtils.toBufferedImage(src);
//            final Scannable scannable = new Scannable(srcBI, FilenameUtils.removeExtension(file.getName()));
//            final Mat rescaled = Preprocessor.rescale(src);
//            final BufferedImage bufferedImage = OpenCvUtils.toBufferedImage(rescaled);
//            OpenCvUtils.writeImage(file(BENCHMARK_LOCATION).getParentFile().getAbsolutePath() + "/result/zdjecia/stat/preprocess/" + getFileName(file) + "-rescaled.png", bufferedImage);
//            final BufferedImage bufferedImage = Preprocessor.preprocess(src);
//            final BufferedImage bufferedImage = OpenCvUtils.toBufferedImage(cropped);
//            OpenCvUtils.writeImage(file(BENCHMARK_LOCATION).getParentFile().getAbsolutePath() + "/result/zdjecia/stat/preprocess/" + getFileName(file) + ".png", bufferedImage);

            final var result = scannerService.processFile(file);
//            final var result = scannerService.processFile(new Scannable(bufferedImage, FilenameUtils.removeExtension(file.getName())));
            stopWatch.stop();
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());
//
//            comparisonResult.setDuration(stopWatch.getTotalTimeMillis());
            comparisonResults.add(comparisonResult);

        }
        final var csvReport = csvReportBuilder.generateReport(comparisonResults);
        final var htmlReport = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(csvReport, BENCHMARK_LOCATION, "reworked-matching-1", "csv");
        saveResultFile(htmlReport, BENCHMARK_LOCATION, "reworked-matching-1", "html");
    }

    @Test
    void testDataBest() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(file);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "data_best");
    }

    @Test
    void testDataTrained() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.TRAINED);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(file);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "data_trained10_no_dawg");
    }

    @Test
    void testDataMixed() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.MIXED);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(file);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "data_mixed");
    }

    private static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/paragon1/IMG_0297.jpeg",
//                "/files/pl/paragon1/IMG_0298.jpeg",
                "/files/pl/paragon1/IMG_0299.jpeg",
                "/files/pl/paragon1/IMG_0300.jpeg",
                "/files/pl/paragon1/IMG_0301.jpeg",
                "/files/pl/paragon1/IMG_0302.jpeg",
//                "/files/pl/paragon1/IMG_0303.jpeg",
                "/files/pl/paragon1/IMG_0304.jpeg",
                "/files/pl/paragon1/IMG_0305.jpeg",
                "/files/pl/paragon1/IMG_0306.jpeg",
                "/files/pl/paragon1/scan_300dpi.jpg",
                "/files/pl/paragon1/scan_600dpi.jpg"
        );
    }
}
