package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BackgroundTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;

    @Test
    void testDataBest() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(file);
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);

        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "background");
    }


    private static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/paragon1/IMG_0297.jpeg",
                "/files/pl/paragon1/IMG_0297_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0298.jpeg",
                "/files/pl/paragon1/IMG_0298_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0299.jpeg",
                "/files/pl/paragon1/IMG_0299_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0300.jpeg",
                "/files/pl/paragon1/IMG_0300_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0301.jpeg",
                "/files/pl/paragon1/IMG_0301_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0302.jpeg",
                "/files/pl/paragon1/IMG_0302_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0303.jpeg",
                "/files/pl/paragon1/IMG_0303_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0304.jpeg",
                "/files/pl/paragon1/IMG_0304_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0305.jpeg",
                "/files/pl/paragon1/IMG_0305_no_margin.jpeg",
                "/files/pl/paragon1/IMG_0306.jpeg",
                "/files/pl/paragon1/IMG_0306_no_margin.jpeg"
        );
    }
}
