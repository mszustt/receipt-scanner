package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.utils.IntegrationTest;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import com.mszust.receiptscanner.utils.file.FileUtils;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

public class SuperResolutionTest extends IntegrationTest {

    @Test
    void superResolutionTests() throws URISyntaxException, IOException, DiffException {
        for (String scale : List.of("x2", "x4")) {
            superResolutionTest(scale);
        }
    }

    void superResolutionTest(String scale) throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        final File parentDir = FileUtils.file(
                String.format(
                        "/files/pl/paragon1/superres/FSRCNN/%s/",
                        scale
                )
        );

        final File[] files = parentDir.listFiles();

        for (File file : files) {
//            final String fileName = getFileName(file) + String.format("-FSRCNN-%s", scale);
            final String fileName = getFileName(file);
            final BufferedImage processed = ImageIO.read(file);

            final var result = scannerService.processFile(new Scannable(processed, fileName));

            final var comparisonResult = lineComparator.compareLines(fileName, benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }

        final var report = reportGenerator.generateCsvReport(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, String.format("-FSRCNN-%s", scale));
    }

}