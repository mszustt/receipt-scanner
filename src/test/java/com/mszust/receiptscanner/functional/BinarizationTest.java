package com.mszust.receiptscanner.functional;

import com.github.difflib.algorithm.DiffException;
import com.mszust.receiptscanner.ocr.engine.Tess4jOcr;
import com.mszust.receiptscanner.ocr.model.Scannable;
import com.mszust.receiptscanner.ocr.service.TesseractOcr;
import com.mszust.receiptscanner.utils.diff.html.HtmlReportBuilder;
import com.mszust.receiptscanner.utils.diff.line.LineComparator;
import com.mszust.receiptscanner.utils.diff.line.LineComparisonResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mszust.receiptscanner.utils.TestFileUtils.saveResultFile;
import static com.mszust.receiptscanner.utils.file.FileUtils.file;
import static com.mszust.receiptscanner.utils.file.FileUtils.getFileName;
import static com.mszust.receiptscanner.utils.file.FileUtils.readFile;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BinarizationTest {
    private static final String BENCHMARK_LOCATION = "/files/pl/paragon1/benchmark";

    @Autowired
    private TesseractOcr scannerService;
    @Autowired
    private LineComparator lineComparator;
    @Autowired
    private HtmlReportBuilder htmlReportBuilder;

    @Test
    void testDataBest() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFiles()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "binarized");
    }

    @Test
    void testBinarizeProperly() throws IOException, URISyntaxException, DiffException {
        scannerService.setTrainingData(Tess4jOcr.TrainingDataType.BEST);
        final var benchmark = readFile(BENCHMARK_LOCATION);
        final List<LineComparisonResult> comparisonResults = new ArrayList<>();

        for (String fileLocation : testFilesBinarization()) {
            final var file = file(fileLocation);

            final var result = scannerService.processFile(new Scannable(file));
            final var comparisonResult = lineComparator.compareLines(getFileName(file), benchmark, result.getRecognizedLines());
            comparisonResult.setDuration(result.getOcrDuration());

            comparisonResults.add(comparisonResult);
        }
        final var report = htmlReportBuilder.generateDocument(comparisonResults);
        saveResultFile(report, BENCHMARK_LOCATION, "binarized_0300");
    }

    private static List<String> testFiles() {
        return Arrays.asList(
                "/files/pl/binarization/IMG_0299.jpeg",
                "/files/pl/binarization/IMG_0299_binarized.jpeg",
                "/files/pl/binarization/IMG_0299_binarized_177.jpeg",
                "/files/pl/binarization/IMG_0300.jpeg",
                "/files/pl/binarization/IMG_0300_binarized.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_82.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_60.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_80.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_100.jpeg"
        );
    }

    private static List<String> testFilesBinarization() {
        return Arrays.asList(
                "/files/pl/binarization/IMG_0300.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_60.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_70.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_73.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_75.jpeg",
                "/files/pl/binarization/IMG_0300_binarized_80.jpeg"
        );
    }
}
